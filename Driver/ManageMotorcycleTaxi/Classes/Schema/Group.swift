//
//  Group.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/14/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class Group: Mappable {
    var name = ""
    var id: Int?
    
    //var lastCreated = NSDate()
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        name   <- map["name"]
        id     <- map["id"]
    }
}