//
//  GMSCustomPlace.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import GoogleMaps
import ObjectMapper

class GMSCustomPlace: Mappable {
    var name = ""
//    var placeID: String
    var coordinate = CLLocationCoordinate2DMake(Constants.TSNAirportInfo.latitude, Constants.TSNAirportInfo.longitude)
    var formattedAddress = ""
    var longitude = 0.0
    var latitude = 0.0
    var distance = 0
    
    init(){
        
    }
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        distance <- map["distance"]
        name   <- map["name"]
        formattedAddress  <- map["location"]
        longitude <- map["coordinates_longitude"]
        latitude <- map["coordinates_latitude"]
        coordinate = CLLocationCoordinate2DMake(latitude, longitude)
    }
    
    convenience init(place: GMSPlace) {
        self.init(name:place.name, latitude:place.coordinate.latitude, longitude:place.coordinate.longitude, formattedAddress:place.formattedAddress!)
    }
    
    init(name:String, latitude:Double, longitude:Double, formattedAddress:String) {
        self.name = name
//        self.coordinate = coordinate
        self.formattedAddress = formattedAddress
//        self.addressComponents = addressComponents
        self.longitude = longitude
        self.latitude = latitude
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    convenience init(name:String, coordinate: CLLocationCoordinate2D, formattedAddress:String) {
        self.init(name:name, latitude:coordinate.latitude, longitude:coordinate.longitude, formattedAddress:formattedAddress)
    }
    
    convenience init(name:String, coordinate: CLLocationCoordinate2D) {
        self.init(name:name, latitude:coordinate.latitude, longitude:coordinate.longitude, formattedAddress:name)
    }
    
}
