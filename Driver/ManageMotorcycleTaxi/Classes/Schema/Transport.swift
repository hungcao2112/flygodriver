//
//  Transport.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/21/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

enum TransportStatus: Int {
    case Available = 1
    case NotAvailable = 0
}

class Transport: Mappable {
    var price = 0
    var name = ""
    var price_fluctuations = 10
    var id = 1
    var brand = "Honda"
    var icon = ""
    var slot_number = 2
    var status = TransportStatus.Available
    var thresholds:[Threshold]! = [Threshold]()
    var percent = 100
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        price_fluctuations      <- map["price_fluctuations"]
        price                   <- map["price"]
        name                    <- map["name"]
        id                      <- map["id"]
        brand                   <- map["brand"]
        icon                    <- map["icon"]
        slot_number             <- map["slot_number"]
        status                  <- map["status"]
        percent                  <- map["percent"]
        thresholds               <- map["threshold"]
        thresholds.sortInPlace {(threshold1:Threshold, threshold2:Threshold) -> Bool in
            threshold1.value < threshold2.value
        }
    }
    
    func isMotobikeTransport() -> Bool {
        return (slot_number == Constants.Transport.motobikeSlotNumber)
    }
}

class Threshold: Mappable {
    var money = 0
    var value = 0
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        money      <- map["money"]
        value                   <- map["value"]
    }
}