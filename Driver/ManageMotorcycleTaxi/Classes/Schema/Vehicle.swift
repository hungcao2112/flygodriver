//
//  Vehicle.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/9/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class Vehicle : Mappable{
    var vehicleNumber = ""
    var vehicleType = ""
    var vehicleStatus = 0
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        vehicleNumber <- map["vehicle_number"]
        vehicleType <- map["vehicle_type"]
        vehicleStatus <- map["vehicle_status"]
    }
}