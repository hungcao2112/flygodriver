//
//  LoginReponseData.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/14/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginResponseData: Mappable {
    
    var groups = [Group]()
    var user = User()
    var id = ""
    var code = ""
    var fullname = ""
    var email = ""
    var phone = ""
    var birthday = ""
    var id_card_number = ""
    var id_card_date = ""
    var id_card_province = ""
    var avartar = ""
    var address = ""
    var city = ""
    var country = ""
    var token = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        fullname <- map["fullname"]
        email <- map["email"]
        phone <- map["phone"]
        birthday <- map["birthday"]
        id_card_number <- map["id_card_number"]
        id_card_date <- map["id_card_date"]
        id_card_province <- map["id_card_province"]
        avartar <- map["avartar"]
        address <- map["address"]
        city <- map["city"]
        country <- map["country"]
        token <- map["token"]
    }
    
    func isBelongToGroup(type: UserType) -> Bool {
        for group in groups {
            if group.id == type.rawValue {
                return true
            }
        }
        return false
    }
}

