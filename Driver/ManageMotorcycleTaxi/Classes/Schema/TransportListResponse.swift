//
//  TransportListResponse.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/28/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class TransportListResponse : Mappable{
    var code: Int?
    var data: [Transport]?
    var message = ""
    var status: Bool = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        code                     <- map["code"]
        data                     <- map["data"]
        message                  <- map["message"]
        status                   <- map["status"]
    }
}