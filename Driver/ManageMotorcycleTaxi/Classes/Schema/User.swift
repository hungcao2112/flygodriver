//
//  User.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/14/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    var birthday = ""
    var firstName = ""
    var lastName = ""
    var address = ""
    var gender = ""
    var phone = ""
    var middleName = ""
    var id: Int?
    var avatar = ""
    var create_at = NSDate()
    var account = ""
    var email = ""
    var code_number = ""
    var vehicle = [Vehicle]()
    var status: Int?
    var facebookId = ""
    var facebookToken = ""
    init(){
        
    }
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        birthday   <- map["birthday"]
        firstName  <- map["firstName"]
        lastName   <- map["lastName"]
        address    <- map["address"]
        gender     <- map["gender"]
        phone      <- map["phone"]
        middleName <- map["middleName"]
        id         <- map["id"]
        avatar     <- map["avatar"]
        create_at  <- (map["create_at"], DateTransformExtension(dateFormat: DateTransformExtension.DateFormatStyle.DateTimeComplex))
        account    <- map["account"]
        email      <- map["email"]
        vehicle    <- map["vehicle"]
        status      <- map["status"]
        code_number <- map["code_number"]
    }
}
