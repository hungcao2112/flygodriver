//
//  ProfileResponse.swift
//  ManageMotorcycleTaxi
//
//  Created by Hưng Cao on 2/1/18.
//  Copyright © 2018 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileResponse: Mappable {
    var code: Int?
    var data = [ProfileResponseData]()
    var message = ""
    var status: Bool = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        code                     <- map["code"]
        data                     <- map["data"]
        message                  <- map["message"]
        status                   <- map["status"]
    }
}
