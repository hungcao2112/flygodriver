//
//  PaymentResponse.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 8/12/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentResponse: Mappable {
    
    var code: Int?
    var data: PaymentResponseData?
    var message = ""
    var status: Bool = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        code                     <- map["code"]
        data                     <- map["data"]
        message                  <- map["message"]
        status                   <- map["status"]
    }
}

class PaymentResponseData: Mappable {
    var responsecode = ""
    var Descriptionvn = ""
    var Descriptionen = ""
    var url = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        responsecode    <-  map["responsecode"]
        Descriptionvn   <-  map["Descriptionvn"]
        Descriptionen   <-  map["Descriptionen"]
        url             <-  map["url"]
    }
    
}