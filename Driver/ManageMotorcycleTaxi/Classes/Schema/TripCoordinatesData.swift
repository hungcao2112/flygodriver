//
//  TripCoordinatesData.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/18/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class TripCoordinatesData: Mappable {

    var latitude = 0.0
    var longitude = 0.0
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        latitude                <- map["latitude"]
        longitude                <- map["longitude"]
    }
    
}