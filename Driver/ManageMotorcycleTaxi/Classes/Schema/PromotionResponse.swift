//
//  PromotionReponse.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 10/28/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class GetPromotionsResponse: Mappable {
    
    var code: Int?
    var data: [PromotionResponseData]?
    var message = ""
    var status: Bool = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        code                     <- map["code"]
        data                     <- map["data"]
        message                  <- map["message"]
        status                   <- map["status"]
    }
}

class SinglePromotionResponse: Mappable {
    
    var code: Int?
    var data: PromotionResponseData?
    var message = ""
    var status: Bool = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        code                     <- map["code"]
        data                     <- map["data"]
        message                  <- map["message"]
        status                   <- map["status"]
    }
}

class PromotionResponseData: Mappable {
    
    var end_date = ""
    var number = 0
    var image = ""
    var code = ""
    var updated_at = ""
    var name = ""
    var created_at = ""
    var id = 0
    var percent = 0
    var start_date = ""
    var status = 0
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        end_date                <- map["end_date"]
        number                <- map["number"]
        image                <- map["image"]
        code                <- map["code"]
        updated_at                <- map["updated_at"]
        name                <- map["name"]
        created_at                <- map["created_at"]
        id                <- map["id"]
        percent                <- map["percent"]
        start_date                <- map["start_date"]
        status                <- map["status"]
    }
    
}
