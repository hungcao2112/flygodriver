//
//  DriverTrip.swift
//  ManageMotorcycleTaxi
//
//  Created by Hưng Cao on 2/2/18.
//  Copyright © 2018 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class DriverTrip: Mappable{
    var id = ""
    var code = ""
    var address_a = ""
    var address_b = ""
    var distance = ""
    var total = ""
    var date_pickup = ""
    var time_pickup = ""
    var vehicle = ""
    var fullname = ""
    var email = ""
    var phone = ""
    var airline = ""
    var status = ""
    var payment_status = ""
    var payment_method = ""
    var date_added = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        address_a <- map["address_a"]
        address_b <- map["address_b"]
        distance <- map["distance"]
        total <- map["total"]
        date_pickup <- map["date_pickup"]
        time_pickup <- map["time_pickup"]
        vehicle <- map["vehicle"]
        fullname <- map["fullname"]
        email <- map["email"]
        phone <- map["phone"]
        airline <- map["airline"]
        status <- map["status"]
        payment_status <- map["payment_status"]
        payment_method <- map["payment_method"]
        date_added <- map["date_added"]
    }
}
