//
//  TripHistoryResponse.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/27/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class TripHistoryResponse : Mappable{
    var code: Int?
    var data = [TripHistoryResponseData]()
    var message = ""
    var status: Bool = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        code                     <- map["code"]
        data                     <- map["data"]
        message                  <- map["message"]
        status                   <- map["status"]
    }
}
