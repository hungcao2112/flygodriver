//
//  PushNotification.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class PushNotification: Mappable {
    
    var trip: Trip?
    var driver: Driver?
    var customer: User?
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        trip   <- map["trip"]
        driver   <- map["driver.user"]
        customer   <- map["customer"]
    }
}