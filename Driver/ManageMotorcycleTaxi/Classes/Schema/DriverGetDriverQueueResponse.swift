//
//  DriverGetDriverQueueResponse.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/16/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//


import Foundation
import ObjectMapper

class DriverGetDriverQueueResponse : Mappable{
    var code: Int?
    var data: DriverGetDriverQueueResponseData?
    var message = ""
    var status: Bool = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        code                     <- map["code"]
        data                     <- map["data"]
        message                  <- map["message"]
        status                   <- map["status"]
    }
}

class DriverGetDriverQueueResponseData: Mappable {
    
    var yourPosition: Int = 0
    var totalQueue: Int = 0
    
    //var lastCreated = NSDate()
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        yourPosition    <- map["your_position"]
        totalQueue      <- map["total_queue"]
    }
}