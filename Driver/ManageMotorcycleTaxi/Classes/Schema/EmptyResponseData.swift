//
//  EmptyResponseData.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/27/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class EmptyResponseData: Mappable {
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
    }
}
