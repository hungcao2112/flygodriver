//
//  ProfileResponseData.swift
//  ManageMotorcycleTaxi
//
//  Created by Hưng Cao on 2/1/18.
//  Copyright © 2018 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileResponseData: Mappable {
    var id = ""
    var code = ""
    var title = ""
    var fullname = ""
    var email = ""
    var phone = ""
    var birthday = ""
    var id_card_number = ""
    var id_card_date = ""
    var id_card_province = ""
    var avartar = ""
    var address = ""
    var city = ""
    var country = ""
    var token = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        code <- map["code"]
        title <- map["title"]
        fullname <- map["fullname"]
        email <- map["email"]
        phone <- map["phone"]
        birthday <- map["birthday"]
        id_card_number <- map["id_card_number"]
        id_card_province <- map["id_card_province"]
        avartar <- map["avartar"]
        address <- map["address"]
        city <- map["city"]
        address <- map["address"]
        city <- map["city"]
        country <- map["country"]
        token <- map["token"]
    }
}
