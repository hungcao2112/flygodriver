//
//  RegisterResponseData.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/15/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class RegisterResponseData: Mappable {
    
    var id = ""
    var active_code = ""
    //var lastCreated = NSDate()
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        active_code <- map["active_code"]
    }
}
