//
//  GetCodeReponseData.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/18/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

class CodeResponseData: Mappable {
    
    var code = ""
    var account = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        code   <- map["code"]
        account     <- map["account"]
    }
}
