//
//  Driver.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/9/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

enum DriverStatus: Int {
    case Idle = 1,
    Ready = 2,
    Waiting = 3,
    Accept = 4,
    Driving = 5
}

class Driver : User{
    var driverLicenseNumber = ""
    var driverCode = ""
    var relatives = ""
    var familyRegister = ""
    var phoneRelatives = ""
    var idNo = ""
    var is_assigned: Int?
    var is_managed: Int?
    
    override func mapping(map: Map) {
        super.mapping(map)
        
        driverLicenseNumber <- map["driver_license_number"]
        driverCode <- map["driver_code"]
        relatives <- map["relatives"]
        familyRegister <- map["family_register"]
        phoneRelatives <- map["phone_relatives"]
        idNo <- map["id_no"]
        is_assigned <- map["is_assigned"]
        is_managed <- map["is_managed"]

    }
    
    func isManageable () -> Bool {
        if (status != nil) {
            switch status! {
            case DriverStatus.Idle.rawValue, DriverStatus.Ready.rawValue, DriverStatus.Driving.rawValue:
                return true
            default:
                return false
            }
        }
        return false
    }
}