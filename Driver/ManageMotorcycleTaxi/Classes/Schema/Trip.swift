//
//  Trip.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/26/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

enum PaymentMethod: Int {
    case PrePaid = 1,
    PostPaid = 2
}

enum TripStatus: Int {
    case UNKNOWN = 0,
    INITIAL,
    WAITING,
    READY,
    RUNNING,
    WAITING_CUSTOMER,
    END,
    TERMINATED
    
    static func getTripStatusName(tripStatus: TripStatus) -> String{
        switch tripStatus {
        case .INITIAL:
            return NSLocalizedString("Next trip", comment: "Next trip")
        case .WAITING,.READY, .RUNNING, .WAITING_CUSTOMER:
            return NSLocalizedString("Started trip", comment: "Started trip")
        case .END:
            return NSLocalizedString("Ended trip", comment: "Ended trip")
        case .TERMINATED:
            return NSLocalizedString("Canceled trip", comment: "Canceled trip")
        default:
            return NSLocalizedString("Unknown trip", comment: "Unknown trip")
        }
    }
    
    static func getTripStatusColor(tripStatus: TripStatus) -> UIColor{
        switch tripStatus {
        case .INITIAL:
            return UIColor(red: 68/255, green: 117/255, blue: 52/255, alpha: 1.0)
        case .WAITING,.READY, .RUNNING, .WAITING_CUSTOMER:
            return UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0)
        case .END:
            return UIColor.defaultAppColor()
        case .TERMINATED:
            return UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
        default:
            return UIColor.blackColor()
        }
    }
}

enum InvoiceStatus: Int {
    case UNKNOW = 0,
    INITIAL,
    PRE_WAITING,
    PRE_SUCCESS,
    POST_NOTCOMPLETED,
    PRE_COMPLETED,
    POST_COMPLETED,
    PRE_TERMINATED,
    POST_TERMINATED
    
    static func getInvoceStatusName(invoiceStatus: InvoiceStatus) -> String{
        switch invoiceStatus {
        case .INITIAL:
            return NSLocalizedString("INITIAL", comment: "Initial")
        case .PRE_WAITING:
            return NSLocalizedString("PREPAID - UNPAID", comment: "Started trip")
        case .PRE_SUCCESS:
            return NSLocalizedString("PREPAID - PAID", comment: "Initial")
        case .POST_NOTCOMPLETED:
            return NSLocalizedString("POSTPAID - UNPAID", comment: "Initial")
        case .PRE_COMPLETED:
            return NSLocalizedString("PREPAID - PAID", comment: "Initial")
        case .POST_COMPLETED:
            return NSLocalizedString("POSTPAID - PAID", comment: "Initial")
        case .PRE_TERMINATED:
            return NSLocalizedString("PREPAID - TERMINATED", comment: "Initial")
        case .POST_TERMINATED:
            return NSLocalizedString("POSTPAID - TERMINATED", comment: "Initial")
        default:
            return NSLocalizedString("Unknown status", comment: "Unknown trip")
        }
    }
}

class Trip: Mappable {
    
    var codeNumber = ""
    var originPlace: GMSCustomPlace!
    var destinationPlace: GMSCustomPlace!
    var destinationActualPlace: GMSCustomPlace!

    var startPoint = ""
    var endPointExpected = ""
    var endPointActual = ""
    
    //estimate
    var kilometerExpected = 0.0  //by km
    var kilometerActual = 0.0
    
    var durationExpected = 0    //by minute
    var durationActual = 0
    
    var priceExpected = 0
    var priceActual = 0     //by VND
    
    var startDate = NSDate()
    var transport = Transport()
    var vehicleId = 0 {
        didSet {
            transport = GlobalAppData.sharedObject.getTransportByVehicleID(vehicleId)
        }
    }
    
    var numberOfBike = 1
    var isRoundTrip = 0
    var paymentMethod = PaymentMethod.PostPaid   //2:post paid, 1:pre paid
  
    var promotion = PromotionResponseData() {
        didSet {
            self.calculatePrice()
        }
    }
    
    var coordinatesStart = TripCoordinatesData()
    var coordinatesEndExpected = TripCoordinatesData()
    var coordinatesEndActual = TripCoordinatesData()

    var driverNumber = ""
    var customerNumber = ""

    var invoiceStatus = InvoiceStatus.INITIAL
    var tripStatus = TripStatus.INITIAL
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        priceActual                <- map["priceActual"]
        coordinatesStart                <- map["coordinatesStart"]
        durationActual                <- map["durationActual"]
        startPoint                <- map["startPoint"]
        endPointExpected                <- map["endPointExpected"]
        coordinatesEndExpected                <- map["coordinatesEndExpected"]
        customerNumber                <- map["customerNumber"]
        codeNumber                <- map["codeNumber"]
        driverNumber                <- map["driverNumber"]
        isRoundTrip                <- map["isRoundTrip"]
        paymentMethod                <- map["paymentMethod"]
        coordinatesEndActual                <- map["coordinatesEndActual"]
        vehicleId                <- map["vehicleId"]
        invoiceStatus                <- map["invoiceStatus"]
        kilometerExpected                <- map["kilometerExpected"]
        durationExpected                <- map["durationExpected"]
        priceExpected                <- map["priceExpected"]
        kilometerActual                <- map["kilometerActual"]
        endPointActual                <- map["endPointActual"]
        tripStatus                <- map["tripStatus"]
        startDate       <- (map["startDate"], DateTransformExtension(dateFormat: DateTransformExtension.DateFormatStyle.DateTimeComplex))

        originPlace = GMSCustomPlace.init(name: startPoint, latitude: coordinatesStart.latitude, longitude: coordinatesStart.longitude, formattedAddress: startPoint)
        destinationPlace = GMSCustomPlace.init(name: endPointExpected, latitude: coordinatesEndExpected.latitude, longitude: coordinatesEndExpected.longitude, formattedAddress: endPointExpected)
        destinationActualPlace = GMSCustomPlace.init(name: endPointActual, latitude: coordinatesEndActual.latitude, longitude: coordinatesEndActual.longitude, formattedAddress: endPointActual)

    }
    
    func getKilometerExpectedString() -> String {
        if let kilometerExpected = kilometerExpected as Double? {
            let formatter = NSNumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 1
            return formatter.stringFromNumber(kilometerExpected)! + "km"
        }
    }
    
    func getDurationExpectedString() -> String {
        var durationExpectedString = "0" + NSLocalizedString("mins", comment: "min")
        if let durationExpected = durationExpected as Int? {
            durationExpectedString = String(durationExpected) + NSLocalizedString("mins", comment: "min")
            if (durationExpected >= 60) {
                durationExpectedString = String(durationExpected/60) + "h" + String(durationExpected%60) + NSLocalizedString("mins", comment: "min")
            }
        }
        return durationExpectedString
    }
    
    func initWithMaptask(mapTasks: MapTasks) {
        originPlace = GMSCustomPlace(name: NSLocalizedString(Constants.TSNAirportInfo.name, comment: "Tan Son Nhat Airport"), coordinate: mapTasks.originCoordinate)
//        destinationPlace = GMSCustomPlace(name: mapTasks.destinationAddress, coordinate: mapTasks.destinationCoordinate)
        kilometerExpected = Double(mapTasks.totalDistanceInMeters) / 1000.0
        let formatter = NSNumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        durationExpected = Int(mapTasks.totalDurationInSeconds / 60)
        self.calculatePrice()
    }
    
    func calculatePrice() {
        priceExpected = self.calculatePriceSub(vehicleId)
    }
    
    private func calculatePriceSub(vehicleId: Int) -> Int {
        var result = 0
        if let transport = GlobalAppData.sharedObject.getTransportByVehicleID(vehicleId) as Transport! {
            if let thresholds = transport.thresholds as [Threshold]! {
                if !thresholds.isEmpty {
                    let thresholdsReverse = thresholds.sort{(t1: Threshold, t2: Threshold) -> Bool in
                        t1.value > t2.value
                    }
                    var tempDistance = kilometerExpected
                    var tempPrice = 0.0
                    for threshold in thresholdsReverse {
                        if tempDistance <= Double(thresholds[1].value) {
                            tempPrice = tempPrice + Double(thresholds[0].money)
                            result = self.getFinalPrice(tempPrice)
                            return result
                        }
                        else if tempDistance >= Double(threshold.value) {
                            tempPrice = tempPrice + (tempDistance - Double(threshold.value)) * Double(threshold.money)
                            tempDistance = Double(threshold.value)
                        }
                    }
                }
            }
            let tmpPrice = kilometerExpected * Double(transport.price)
            result = getFinalPrice(tmpPrice)
        }
        return result
    }
    
    private func getFinalPrice(basePrice: Double) -> Int {
        let result1 = (basePrice + (Double(transport.percent)/100.0)*basePrice*Double(isRoundTrip)) * Double(numberOfBike)
        let afterPromotion = result1 - result1 * Double(promotion.percent)/100.0
        return Int(afterPromotion/1000 * 1000)
    }
    
    //get current expected price - current vehicleID
    func getPriceExpectedString() -> String {
        return getPriceExpectedString(vehicleId)
    }
    
    //get expected price by specific vehicleID
    func getPriceExpectedString(vehicleID: Int) -> String {
        self.calculatePrice()
        var priceExpectedString = String(Int(priceExpected/1000)) + "K"
        if let transport = GlobalAppData.sharedObject.getTransportByVehicleID(vehicleId) as Transport! {
            let fluctuationsPrice = Int(Double(priceExpected) * Double(transport.price_fluctuations)/100.0)
            let minPrice = priceExpected - fluctuationsPrice
            let maxPrice = priceExpected + fluctuationsPrice
            var fluctuationsPriceString = String(minPrice/1000) + "K ~ " + String(maxPrice/1000) + "K"
            
            if (minPrice >= 1000000) {
                let formatter = NSNumberFormatter()
                formatter.minimumFractionDigits = 2
                formatter.maximumFractionDigits = 2
                priceExpectedString = formatter.stringFromNumber(Double(priceExpected)/1000000.0)! + NSLocalizedString("M", comment: "M")
                fluctuationsPriceString = formatter.stringFromNumber(Double(minPrice)/1000000.0)! + NSLocalizedString("M", comment: "M") + " ~ " + formatter.stringFromNumber(Double(maxPrice)/1000000.0)! + NSLocalizedString("M", comment: "M")
            }
            
            if maxPrice == 0 {
                fluctuationsPriceString = "0 K"
            }
            
            if (self.isMotobikeTrip()) {
                priceExpectedString = fluctuationsPriceString
            }
        }
        return priceExpectedString
    }
    
    //get range of price of the same type of vehicle (Innova - Fortuner)
    func getPriceRangeExpectedString(vehicleID: Int) -> String {
        var minPrice = priceExpected
        var maxPrice = priceExpected
        if let transport = GlobalAppData.sharedObject.getTransportByVehicleID(vehicleID) as Transport! {
            if let transports = GlobalAppData.sharedObject.getTransportAvailableListBySlotNumber(transport.slot_number) as [Transport]? {
                for tran in transports {
                    let currentPrice = self.calculatePriceSub(tran.id)
                    if currentPrice < minPrice {
                        minPrice = currentPrice
                    }
                    if currentPrice > maxPrice {
                        maxPrice = currentPrice
                    }
                }
            }
        }
        if minPrice != maxPrice {
            var fluctuationsPriceString = String(minPrice/1000) + "K ~ " + String(maxPrice/1000) + "K"
            if (minPrice >= 1000000) {
                let formatter = NSNumberFormatter()
                formatter.minimumFractionDigits = 2
                formatter.maximumFractionDigits = 2
                fluctuationsPriceString = formatter.stringFromNumber(Double(minPrice)/1000000.0)! + NSLocalizedString("M", comment: "M") + " ~ " + formatter.stringFromNumber(Double(maxPrice)/1000000.0)! + NSLocalizedString("M", comment: "M")
            }
            return fluctuationsPriceString
        }
        return getPriceExpectedString()
    }
    
    func isMotobikeTrip() -> Bool {
        return (GlobalAppData.sharedObject.getTransportSlotNumber(vehicleId) == Constants.Transport.motobikeSlotNumber)
    }

    func getDurationString(duration: Int) -> String {
        var durationString = String(duration) + NSLocalizedString("mins", comment: "min")
        if (Int(duration) >= 60) {
            durationString = String(Int(duration)/60) + "h" + String(Int(duration)%60) + NSLocalizedString("mins", comment: "mins")
        }
        return durationString
    }
    
    func getPriceString(price: Int) -> String{
        var priceString =  String((price)/1000) + NSLocalizedString("K", comment: "K")
        if (price >= 1000000) {
            let formatter = NSNumberFormatter()
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
            priceString = (formatter.stringFromNumber(Double(price)/1000000.0) ?? "") + NSLocalizedString("M", comment: "M")
        }
        
        return priceString
    }
    
    func getDistanceString(distance: Double) -> String{
        let formatter = NSNumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        return (formatter.stringFromNumber(distance) ?? "") + "Km"
    }
    
    func getTransport() -> Transport! {
        return GlobalAppData.sharedObject.getTransportByVehicleID(vehicleId)
    }
}
