//
//  CustomMapView.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/9/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import GoogleMaps

protocol CustomMapViewDelegate{
    func didUpdateLocations (locations: [CLLocation])
    func didUpdateLocation (location: CLLocation?)
    func didChangeAuthorizationStatus (status: CLAuthorizationStatus)
}

class CustomMapView : GMSMapView, GMSMapViewDelegate, LocationManagerDelegate{
    
    private var locationManager: LocationManager?
    
    var polylineColor = UIColor.defaultAppColor()
    var polylineWidth : CGFloat = Constants.UI.MapView.StrokeWidth
    
    var isAutoDrawPolyline = false
    var isEnableCurrentMarker = true
    var currentLocation: CLLocation?
    
    var mapTasks = MapTasks()
    
    var customMapViewDelegate: CustomMapViewDelegate?
    
    var currentMarker = GMSMarker()
    
    var getBestLocationTimer: NSTimer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(Constants.TSNAirportInfo.latitude, longitude: Constants.TSNAirportInfo.longitude, zoom: Constants.UI.MapView.CameraZoom)
        
        self.camera = camera
        self.delegate = self
        self.settings.compassButton = true
        self.settings.myLocationButton = true
        self.myLocationEnabled = true
    }
    
    func initLocationManager(){
        locationManager = LocationManager()
        locationManager?.setObserver(self)
        
        if let timer = getBestLocationTimer{
            timer.invalidate()
        }
        
        getBestLocationTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(CustomMapView.getBestLocation), userInfo: nil, repeats: true)
    }
    
    func startUpdatingLocation(){
        locationManager?.startLocationTracking()
    }
    
    func stopUpdatingLocation(){
        locationManager?.startLocationTracking()
        
        if let timer = getBestLocationTimer{
            timer.invalidate()
        }
    }
    
    //MARK: - LocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        self.customMapViewDelegate?.didChangeAuthorizationStatus(status)
    }
    
    func didUpdateLocation(location: CLLocation!) {
        
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        /*
        if let locations = locations as? [CLLocation] {
            if let nextLocation = locations.last{
                if isAutoDrawPolyline && currentLocation != nil{
                    self.drawPath(currentLocation!, endLocation: nextLocation)
                }
            }
            
            currentLocation = locations.last
            if let coordinate = currentLocation?.coordinate{
                if isEnableCurrentMarker == true {
                    currentMarker.position = coordinate
                    currentMarker.map = self
                }
            }
        }
        */
    }
    /*
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.customMapViewDelegate?.didUpdateLocations(locations)
        
        if let nextLocation = locations.last{
            if isAutoDrawPolyline && currentLocation != nil{
                self.drawPath(currentLocation!, endLocation: nextLocation)
            }
        }
        
        currentLocation = locations.last
        if let coordinate = currentLocation?.coordinate{
            if isEnableCurrentMarker == true {
                currentMarker.position = coordinate
                currentMarker.map = self
            }
        }
    }
    */
    func drawPath(startLocation: CLLocation, endLocation: CLLocation){
        let path = GMSMutablePath()
        path.addCoordinate(CLLocationCoordinate2D(latitude: startLocation.coordinate.latitude, longitude: startLocation.coordinate.longitude))
        path.addCoordinate(CLLocationCoordinate2D(latitude: endLocation.coordinate.latitude, longitude: endLocation.coordinate.longitude))
        
        let polyline = GMSPolyline(path: path)
        polyline.map = self
        polyline.strokeWidth = self.polylineWidth
        polyline.strokeColor = self.polylineColor
    }
    
    func drawRoute(origin: CLLocationCoordinate2D, originAddress: String? = "", destination: CLLocationCoordinate2D, destinationAddress: String? = "", travelMode: TravelModes, completionHandler: ((status: String, success: Bool) -> Void)) {
        let startMarker = GMSMarker(position: origin)
        startMarker.icon = Utils.resizeImage(UIImage(named: "icon_diemdi") ?? UIImage(), targetSize: CGSize(width: 30, height: 30*206/150))
        startMarker.title = originAddress
        startMarker.map = self
        
        let destinationMarker = GMSMarker(position: destination)
        destinationMarker.icon = Utils.resizeImage(UIImage(named: "icon_diemden") ?? UIImage(), targetSize: CGSize(width: 30, height: 30*206/150))
        destinationMarker.title = destinationAddress
        destinationMarker.map = self
        
        self.mapTasks.getDirections(origin, destination: destination, waypoints: nil, travelMode: travelMode, completionHandler: { (status, success) -> Void in
            if success {
                let route = self.mapTasks.overviewPolyline["points"] as! String
                let path: GMSPath = GMSPath(fromEncodedPath: route)!
                let routePolyline = GMSPolyline(path: path)
                routePolyline.strokeColor = UIColor.defaultAppColor()
                routePolyline.strokeWidth = self.polylineWidth
                routePolyline.map = self
                completionHandler(status: status, success: true)
            }
            else {
                completionHandler(status: status, success: false)
            }
        })
    }
    
    func fixBoundCamera() {
        if self.mapTasks.originCoordinate != nil && self.mapTasks.destinationCoordinate != nil{
            let bound = GMSCoordinateBounds.init(coordinate: self.mapTasks.originCoordinate, coordinate: self.mapTasks.destinationCoordinate)
            self.moveCamera(GMSCameraUpdate.fitBounds(bound))
        }
    }
    
    func findPlaceByCoordinate(coordinate: CLLocationCoordinate2D, completionHandler: ((address: GMSAddress, success: Bool) -> Void)) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                completionHandler(address: address, success: true)
            } else {
                // Here the response is 'nil'
                completionHandler(address: GMSAddress(), success: false)
            }
        }
    }
    
    func addNewMarker(coordinate: CLLocationCoordinate2D, title: String) -> GMSMarker{
        let newMarker = GMSMarker()
        newMarker.position = coordinate
        newMarker.title = title
        newMarker.map = self
        return newMarker
    }
    
    func addPolyline(startCoordinate: CLLocationCoordinate2D, endCoordinate: CLLocationCoordinate2D){
        let path = GMSMutablePath()
        path.addLatitude(startCoordinate.latitude, longitude: startCoordinate.longitude)
        path.addLatitude(endCoordinate.latitude, longitude: endCoordinate.longitude)
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.defaultAppColor()
        polyline.strokeWidth = self.polylineWidth
        polyline.map = self
    }
    
    //Get Best Location Timer
    func getBestLocation(){
        if let location = locationManager?.getBestLocation(){
            self.customMapViewDelegate?.didUpdateLocation(location)
            
            if isAutoDrawPolyline && currentLocation != nil{
                self.drawPath(currentLocation!, endLocation: location)
            }
            
            currentLocation = location
            
            if let coordinate = currentLocation?.coordinate{
                if isEnableCurrentMarker == true {
                    currentMarker.position = coordinate
                    currentMarker.map = self
                }
            }
        }
    }
}