//
//  CircleMaskView.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/26/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//
import UIKit

class CircleMaskView{
    
    private var fillLayer = CAShapeLayer()
    var target: UIView
    private var circleButton : UIButton!
    
    var circleRect: CGRect = CGRectZero
    private var convertedCircleRect: CGRect = CGRectZero
    
    var didTapSubCircleView: (() -> ())?
    
    var guideMessageView: GuideMessageView!
    
    var fillColor: UIColor = UIColor.blackColor() {
        didSet {
            self.fillLayer.fillColor = self.fillColor.CGColor
        }
    }
    
    var radius: CGFloat? {
        didSet {
            self.draw()
        }
    }
    
    var opacity: Float = 0.3 {
        didSet {
            self.fillLayer.opacity = self.opacity
        }
    }
    
    init(drawIn: UIView, circleRect: CGRect) {
        self.target = drawIn
        self.circleRect = circleRect
    }
    
    func draw() {
        var rad: CGFloat = 0
        
        let size = target.frame.size
        if let r = self.radius {
            rad = r
        } else {
            rad = max(circleRect.size.height * 1.5, circleRect.size.width * 1.5)
        }
        
        let path = UIBezierPath(roundedRect: CGRectMake(0, 0, size.width, size.height), cornerRadius: 0.0)
        convertedCircleRect = CGRectMake(circleRect.midX - rad/2, circleRect.midY - rad/2, rad, rad)
        let circlePath = UIBezierPath(roundedRect: convertedCircleRect, cornerRadius: rad)
        path.appendPath(circlePath)
        path.usesEvenOddFillRule = true
        
        fillLayer.path = path.CGPath
        fillLayer.fillRule = kCAFillRuleEvenOdd
        fillLayer.fillColor = self.fillColor.CGColor
        fillLayer.opacity = self.opacity
        self.target.layer.addSublayer(fillLayer)
        
        circleButton = UIButton(frame: convertedCircleRect)
        circleButton.layer.cornerRadius = ((circleButton.height()) ?? 0 )/2
        circleButton.addTarget(self, action: #selector(CircleMaskView.handleTapCircleButton), forControlEvents: .TouchUpInside)
        target.addSubview(circleButton)
    }
    
    @objc func handleTapCircleButton(sender: UIButton? = nil) {
        if let action = self.didTapSubCircleView{
            action()
        }
    }
    
    func remove() {
        fillLayer.removeFromSuperlayer()
        removeGuideMessageView()
        circleButton.removeFromSuperview()
    }
    
    func addGuideMessageView(title: String?, message: String?){
        var frame = CGRectZero
        
        if convertedCircleRect.minY - target.frame.minY > GuideMessageView.DefaultHeight{
            frame = CGRectMake(10, convertedCircleRect.minY - GuideMessageView.DefaultHeight, target.frame.width - 20, GuideMessageView.DefaultHeight)
        }
        else{
            frame = CGRectMake(10, convertedCircleRect.maxY , target.frame.width - 20, GuideMessageView.DefaultHeight)
        }
        
        guideMessageView = GuideMessageView(frame: frame, title: title, message: message)
        target.addSubview(guideMessageView)
    }
    
    func removeGuideMessageView(){
        guideMessageView.removeFromSuperview()
    }
}

internal class GuideMessageView : UIView{
    static let DefaultHeight: CGFloat = 80

    var titleLabel = UILabel()
    var messageLabel = UILabel()
    
    init(frame: CGRect,title: String? = "", message: String? = "") {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.guideTourMessageColor()
        
        let contentPadding: CGFloat = 5
        titleLabel = UILabel(frame: CGRectMake(contentPadding,contentPadding,frame.width - 2*contentPadding,CGFloat.max))
        titleLabel.text = title
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont(name: (titleLabel.font?.fontName)!, size: 19.0)
        titleLabel.sizeToFit()
        titleLabel.frame = CGRectMake(contentPadding,contentPadding,frame.width - 2*contentPadding,titleLabel.frame.height)
        addSubview(titleLabel)
        
        messageLabel = UILabel(frame: CGRectMake(contentPadding,titleLabel.frame.maxY + contentPadding,frame.width - 2*contentPadding,CGFloat.max))
        messageLabel.textColor = UIColor.whiteColor()
        messageLabel.text = message
        messageLabel.textAlignment = NSTextAlignment.Center
        messageLabel.numberOfLines = 0
        messageLabel.font = UIFont(name: (messageLabel.font?.fontName)!, size: 13.0)
        messageLabel.sizeToFit()
        messageLabel.frame = CGRectMake(contentPadding,titleLabel.frame.maxY + contentPadding,frame.width - 2*contentPadding,messageLabel.frame.height)
        addSubview(messageLabel)
        
        self.frame = CGRectMake(frame.minX, frame.minY, frame.width, titleLabel.frame.height + messageLabel.frame.height + 4*contentPadding)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
