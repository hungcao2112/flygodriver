//
//  CustomNavigationBar.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/18/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit


class CustomNavigationBar: UIView {
    
    static let height: CGFloat = 80
    
    var title: String?{
        didSet{
            self.titleLabel?.text = title
        }
    }
    
    var titleLabel: UILabel?
    var backButton: UIButton?

    var backButtonHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel = {
            let label = UILabel(frame: CGRectMake(50, 0, frame.width - 100, frame.height))
            label.textColor = UIColor.whiteColor()
            label.textAlignment = NSTextAlignment.Center
            return label
            }()
        addSubview(titleLabel!)

        backButton = {
            let button = UIButton(frame: CGRectMake(5, (frame.height - 30)/2, 30, 30))
            button.setImage(UIImage(named: "icon-back"), forState: UIControlState.Normal)
            return button
            }()
        backButton?.addTarget(self, action: #selector(CustomNavigationBar.backButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        addSubview(backButton!)

        self.backgroundColor = UIColor.navigationBarBackgroundColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func backButtonTapped(sender: UIButton){
        if let handler = backButtonHandler{
            handler()
        }
    }
    
    func addLeftButtonBar(marginX: CGFloat, title: String? = "", target: UIViewController, selector: Selector) -> UIButton{
        let button = { () -> UIButton in
            let button = UIButton(frame: CGRectMake(marginX, (frame.height - 30)/2, 30, 30))
            button.setImage(UIImage(named: "icon-back"), forState: UIControlState.Normal)
            button.setTitle(title, forState: UIControlState.Normal)
            button.addTarget(target, action: selector, forControlEvents: UIControlEvents.TouchUpInside)
            return button
            }()
        self.addSubview(button)
        return button
    }
}