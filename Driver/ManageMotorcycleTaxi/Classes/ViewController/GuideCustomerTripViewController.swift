//
//  GuideCustomerTripViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/8/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class GuideCustomerTripViewController: BaseCollectionViewController {
    
    private var settingDatas = [SettingData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpDataSource()
        
        self.setUpCollectionView()
    }
    
    func setUpCollectionView(){
        self.collectionView.registerClass(InputCollectionViewCell.self, forCellWithReuseIdentifier: InputCollectionViewCell.cellIdentifier)
        
        self.collectionView.registerClass(TimeTripCollectionViewCell.self, forCellWithReuseIdentifier: TimeTripCollectionViewCell.cellIdentifier)
        
        self.collectionView!.registerNib(UINib(nibName: "DriverInfoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: DriverInfoCollectionViewCell.cellIdentifier)
        
        self.collectionView!.registerNib(UINib(nibName: "StatusTripCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: StatusTripCollectionViewCell.cellIdentifier)
        
        self.collectionView!.registerNib(UINib(nibName: "TimeTripCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: TimeTripCollectionViewCell.cellIdentifier)
        
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        
        self.collectionView.registerClass(CommentCollectionCell.self, forCellWithReuseIdentifier: CommentCollectionCell.cellIdentifier)
        
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
    }
    
    func setUpDataSource(){

        settingDatas.append(SettingData(identifier: DriverInfoCollectionViewCell.cellIdentifier, height: DriverInfoCollectionViewCell.defaultHeight))
        
        settingDatas.append(SettingData(identifier: StatusTripCollectionViewCell.cellIdentifier, height: StatusTripCollectionViewCell.defaultHeight))
        
        settingDatas.append(SettingData(identifier: TimeTripCollectionViewCell.cellIdentifier, height: TimeTripCollectionViewCell.defaultHeight))
        
        settingDatas.append(SettingData(title: NSLocalizedString("QUÝ KHÁCH CÓ HÀI LÒNG VỚI CHUYẾN ĐI?", comment: "QUÝ KHÁCH CÓ HÀI LÒNG VỚI CHUYẾN ĐI?"), identifier: IntroCollectionCell.cellIdentifier))
        
        settingDatas.append(SettingData(identifier: IntroCollectionCell.cellIdentifier))
        
        settingDatas.append(SettingData(identifier: CommentCollectionCell.cellIdentifier))
        
        settingDatas.append(SettingData(title: NSLocalizedString("SEND,CANCEL", comment: "SEND,CANCEL"), identifier: ActionCollectionCell.cellIdentifier))
    }
    
    //MARK: - CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:10.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let settingData = settingDatas[indexPath.row]
        
        return CGSizeMake(self.view.width() - 20, settingData.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settingDatas.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let settingData = settingDatas[indexPath.row]
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(settingData.identifier, forIndexPath: indexPath)
        
        if let inputCell = cell as? DriverInfoCollectionViewCell{
            return inputCell
        }
        else if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = settingData.title
            
            return inputCell
        }
        else if let inputCell = cell as? StatusTripCollectionViewCell{
            return inputCell
        }
        else if let inputCell = cell as? TimeTripCollectionViewCell{
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            //inputCell.actionButton.setTitle(settingData.title, forState: UIControlState.Normal)
            let titles = (settingData.title ?? "").characters.split{$0 == ","}.map(String.init)
            
            var actionButtonDatas = [ActionButtonData]()
            
            for title in titles{
                if title == NSLocalizedString("SEND", comment: "SEND"){
                    actionButtonDatas.append(ActionButtonData(title: title, backgroundColor: UIColor.defaulButtonColor(), titleColor: UIColor.whiteColor(),horizontalAlignment: UIControlContentHorizontalAlignment.Center))
                }
                else if title == NSLocalizedString("CANCEL", comment: "CANCEL"){
                    actionButtonDatas.append(ActionButtonData(title: title, backgroundColor: UIColor.secondaryButtonColor(), titleColor: UIColor.blackColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center))
                }
            }
            
            inputCell.actionButonDatas = actionButtonDatas
            
            return inputCell
        }
        
        return cell
    }
}