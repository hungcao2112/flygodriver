//
//  BaseCollectionViewCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/24/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

enum BorderEdge: Int {
    case Left = 0,
    Top,
    Right,
    Bottom
}

class BaseCollectionViewCell: UICollectionViewCell {
    
    var containerView = UIView()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.layer.borderWidth = 1.0
//        self.layer.borderColor = UIColor.defaultBorderColor().CGColor
        self.backgroundColor = UIColor.defaultBorderColor()

        containerView.frame = CGRectMake(1, 1, self.width() - 2, self.height() - 2)
        containerView.backgroundColor = UIColor.whiteColor()
        insertSubview(containerView, atIndex: 0)
//        addSubview(containerView)
//        self.insertSubview(containerView, atIndex: 0)
    }
    
    func roundCornersForCell(corners: UIRectCorner, radius: CGFloat) {
        containerView.roundCorners(corners, radius: radius - 2)
        self.roundCorners(corners, radius: radius)
    }
    
    func hideBoardEdge(borderEdge: BorderEdge){
        switch borderEdge {
        case .Left:
            containerView.frame = CGRectMake(0, 1, self.width() - 1, self.height() - 2)
            return
        case .Top:
            containerView.frame = CGRectMake(1, 0, self.width() - 2, self.height() - 1)
            return
        case .Right:
            containerView.frame = CGRectMake(1, 1, self.width() - 1, self.height() - 2)
            return
        case .Bottom:
            containerView.frame = CGRectMake(1, 1, self.width() - 2, self.height() - 1)
            return
        }
    }
    
    func fixContainerView() {
        containerView.frame = CGRectMake(1, 1, self.width() - 2, self.height() - 2)
    }
    
    func roundCornersForBottomCell() {
        self.roundCornersForCell([.BottomLeft, .BottomRight], radius: 5)
    }
}
