//
//  DriverTripViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/9/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import CoreLocation
import Firebase

protocol DriverChangeTitleDelegate {
    func didChangeTitle(title: String)
}

class DriverHomeViewController: BaseViewController {
    
    @IBOutlet weak var readyButton: UIButton!
    @IBOutlet weak var readyView: UIView!

    @IBOutlet weak var progressView: UAProgressView!
    @IBOutlet weak var numOfOrder: UILabel!
    @IBOutlet weak var myOrder: UILabel!
    
    @IBOutlet weak var refreshBtn: UIButton!
    var mapView: CustomMapView!
    
    var refreshTimer: NSTimer?
    var driver: User?
    var delegate: DriverChangeTitleDelegate?
    var isLoading = false

    var ref: FIRDatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("NOT READY", comment: "NOT READY")
        self.hideBackBarBtn(true)
        driver = GlobalAppData.sharedObject.getLoginUser()
        self.getAirportLocation()
        self.readyView.hidden = false
        self.initProgressView()
        self.initMapView()
        
        self.ref = FIRDatabase.database().reference()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.getProfiles()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopRefreshTimer()
    }
    
    func initMapView() {
        mapView = CustomMapView.init(frame: CGRectMake(0, 0, 0, 0))
        mapView.customMapViewDelegate = self
        mapView.initLocationManager()
        mapView.startUpdatingLocation()
    }
    
    func initProgressView() {
        self.progressView.tintColor = UIColor.defaultAppColor()
        self.progressView.borderWidth = 10.0;
        self.progressView.lineWidth = 10.0;
        self.progressView.fillOnTouch = true;
        refreshBtn.frame = self.progressView.frame
        refreshBtn.setTitle(NSLocalizedString("REFRESH", comment: "REFRESH"), forState: .Normal)
        self.progressView.centralView = refreshBtn
    }
    
    @IBAction func readyTapped(sender: AnyObject) {
        if self.checkLocationEnabled() == true {
            if let driverStatus = DriverStatus(rawValue: driver!.status!) {
                switch driverStatus {
                case .Idle:
                    if let driver = GlobalAppData.sharedObject.getLoginUser() as? Driver {
                        //TODO: test account
                        if driver.account == "0908930530" {
                            self.updateStatus(DriverStatus.Ready.rawValue)
                        }
                        else {
                            let airportPlace = GlobalAppData.sharedObject.getAirportPlace()
                            let maxDistance = airportPlace.distance
                            self.mapView.getBestLocation()
                            let currentLocation = mapView.currentLocation
                            print(currentLocation)
                            let distance = mapView.currentLocation?.distanceFromLocation(CLLocation.init(latitude: airportPlace.latitude, longitude: airportPlace.longitude))
                            
                            print(distance)
                            print(maxDistance)
                            if let distance = distance as CLLocationDistance? {
                                if (Int(distance) <= maxDistance) {
                                    self.updateStatus(DriverStatus.Ready.rawValue)
                                }
                                else {
                                    self.showMessageAtCenter(NSLocalizedString("You can only switch to ready status if you're at the airport!", comment: "You can only switch to ready status if you're at the airport!"))
                                }
                            }
                            else {
                                self.showMessageAtCenter(NSLocalizedString("Can't calculate distance", comment: "Can't calculate distance"))
                            }
                        }
                    }
                    break;
                case .Ready:

                    break;
                default:
                    break;
                }
            }
        }
    }
    
    @IBAction func refreshBtnTap(sender: UIButton) {
        self.getDriverQueue()
        self.getProfiles()
    }
    
    @IBAction func notReadyButtonTapped(sender: UIButton) {
        self.showAlertWindow(NSLocalizedString("Do you really want to stop receiving trips?", comment: "Do you really want to stop receiving trips?"), okAction: { () -> Void in
            self.updateStatus(DriverStatus.Idle.rawValue)
        })
    }
    
    func getAirportLocation() {
        firstly {
            return DriverManager.getAirport()
            }.then { requestResponse -> Void in
                if let requestResponse = requestResponse as? DriverGetAirportResponse {
                    GlobalAppData.sharedObject.setAirportPlace(requestResponse.data!)
                }
            }.always {

            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
            }
        }
    }
    
    func getProfiles() {
        if isLoading == false {
            self.showLoading()
            isLoading = true
        }
        firstly {
            return UserManager.getProfile()
            }.then { loginResponse -> Void in
                if let loginResponse = loginResponse as? LoginResponse <Driver> {
                    GlobalAppData.sharedObject.setLoginUser(loginResponse.data[0].user ?? User())
                    self.driver = loginResponse.data[0].user ?? User()
                    self.handleDriverStatus()
                }
            }.always {
                self.stopLoadingWithoutAnimation()
                self.isLoading = false
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }
    }
    
    func handleDriverStatus() {
        if self.driver != nil {
            let driverStatus: Int = (self.driver?.status)!
            switch driverStatus {
            case DriverStatus.Idle.rawValue:
                self.title = NSLocalizedString("NOT READY", comment: "NOT READY")
                self.delegate?.didChangeTitle(NSLocalizedString("NOT READY", comment: "NOT READY"))
                self.readyView.hidden = true
                self.stopRefreshTimer()
                break
            case DriverStatus.Ready.rawValue:
                self.readyView.hidden = false
                self.title = NSLocalizedString("WAITING ASSIGN TRIP", comment: "AVI Driver")
                self.delegate?.didChangeTitle(NSLocalizedString("WAITING ASSIGN TRIP", comment: "AVI Driver"))
                self.getDriverQueue()
                self.startRefreshTimer()
                break
            case DriverStatus.Waiting.rawValue:
                self.handleGetAssignTrip()
                break
            case DriverStatus.Accept.rawValue:
                self.handleGetCurrentTrip()
                break
            case DriverStatus.Driving.rawValue:
                self.handleGetCurrentTrip()
                break
            default:
                break
            }
        }
    }
    
    func startRefreshTimer() {
        self.stopRefreshTimer()
        refreshTimer = NSTimer.scheduledTimerWithTimeInterval(Constants.Timer.TimerInterval, target: self, selector: #selector(DriverHomeViewController.timerHandler), userInfo: nil, repeats: true)
    }
    
    func stopRefreshTimer() {
        if (refreshTimer != nil) {
            refreshTimer?.invalidate()
            refreshTimer = nil
        }
    }
    
    func timerHandler() {
        progressView.progress = progressView.progress + CGFloat(Constants.Timer.TimerInterval/Constants.Timer.TimerDriverRefreshDriverQueue)
        if progressView.progress > 0.99 {
            self.getDriverQueue()
            self.getProfiles()
        }
    }
    
    func updateStatus(status: Int) {
        if (self.isLoading == false) {
            self.isLoading = true
            self.showLoading()
        }
        firstly {
            return DriverManager.updateStatus(status)
            }.then { loginResponse -> Void in
                if let loginResponse = loginResponse as? LoginResponse <Driver> {
                    GlobalAppData.sharedObject.setLoginUser(loginResponse.data[0].user ?? User())
                    self.driver = loginResponse.data[0].user ?? User()
                    self.handleDriverStatus()
                }
            }.always {
                self.stopLoadingWithoutAnimation()
                self.isLoading = false
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }

    }
    
    func getDriverQueue() {
        progressView.progress = 0.0
        firstly {
            return DriverManager.getDriverQueue()
            }.then { response -> Void in
                if let response = response as? DriverGetDriverQueueResponse {
                    self.myOrder.text = String(Int((response.data?.yourPosition)!))
                    self.numOfOrder.text = String(Int((response.data?.totalQueue)!))
                }
            }.always {
                
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }
    }
    
    func handleGetAssignTrip() {
        if (self.isLoading == false) {
            self.showLoading()
            self.isLoading = true
        }
        firstly {
            return DriverManager.getAssignTrip("", status: "0")
            }.then { response -> Void in
                if let response = response as? TripArrayResponse {
                    if response.data.count > 0{
                        let assignTrip = response.data[0]
                        //GlobalAppData.sharedObject.setCurrentTrip(assignTrip)
                        self.stopLoadingWithoutAnimation()
                        self.navigateToDriverAcceptView()
                    }
                }
            }.always {
                self.stopLoadingWithoutAnimation()
                self.isLoading = false
            }.error { error -> Void in
                print(error)
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }
    }
    
    func handleGetCurrentTrip() {
        if (self.isLoading == false) {
            self.showLoading()
            self.isLoading = true
        }
        firstly {
            return DriverManager.getCurrentTrip()
            }.then { response -> Void in
                if let response = response as? TripArrayResponse {
                    let assignTrip = response.data![0]
                    //GlobalAppData.sharedObject.setCurrentTrip(assignTrip)
                    self.stopLoadingWithoutAnimation()
                    self.navigateToDriverTripView()
                }
            }.always {
                self.stopLoadingWithoutAnimation()
                self.isLoading = false
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }
    }
    
    func navigateToDriverAcceptView() {
        let controller = Storyboards.driver.instantiateViewControllerWithIdentifier(Constants.StoryBoard.Driver.DriverAcceptViewControllerID)
            as! DriverAcceptViewController
        controller.navigator = self.navigator
        if let nv = self.navigationController{
            print("navigateToDriverAcceptView: pushViewController");
            nv.pushViewController(controller, animated: true)
        }
    }
    
    func navigateToDriverTripView() {
        let controller = Storyboards.driver.instantiateViewControllerWithIdentifier(Constants.StoryBoard.Driver.DriverTripViewID)
            as! DriverTripViewController
        controller.navigator = self.navigator
        if let nv = self.navigationController{
            //nv.presentViewController(controller, animated: true, completion: nil)
            nv.pushViewController(controller, animated: true)
        }
    }
}

extension DriverHomeViewController : CustomMapViewDelegate{
    func didUpdateLocation(location: CLLocation?) {
        if let strongLocation = location{
            print("DriverHomeViewController: didUpdateLocations")
            
            let currentTime = Int64(NSDate().timeIntervalSince1970 * 1000)
            
            let locationDict = [
                "latitude": "\(strongLocation.coordinate.latitude)",
                "longitude": "\(strongLocation.coordinate.longitude)",
                "updatedAt": "\(currentTime)"
            ]
            
            if let codeNumber = driver?.code_number{
                self.ref.child("driverLocations/\(codeNumber)").setValue(locationDict)
            }
        }
    }
    
    func didUpdateLocations (locations: [CLLocation]){

    }
    
    func didChangeAuthorizationStatus(status: CLAuthorizationStatus) {
        print("DriverHomeViewController \(status)")
    }
}
