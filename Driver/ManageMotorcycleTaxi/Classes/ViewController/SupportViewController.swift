//
//  SupportViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/8/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class SupportViewController: BaseViewController {

    @IBOutlet weak var supportCallButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("SUPPORT", comment: "SUPPORT")

        supportCallButton.formatWithDefaultBackground()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func supportCallAction(sender: AnyObject) {
        if let url = NSURL(string: "tel://" + "0902764475"){
            UIApplication.sharedApplication().openURL(url)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
