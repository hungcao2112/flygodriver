//
//  CBCBankSelectionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 8/12/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class CBCBankSelectionCell: BaseCollectionViewCell {
    static let cellIdentifier = "CBCBankSelectionCellID"
    
    
    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clearColor()
        containerView.backgroundColor = UIColor.clearColor()
    }

}
