//
//  DriverViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/5/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class DriverViewController: BaseViewController , DriverTabViewDelegate, DriverChangeTitleDelegate {
    
    private var driverTabViewController : DriverTabViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideBackBarBtn(true)
        self.title = NSLocalizedString("NOT READY", comment: "NOT READY")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if let uiTabBarController = segue.destinationViewController as? DriverTabViewController{
            uiTabBarController.driverTabViewDelegate = self
            uiTabBarController.navigator = self.navigator
            driverTabViewController = uiTabBarController
            self.title = driverTabViewController?.tabBar.items?.first?.title
            uiTabBarController.driverDelegate = self
        }
    }
    
    func didSelectItem(tabBar: UITabBar, didSelectItem item: UITabBarItem){
        self.title = item.title
    }
    
    func didChangeTitle(title: String) {
        self.title = title
    }
}
