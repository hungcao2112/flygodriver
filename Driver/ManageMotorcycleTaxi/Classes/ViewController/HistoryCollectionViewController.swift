//
//  HistoryCollectionViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/23/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import UIKit

class HistoryCollectionViewController: BaseCollectionViewController {
    
    var tripHistories = [TripHistoryResponseData]()
    
    var isLoading = false
    var nextHistoryPage: Int = -1
    var curPage: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("HISTORY", comment: "HISTORY")
        
        
        self.setUpCollectionView()
        
        self.didScrollToBottom = { () -> Void in
            if self.nextHistoryPage != -1 && self.isLoading == false{
                print("didScrollToBottom")
                self.getTripHistories(String(self.nextHistoryPage), limit: "", order_by: "", order_mode: "")
            }
        }
        self.didScrollToTop = { () -> Void in
            if self.isLoading == false{
                print("didScrollToTop")
                self.getTripHistories("0", limit: "", order_by: "", order_mode: "")
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        self.getDataSource()
    }
    
    func setUpCollectionView(){
        self.collectionView!.registerNib(UINib(nibName: "HistoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: HistoryCollectionViewCell.cellIdentifier)
    }
    
    func getDataSource(){
        self.tripHistories = []
        self.getTripHistories("1", limit: "", order_by: "", order_mode: "")
    }
    
    func getTripHistories(page: String, limit: String, order_by: String, order_mode: String){

//        if page == -1 {
//            return
//        }
//        if (self.isLoading == false) {
//            self.isLoading = true
//            self.showLoading()
//            firstly {
//                return CustomerManager.getHistories(page)
//                }.then { [weak self] tripHistoryResponse -> Void in
//                    if let strongSelf = self{
//                        strongSelf.stopLoadingWithoutAnimation()
//                        strongSelf.isLoading = false
//                        if let tripHistoryResponse = tripHistoryResponse as? TripHistoryResponse{
//                            
//                            strongSelf.nextHistoryPage = tripHistoryResponse.data.nextPage ?? -1
//                            
//                            if (page == 0) {
//                                strongSelf.tripHistories = tripHistoryResponse.data.tripHistories ?? [Trip]()
//                            }
//                            else {
//                                strongSelf.tripHistories.appendContentsOf(tripHistoryResponse.data.tripHistories ?? [Trip]())
//                            }
//                            strongSelf.tripHistories = strongSelf.tripHistories.sort{$0.startDate > $1.startDate}
//                            strongSelf.collectionView.reloadData()
//                        }
//                    }
//                    
//                }.always {
//                    self.stopLoadingWithoutAnimation()
//                }.error { error -> Void in
//                    self.isLoading = false
//                    self.stopLoadingWithoutAnimation()
//                    if let errorType = error as NSError?{
//                        self.showMessage(errorType.localizedDescription)
//                    }
//            }
//        }
//
    }
    
    //MARK: - CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:10.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.width(), HistoryCollectionViewCell.defaultHeight)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tripHistories.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(HistoryCollectionViewCell.cellIdentifier, forIndexPath: indexPath)
        let tripHistory = tripHistories[indexPath.row]
        
        if let inputCell = cell as? HistoryCollectionViewCell{
//            inputCell.briefHistoryView.valueLabel.text = tripHistory.codeNumber
            inputCell.briefHistoryView.valueLabel.text = tripHistory.date_pickup
            inputCell.briefHistoryView.startAddressLabel.text = tripHistory.address_a
            inputCell.briefHistoryView.endAddressLabel.text = tripHistory.address_b
            
            inputCell.briefHistoryView.titleLabel.text  = ""
            inputCell.briefHistoryView.titleLabel.text = tripHistory.status_text
            inputCell.briefHistoryView.titleLabel.textColor = UIColor.redColor()
//            if let tripStatus = tripHistory.tripStatus {
//                inputCell.briefHistoryView.titleLabel.text = TripStatus.getTripStatusName(tripStatus)
//                inputCell.briefHistoryView.titleLabel.textColor = TripStatus.getTripStatusColor(tripStatus)
//                if (tripStatus == TripStatus.END) {
//                    inputCell.briefHistoryView.endAddressLabel.text = tripHistory.endPointActual
//                }
//            }
            
            return inputCell
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let tripHistory = tripHistories[indexPath.row]
        //self.navigator.navigateToHistoryDetailView(tripHistory)
//        if let tripStatus = TripStatus(rawValue: tripHistory.tripStatus ?? 0){

//        let tripStatus = tripHistory.tripStatus
//            switch tripStatus {
//            case .INITIAL:
//                let historyDetailVC = OrderHistoryDetailViewController()
//                historyDetailVC.delegate = self
//                historyDetailVC.tripHistory = tripHistory
//                historyDetailVC.navigator = self.navigator
//                self.navigator.navigationController.pushViewController(historyDetailVC, animated: true)
//            case .WAITING, .READY, .RUNNING, .WAITING_CUSTOMER:
//                let vc = self.navigator.navigateToCustomerTripView()
//                vc.tripHistory = tripHistory
//                break
//            case .END:
//                self.navigator.navigateToHistoryDetailView(tripHistory)
//                break
//            case .TERMINATED:
//                break
//            default:
//                break
//            }
    }
}

extension HistoryCollectionViewController : OrderHistoryDetailViewDelegate{
    func didCacelTrip(isSuccess: Bool) {
//        self.getDataSource()
    }
}
