//
//  CustomerBookingPaymentViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/24/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import PromiseKit

class CustomerBookingPaymentViewController: BaseCollectionViewController {

    var trip: Trip!
    var cellIdentifiers = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("PAYMENT", comment: "BOOKING CONFIRM")
        self.setUpCollectionView()
        self.setUpDataSource()
        // Do any additional setup after loading the view.
        
        //TODO: for testing
//        self.trip.priceExpected = 5001
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpDataSource() {
        cellIdentifiers.append(IntroCollectionCell.cellIdentifier)
        cellIdentifiers.append(ActionCollectionCell.cellIdentifier)
        //start date - current date >= 24h => Pre-paid
        if (NSDate().numOfHoursToDate(trip.startDate) < Constants.Trip.PaymentMethodTimeline) {
            cellIdentifiers.append(ActionCollectionCell.cellIdentifier)
        }
    }
    
    func setUpCollectionView(){
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
    }
    
    // MARK: CollectionView delegate
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellIdentifiers.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.width() - 20, Constants.UI.CollectionCellHeight.NormalCellHeight)
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let rowNumber = indexPath.row
        let cellIdentifier = cellIdentifiers[rowNumber]
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = NSLocalizedString("Please pay for booked trips 24 hours early", comment: "Please pay for booked trips 24 hours early")
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            switch rowNumber {
            case 1:     //pre-paid
                inputCell.actionButonDatas = [ActionButtonData(title: NSLocalizedString("PAY IN ADVANCE", comment: "PAY IN ADVANCE"), backgroundColor: UIColor.defaulButtonColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                    self.handlePrePaidPayment()
                })
                ]
                break
            default:    //post-paid
                inputCell.actionButonDatas = [ActionButtonData(title: NSLocalizedString("PAY LATER", comment: "PAY LATER"), backgroundColor: UIColor.defaultAppYellowColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                    self.handlePostPaidPayment()
                })
                ]
                break
            }
 
            return inputCell
        }
        return cell
    }
    
    func handlePrePaidPayment() {
        trip.paymentMethod = PaymentMethod.PrePaid
        if (trip.codeNumber != "" && trip.invoiceStatus == InvoiceStatus.PRE_WAITING) {
            let controler = self.navigator.navigateToCustomerBankSelectionView()
            controler.trip = self.trip
        }
        else {
            self.showLoading()
            firstly {
                return TripManager.bookTickets(trip)
                }.then { requestResponse -> Void in
                    if let requestResponse = requestResponse as? TripArrayResponse{
                        print(requestResponse)
                        if let responseTrip = requestResponse.data[0] as Trip! {
                            self.trip = responseTrip
                            let controler = self.navigator.navigateToCustomerBankSelectionView()
                            controler.trip = self.trip
                        }
                    }
                }.always {
                    self.stopLoadingWithoutAnimation()
                }.error { error -> Void in
                    if let errorType = error as NSError?{
                        self.handleError(errorType)
                    }
            }
        }
    }
    
    func handlePostPaidPayment() {
        if (trip.codeNumber != "" && trip.invoiceStatus == InvoiceStatus.PRE_WAITING) {
            self.showAlertWindow(NSLocalizedString("Change payment method", comment: ""), message: NSLocalizedString("Do you want to change this trip to postpaid?", comment: ""), okAction: { () -> Void in
                self.showLoading()
                firstly {
                    return TripManager.updateMethodTickets(self.trip.codeNumber, method: PaymentMethod.PostPaid)
                    }.then { requestResponse -> Void in
                        if let requestResponse = requestResponse as? SingleTripResponse{
                            print(requestResponse)
                            if let responseTrip = requestResponse.data?.trip as Trip! {
                                self.showMessage(NSLocalizedString("Booking successfully", comment: "Logout successfully"))
                                self.trip = responseTrip
                                let bookingCompleteVC = self.navigator.navigateToCustomerBookingCompleteView()
                                bookingCompleteVC.trip = self.trip
                            }
                        }
                    }.always {
                        self.stopLoadingWithoutAnimation()
                    }.error { error -> Void in
                        if let errorType = error as NSError?{
                            self.handleError(errorType)
                        }
                }
                }, cancelAction: {})
        }
        else {
            trip.paymentMethod = PaymentMethod.PostPaid
            self.showLoading()
            firstly {
                return TripManager.bookTickets(trip)
                }.then { requestResponse -> Void in
                    if let requestResponse = requestResponse as? TripArrayResponse{
                        print(requestResponse)
                        if let responseTrip = requestResponse.data[0] as Trip! {
                            self.showMessage(NSLocalizedString("Booking successfully", comment: "Logout successfully"))
                            self.trip.codeNumber = responseTrip.codeNumber
                            self.trip.paymentMethod = responseTrip.paymentMethod
                            self.trip.invoiceStatus = responseTrip.invoiceStatus
                            let bookingCompleteVC = self.navigator.navigateToCustomerBookingCompleteView()
                            bookingCompleteVC.trip = self.trip
                        }
                    }
                }.always {
                    self.stopLoadingWithoutAnimation()
                }.error { error -> Void in
                    if let errorType = error as NSError?{
                        self.handleError(errorType)
                    }
            }
        }
    }
}
