//
//  ForgotPasswordViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/18/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import UIKit
import Alamofire

class ForgotPasswordViewController: BaseCollectionViewController {
    
    let StartInputRow = 1
    let NumberOfInputRow = 1
    
    var phoneNumberTextField: UITextField?
    
    private var settingDatas = [SettingData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("FORGOT PASSWORD", comment: "FORGOT PASSWORD")
        
        self.setUpDataSource()
        
        self.setUpCollectionView()
        
        self.view.backgroundColor = UIColor.defaultBackgroundColor()
    }
    
    func setUpCollectionView(){
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(InputCollectionViewCell.self, forCellWithReuseIdentifier: InputCollectionViewCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
    }
    
    func setUpDataSource(){
        settingDatas.append(SettingData(title: NSLocalizedString("Please enter your phone numer:", comment: "Please enter your phone numer:"), identifier: IntroCollectionCell.cellIdentifier))
        
        settingDatas.append(SettingData(title: NSLocalizedString("Mobile phone", comment: "Mobile phone"), placeHolder: NSLocalizedString("Mobile phone", comment: "Mobile phone"), identifier: InputCollectionViewCell.cellIdentifier))

        settingDatas.append(SettingData(title: NSLocalizedString("CONTINUE", comment: "CONTINUE"), identifier: ActionCollectionCell.cellIdentifier, height: ActionCollectionCell.defaultHeight + 40))
    }
    
    //MARK: - CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let settingData = settingDatas[indexPath.row]
        
        return CGSizeMake(self.view.width() - 20, settingData.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settingDatas.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let settingData = settingDatas[indexPath.row]
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(settingData.identifier, forIndexPath: indexPath)
        
        if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = settingData.title
            inputCell.introLabel.textAlignment = NSTextAlignment.Left
            return inputCell
        }
        else if let inputCell = cell as? InputCollectionViewCell{
            if NumberOfInputRow == 1 {
                inputCell.roundCornersForCell([.BottomLeft, .BottomRight, .TopLeft, .TopRight], radius: 5)
            }
            
            inputCell.titleLabel.text = settingData.title
            inputCell.inputTextField.placeholder = settingData.placeHolder
            inputCell.inputTextField.delegate = self
            phoneNumberTextField = inputCell.inputTextField
            phoneNumberTextField?.keyboardType = UIKeyboardType.PhonePad
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            if settingData.title ==  NSLocalizedString("CONTINUE", comment: "CONTINUE"){
                inputCell.actionButonDatas = [
                    ActionButtonData(title: settingData.title, backgroundColor: UIColor.defaulButtonColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                        self.activeTextField?.resignFirstResponder()
                        self.handleGetCode(account: self.phoneNumberTextField?.text ?? "")
                    })
                ]
            }
            
            return inputCell
        }
        
        return cell
    }
    
    //MARK: - Methods
    
    private func handleGetCode(account account: String){
        if !account.isValidPhone(){
            self.showMessage(NSLocalizedString("Please enter a valid phone numer", comment: "Please enter a valid phone numer"))
            return
        }
        self.showLoading()
        firstly {
            return AuthManager.getCode(account)
            }.then { codeResponse -> Void in
                if let codeResponse = codeResponse as? CodeResponse{
                    let vc = self.navigator.navigateToMobileVerificationView(codeResponse.data![0] ?? CodeResponseData())
                   
                     vc.title = NSLocalizedString("FORGOT PASSWORD", comment: "FORGOT PASSWORD")
                }
                
            }.always {
                self.stopLoading()
            }.error { error -> Void in
//                if let _ = error as NSError?{
//                    //self.handleError(errorType)
//                }
                self.showMessage(NSLocalizedString("Get code failed", comment: "Get code failed"))
                
        }
    }
}
