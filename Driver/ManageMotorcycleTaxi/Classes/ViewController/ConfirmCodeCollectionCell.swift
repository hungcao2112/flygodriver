//
//  ConfirmCodeCollectionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/7/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class ConfirmCodeCollectionCell: UICollectionViewCell {
    
    static let cellIdentifier = "ConfirmCodeCollectionCellID"
    static let defaultHeight: CGFloat = 100
    
    let numberOfConfirmCode = 4
    
    var confirmCodeViews = [ConfirmCodeView]()
    
    private var containerView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        
        containerView.frame = CGRectMake(0, 0, self.width(), self.height())
        containerView.backgroundColor = UIColor.clearColor()
        addSubview(containerView)
        
        let codeFirmCodeWidth = self.width()/CGFloat(numberOfConfirmCode)
        
        for index in 0...numberOfConfirmCode - 1{
            let confirmCodeView = ConfirmCodeView(frame: CGRectMake(codeFirmCodeWidth*CGFloat(index), 0, codeFirmCodeWidth, self.height()))
            confirmCodeView.tag = index
            confirmCodeView.didEnterCode = { confirmCodeView -> Void in
                self.nextSelectFirstResponseOn(currentConfirmCodeView: confirmCodeView)
            }
            containerView.addSubview(confirmCodeView)
            confirmCodeViews.append(confirmCodeView)
        }
    }
    
    func nextSelectFirstResponseOn(currentConfirmCodeView currentConfirmCodeView: ConfirmCodeView){
        currentConfirmCodeView.resignFirstResponder()
        for confirmCodeView in confirmCodeViews{
            if confirmCodeView.tag > currentConfirmCodeView.tag && confirmCodeView.codeTextField.text?.characters.count == 0{
                confirmCodeView.codeTextField.becomeFirstResponder()
                break
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getCode() -> String{
        var code = ""
        for confirmCodeView in confirmCodeViews{
            code = code + confirmCodeView.codeTextField.text! ?? ""
        }
        
        return code
    }
    
}

internal class ConfirmCodeView: UIView, UITextFieldDelegate{
    var codeTextField = UITextField()
    var bottomLineView =  UIView()
    
    var didEnterCode : ((ConfirmCodeView) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        codeTextField.frame = CGRectMake(0, 0, frame.width, frame.height - 1)
        codeTextField.textColor = UIColor.blackColor()
        codeTextField.textAlignment = NSTextAlignment.Center
        codeTextField.keyboardType = UIKeyboardType.NumberPad
        codeTextField.placeholder = "X"
        codeTextField.delegate = self
        codeTextField.addTarget(self, action: #selector(ConfirmCodeView.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        addSubview(codeTextField)
        
        bottomLineView.frame = CGRectMake(frame.width/4, frame.height - 10, frame.width/2, 1)
        bottomLineView.backgroundColor = UIColor.defaultBorderColor()
        addSubview(bottomLineView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func textFieldDidChange(textField: UITextField){
        if textField.text?.characters.count > 0{
            if let action = didEnterCode{
                action(self)
            }
        }
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.characters.count == 0 || string.isEmpty{
            return true
        }

        return false
    }
}