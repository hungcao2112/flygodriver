//
//  RegisterViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/6/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import DropDown

enum RegisterMode: Int{
    case Normal = 0,
    Facebook,
    Google
}

class RegisterViewController: BaseCollectionViewController {
    private var numberOfInputRow = 6
    
    private var settingDatas = [SettingData]()
    
    enum InputRow: Int{
        case FullName = 0,
        Email,
        MobilePhone,
        Password,
        ConfirmPassword,
        selectVehicle
    }
    
    var fullNameTextField: UITextField?
    var emailTextField: UITextField?
    var mobilePhoneTextField: UITextField?
    var passwordTextField: UITextField?
    var confirmPasswordTextField: UITextField?
    var vehicleSelectionTextField: UITextField?
    
    var vehicleDropdown = DropDown()
    var vehicleSelected = ""
    var vehicleIndex = ""
    
    var registerMode = RegisterMode.Normal
    var facebookUser = User()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if registerMode == .Normal{
            self.title = NSLocalizedString("REGISTER", comment: "REGISTER")
            numberOfInputRow = 6
        }
        else{
            self.title = NSLocalizedString("VERIFY PROFILE", comment: "VERIFY PROFILE")
            numberOfInputRow = 3
        }
        
        self.setUpDataSource()
        
        self.setUpCollectionView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
    }
    
    func setUpCollectionView(){
        self.collectionView.registerClass(InputCollectionViewCell.self, forCellWithReuseIdentifier: InputCollectionViewCell.cellIdentifier)
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"SeperateCollectionCell", bundle: nil), forCellWithReuseIdentifier: SeperateCollectionCell.cellIdentifier)
    }
    
    func setUpDataSource(){
        settingDatas.append(SettingData(title: NSLocalizedString("Full Name", comment: "Full Name"), placeHolder: NSLocalizedString("Full Name", comment: "Full Name"), identifier: InputCollectionViewCell.cellIdentifier))
        settingDatas.append(SettingData(title: NSLocalizedString("Email", comment: "Email"), placeHolder: NSLocalizedString("Email", comment: "Email"), identifier: InputCollectionViewCell.cellIdentifier))
        settingDatas.append(SettingData(title: NSLocalizedString("Mobile Phone", comment: "Mobile Phone"), placeHolder: NSLocalizedString("Mobile Phone", comment: "Mobile Phone"), identifier: InputCollectionViewCell.cellIdentifier))
        
        if registerMode == .Normal{
            settingDatas.append(SettingData(title: NSLocalizedString("Password", comment: "Password"), placeHolder: NSLocalizedString("Password", comment: "Password"), identifier: InputCollectionViewCell.cellIdentifier))
            settingDatas.append(SettingData(title: NSLocalizedString("Confirm", comment: "Confirm"), placeHolder: NSLocalizedString("Confirm", comment: "Confirm"), identifier: InputCollectionViewCell.cellIdentifier))
            settingDatas.append(SettingData(title: NSLocalizedString("Select vehicle", comment: "Select vehicle"), placeHolder: NSLocalizedString("Select vehicle", comment: "Select vehicle"), identifier: InputCollectionViewCell.cellIdentifier))
        }
        settingDatas.append(SettingData(title: NSLocalizedString("Confirmation code will be sent to your mobile phone number.", comment: "Confirmation code will be sent to your mobile phone number."), identifier: IntroCollectionCell.cellIdentifier))
        
        if registerMode == .Normal{
            settingDatas.append(SettingData(title: NSLocalizedString("REGISTER", comment: "REGISTER"), identifier: ActionCollectionCell.cellIdentifier))
            settingDatas.append(SettingData(identifier: SeperateCollectionCell.cellIdentifier, height: InputCollectionViewCell.defaultHeight))
            settingDatas.append(SettingData(title: NSLocalizedString("FACEBOOK LOGIN", comment: "FACEBOOK LOGIN"), identifier: ActionCollectionCell.cellIdentifier))
        }else{
            settingDatas.append(SettingData(title: NSLocalizedString("CONFIRM", comment: "CONFIRM"), identifier: ActionCollectionCell.cellIdentifier))
        }

    }
    
    //MARK: - CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let settingData = settingDatas[indexPath.row]
        return CGSizeMake(self.view.width() - 20, settingData.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settingDatas.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let settingData = settingDatas[indexPath.row]
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(settingData.identifier, forIndexPath: indexPath)
        
        
        if let inputCell = cell as? InputCollectionViewCell{
            if indexPath.row == 0{
                inputCell.hideBoardEdge(BorderEdge.Bottom)
                inputCell.roundCornersForCell([.TopLeft, .TopRight], radius: 5)
            }else if indexPath.row == numberOfInputRow - 1{
                inputCell.roundCornersForCell([.BottomLeft, .BottomRight], radius: 5)
            }
            else{
                inputCell.hideBoardEdge(BorderEdge.Bottom)
            }
            inputCell.titleLabel.text = settingData.title
            inputCell.inputTextField.placeholder = settingData.placeHolder
            inputCell.inputTextField.returnKeyType = UIReturnKeyType.Done
            inputCell.inputTextField.delegate = self
            
            if let inputRow = InputRow(rawValue: indexPath.row){
                switch inputRow{
                case InputRow.FullName:
                    fullNameTextField = inputCell.inputTextField
                    if registerMode == .Facebook{
                        fullNameTextField?.text = facebookUser.firstName
                    }
                    break
                case InputRow.Email:
                    emailTextField = inputCell.inputTextField
                    mobilePhoneTextField?.keyboardType = UIKeyboardType.EmailAddress
                    if registerMode == .Facebook{
                        emailTextField?.text = facebookUser.email
                    }
                    break
                case InputRow.MobilePhone:
                    mobilePhoneTextField = inputCell.inputTextField
                    mobilePhoneTextField?.keyboardType = UIKeyboardType.PhonePad
                    break
                case InputRow.Password:
                    passwordTextField = inputCell.inputTextField
                    inputCell.inputTextField.autocapitalizationType = .None
                    inputCell.inputTextField.secureTextEntry = true
                    break
                case InputRow.ConfirmPassword:
                    confirmPasswordTextField = inputCell.inputTextField
                    inputCell.inputTextField.autocapitalizationType = .None
                    inputCell.inputTextField.secureTextEntry = true
                    break
                case InputRow.selectVehicle:
                    vehicleSelectionTextField = inputCell.inputTextField
                    vehicleDropdown.anchorView = inputCell.inputTextField
                    inputCell.inputTextField.text = vehicleSelected
                    inputCell.inputTextField.addTarget(self, action: #selector(showVehicleDropdown), forControlEvents: .TouchDown)
                    
                }
            }
            
            return inputCell
        }
        else if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = settingData.title
            
            return inputCell
        }
        else if let inputCell = cell as? SeperateCollectionCell{
            inputCell.backgroundColor = UIColor.clearColor()
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            if settingData.title ==  NSLocalizedString("REGISTER", comment: "REGISTER")
            || settingData.title ==  NSLocalizedString("CONFIRM", comment: "CONFIRM"){
                inputCell.actionButonDatas = [ActionButtonData(title: settingData.title, backgroundColor: UIColor.defaulButtonColor(), fontSize: 14.0, horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                    
                    self.view.endEditing(true)
                    
                    self.handleRegister(phone: self.mobilePhoneTextField!.text ?? "", fullname: self.fullNameTextField!.text ?? "", email: self.emailTextField!.text ?? "", password: self.passwordTextField!.text ?? "", driver: "1", deviceToken: "\(Context.getPushKey())", fcmToken: "AIzaSyBLwUFyKFOv2jGLSObuOf19YHPw0hpYkjg", vehicle: "\(self.vehicleIndex ?? "")")
                })
                ]
            }else if settingData.title ==  NSLocalizedString("FACEBOOK LOGIN", comment: "FACEBOOK LOGIN"){
                inputCell.showLeftImage(UIImage(named: "icon_facebook") ?? UIImage())
//                inputCell.actionButonDatas = [
//                    ActionButtonData(title: settingData.title, backgroundColor: UIColor.facebookButtonBackgroundColor(), fontSize: 14.0, horizontalAlignment: UIControlContentHorizontalAlignment.Center,
//                        action: { () -> Void in
//                            FacebookManager.sharedInstance.loginToFacebook(
//                                {(facebookId, facebookToken) -> Void in
//                                    self.handleFacebookLogin(facebookId, socialToken: facebookToken)
//                                }, onError: { error -> Void in
//                                    
//                            })
//                    })
//                ]
            }
            return inputCell
        }
        
        return cell
    }
    func showVehicleDropdown(){
        let dataSource = ["Xe ôm", "Xe 5 chỗ", "Xe 7 chỗ", "Xe 16 chỗ"]
        vehicleDropdown.dataSource = dataSource
        vehicleDropdown.show()
        vehicleDropdown.selectionAction = {[weak self](index: Int, item: String) in
            if let strongSelf = self{
                strongSelf.vehicleSelected = item ?? ""
                strongSelf.vehicleIndex = String(index + 1)
                strongSelf.collectionView.reloadItemsAtIndexPaths([NSIndexPath(forItem: 5, inSection: 0)])
            }
        }
    }
    
    //MARK: - Methods
    private func handleRegister(phone phone: String, fullname: String, email: String, password: String, driver: String, deviceToken: String, fcmToken: String, vehicle: String){
//        if !account.isValidAccount(){
//            self.showMessage(NSLocalizedString("Invalid account", comment: "Invalid account"))
//            return
//        }
        if fullname.isEmpty{
            self.showMessage(NSLocalizedString("Please enter your name", comment: "Please enter your name"))
            return
        }
        self.showLoading()
        if registerMode == .Facebook{
//            firstly {
//                return self.socialRegister(name: fullname, /*account: account,*/ password: password, phone: phone, email: email, socialId: facebookUser.facebookId, socialToken: facebookUser.facebookToken, socialType: .Facebook)
//                }.then { registerResponse -> Void in
//                    if let registerResponse = registerResponse as? RegisterResponse{
//                        self.showMessage(NSLocalizedString("Register successfully", comment: "Register successfully"))
//                        let vc = self.navigator.navigateToMobileVerificationView(registerResponse.data ?? RegisterResponseData())
//                        vc.registerMode = .Facebook
//                        vc.title = NSLocalizedString("ACTIVATION", comment: "ACTIVATION")
//                    }
//                    
//                }.always {
//                    self.stopLoading()
//                }.error { error -> Void in
//                    if let errorType = error as NSError?{
//                        self.handleError(errorType)
//                    }
//                    self.showMessage(NSLocalizedString("Register failed", comment: "Register failed"))
//                    
//            }
        }
        else{
            firstly {
                return self.register(phone, fullname: fullname, email: email, password: password, driver: driver, deviceToken: deviceToken, fcmToken: fcmToken, vehicle: vehicle)
                }.then { registerResponse -> Void in
                    if let registerResponse = registerResponse as? RegisterResponse{
                        self.showMessage(NSLocalizedString("Register successfully", comment: "Register successfully"))
                        print("id = \(registerResponse.data!.id), code = \(registerResponse.data!.active_code)")
                        self.showAlertWindowWithOK(NSLocalizedString("Almost done", comment: "Almost done"), message: NSLocalizedString("Please wait for server to confirm your registration", comment: "Please wait for server to confirm your registration"), okAction: {
                            self.navigator.navigateToLoginView()
                        })
                    }
                    
                }.always {
                    self.stopLoading()
                }.error { error -> Void in
                    if let errorType = error as NSError?{
                        self.handleError(errorType)
                    }
                    self.showMessage(NSLocalizedString("Register failed", comment: "Register failed"))
                    
            }
        }
        
    }
    
    private func register(phone: String, fullname: String, email: String, password: String, driver: String, deviceToken: String, fcmToken: String, vehicle: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["phone": phone, "fullname": fullname, "email":email, "password": password, "driver": driver, "deviceToken": deviceToken, "fcmToken": fcmToken, "vehicle": vehicle]
        
        return Promise{ fulfill, reject in
            DriverService.register(parameters, completion: {
                [weak self] (response) in
                if let _ = self {
                    if response.result.isSuccess{
                        print(response)
                        if response.response?.statusCode == 200 {
                            if let registerResponse = Mapper<RegisterResponse>().map(response.result.value) {
                                fulfill(registerResponse)
                            }
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }
    private func socialRegister(name name: String, account: String, password: String, phone: String, email: String, socialId: String, socialToken: String, socialType: RegisterMode) -> Promise<AnyObject> {
        var parameters: [String: AnyObject]
        switch socialType {
        case .Facebook:
            parameters = ["name" : name, "phone": phone, "email": email, "password": password, "type" : "FACEBOOK","socialId" : socialId,"socialToken" : socialToken]
        default:
            parameters = ["fullName" : name,"account": account, "password": password, "phone": phone, "email": email]
            break
        }
        
        return Promise{ fulfill, reject in
            CustomerService.socialRegister(parameters, completion: {
                [weak self] (response) in
                if let _ = self {
                    if response.result.isSuccess{
                        print(response)
                        if response.response?.statusCode == 200 {
                            if let registerResponse = Mapper<RegisterResponse>().map(response.result.value) {
                                fulfill(registerResponse)
                            }
                            
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }
//    private func handleFacebookLogin(socialId: String, socialToken: String){
//        self.showLoading()
//        firstly {
//            return  CustomerManager.socialLogin(socialId, socialToken: socialToken, socialType: CustomerManager.SocialType.FACEBOOK)
//            }.then { loginResponse -> Promise<AnyObject> in
//                if let loginResponse = loginResponse as? LoginResponse <User>{
//                    if loginResponse.code == 404{
//                        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"], tokenString: socialToken, version: nil, HTTPMethod: "GET")
//                        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
//                            if(error == nil)
//                            {
//                                //print("result \(result)")
//                                let facebookUser = User()
//                                facebookUser.facebookId = socialId
//                                facebookUser.facebookToken = socialToken
//                                if let email = result["email"] as? String{
//                                    facebookUser.email = email
//                                }
//                                if let name = result["name"] as? String{
//                                    facebookUser.firstName = name
//                                }
//                                self.navigator.navigateToRegisterView(.Facebook, facebookUser: facebookUser)
//                            }
//                            else
//                            {
//                                self.navigator.navigateToRegisterView()
//                            }
//                        })
//                    }
//                    else if loginResponse.code == 200{
//                        if (loginResponse.data?.isBelongToGroup(UserType.Customer) == true) {
//                            GlobalAppData.sharedObject.setLoginUser(loginResponse.data?.user ?? User())
//                            Context.setToken(loginResponse.data?.token ?? "")
//                            Context.setAuthKey(loginResponse.data?.token ?? "")
//                            return AuthManager.registerMobile(Context.getPushKey())
//                        }
//                        else {
//                        }
//                    }
//                    
//                }
//                
//                return Promise{ fulfill, reject in
//                    fulfill("OK")
//                }
//            }.then { requestResponse -> Void in
//                if let _ = requestResponse as? RequestResponse<EmptyResponseData>{
//                    self.showMessage(NSLocalizedString("Login succesfully", comment: "Login succesfully"))
//                    self.stopLoadingWithoutAnimation()
//                    self.navigator.navigateToCustomerRevealView()
//                }
//            }.always {
//                self.stopLoadingWithoutAnimation()
//            }.error { error -> Void in
//                if (error as NSError?) != nil{
//                    //self.handleError(errorType)
//                }
//                
//        }
//    }
    
    //MARK: - Text field delegate
    
    override func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        super.textFieldShouldBeginEditing(textField)
        return true
    }
    
    override func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        super.textFieldShouldEndEditing(textField)
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

class SettingData{
    var title: String?
    var image: UIImage?
    var placeHolder: String?
    var identifier: String
    var height: CGFloat
    var backgroundColor: UIColor?
    var action: (() -> Void)?
    
    init(title: String? = "", image: UIImage? = nil, placeHolder: String? = "", identifier: String?, height: CGFloat = InputCollectionViewCell.defaultHeight, backgroundColor: UIColor? = UIColor.clearColor(), action: (() -> Void)? = nil){
        self.title = title
        self.image = image
        self.placeHolder = placeHolder
        self.identifier = identifier ?? ""
        self.height = height
        self.backgroundColor = backgroundColor
        self.action = action
    }
}
