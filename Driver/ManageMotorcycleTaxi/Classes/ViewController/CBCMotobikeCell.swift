//
//  CBCMotobikeCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/9/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class CBCMotobikeCell: BaseCollectionViewCell {

    static let cellIdentifier = "CBCMotobikeCellID"
//    static let defaultHeight: CGFloat = 60
    
    @IBOutlet weak var motoBikeImgV: UIImageView!
    @IBOutlet weak var bikeNumberBtn: UIButton!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.customView(view2)
    }
    
    func customView(view: UIView) {
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.defaultBorderColor().CGColor
    }
    
    func loadMotobikeImage(imageURLString: String) {
        self.motoBikeImgV.af_setImageWithURL(NSURL.init(string: imageURLString)!)
    }

}
