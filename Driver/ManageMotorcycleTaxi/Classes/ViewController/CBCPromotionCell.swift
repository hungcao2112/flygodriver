//
//  CBCPromotionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/23/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class CBCPromotionCell: BaseCollectionViewCell {

    static let cellIdentifier = "CBCPromotionCellID"
    
    @IBOutlet weak var promotionCodeLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
