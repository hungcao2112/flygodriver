//
//  CustomerPaymentProcessViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 8/12/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import XCGLogger
import PromiseKit

protocol CustomerPaymentFailedDelegate {
    func didPaymentFailed(title: String, detail: String)
}

class CustomerPaymentProcessViewController: BaseViewController, UIWebViewDelegate, UIScrollViewDelegate {
    var paymentURL = ""
    let log = XCGLogger.defaultInstance()
    var isLoading = false
    var trip: Trip!
    var webView: UIWebView!
    var delegate: CustomerPaymentFailedDelegate?
    var bankID = 0
    let DongABankID = 99007
    var isShowKeyboard = false
    var timer = NSTimer()
    var zoomScale:CGFloat = 1.0
    var zoomInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageView = UIImageView(frame: CGRectMake(self.view.width()/4, 0, self.view.width()/2,  self.view.height()))
        imageView.image = UIImage(named: "avi_logo")
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        self.view.addSubview(imageView)
        self.view.backgroundColor = UIColor.whiteColor()
        imageView.backgroundColor = UIColor.whiteColor()
        let height = UIApplication.sharedApplication().statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        webView = UIWebView.init(frame: CGRectMake(0, height, self.view.width(), self.view.height() - height))
        webView.delegate = self
        self.view.addSubview(webView)
        webView.loadRequest(NSURLRequest.init(URL: NSURL.init(string: paymentURL)!))
        log.setup(.Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: nil, fileLogLevel: .Debug)
        webView.scalesPageToFit = true
        self.title = NSLocalizedString("PAYMENT", comment: "PAYMENT")
        
        if bankID == DongABankID {
            zoomInButton = UIButton.init(frame: CGRectMake(20, self.webView.height() - 70, 50, 50))
            zoomInButton.addTarget(self, action: #selector(CustomerPaymentProcessViewController.handleButtonTap), forControlEvents: UIControlEvents.TouchUpInside)
            zoomInButton.backgroundColor = UIColor.whiteColor()
            //zoomInButton.setImage(UIImage(named: "icon_zoom_out")?.resizeImage(25.0), forState: .Normal)
            zoomInButton.setImage(UIImage(named: "icon_zoom_out"), forState: .Normal)
            zoomInButton.contentMode = UIViewContentMode.ScaleAspectFit
            zoomInButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
            zoomInButton.roundCornersWithBorder(0.2, borderColor: UIColor.lightGrayColor().CGColor, radius: 25.0)
            zoomInButton.addShadowView()
            zoomInButton.hidden = true
            webView.addSubview(zoomInButton)
            webView.scrollView.delegate = self
        }
    }

    func handleButtonTap() {
        print("handleButtonTap");
        self.handleZoomInWebview()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        log.info(error?.description)
        self.stopLoadingWithoutAnimation()
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        log.info("webViewDidStartLoad")
        if isLoading == false {
            isLoading = true
            self.showLoading()
        }
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        log.info("webViewDidFinishLoad")
        self.stopLoadingWithoutAnimation()
        isLoading = false
        if bankID == DongABankID {
 //           log.info("scalesPageToFit")
            self.handleZoomInWebview()
        }
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        log.info("request:\(request) navigationType:\(navigationType.rawValue)")
        let URLString = request.URLString
        if URLString.hasPrefix("http://avigo.com.vn:9000") {
            self.webView.hidden = true
            let stringArray = URLString.componentsSeparatedByString("?")
            if stringArray.count > 0 {
                let lastString = stringArray.last
                let array = lastString?.componentsSeparatedByString("&")
                let dict = NSMutableDictionary()
                for component in array! {
                    let comArray = component.componentsSeparatedByString("=")
                    if (comArray.count == 2) {
                        dict.setValue(comArray[1], forKey: comArray[0])
                    }
                }
                if (dict.count == 3) {
                    if let trip = trip as Trip? {
                        if let tranid = dict.valueForKey("transid") as! String! {
                            if let confirmCode = dict.valueForKey("responCode") as! String! {
                                if let mac = dict.valueForKey("mac") as! String! {
                                    let txnAmount = trip.priceExpected
                                    if isLoading == false {
                                        isLoading = true
                                        self.showLoading()
                                    }
                                    firstly {
                                        return PaymentManager.paymentConfirm(tranid, confirmCode: confirmCode, txnAmount: txnAmount, mac: mac, codeNumber: trip.codeNumber)
                                        }.then { requestResponse -> Void in
                                            if let requestResponse = requestResponse as? PaymentResponse{
                                                print(requestResponse)
                                                let responseCode = requestResponse.data!.responsecode
                                                if responseCode == "00" {
                                                    self.stopLoadingWithoutAnimation()
                                                    self.isLoading = false
                                                    self.showMessage(NSLocalizedString("Thanks for payment", comment: "Thanks for payment"))
                                                    let controller = self.navigator.navigateToCustomerBookingCompleteView()
                                                    self.trip.invoiceStatus = InvoiceStatus.PRE_SUCCESS
                                                    controller.trip = self.trip
                                                }
                                                else {
                                                    let Descriptionvn = requestResponse.data?.Descriptionvn
                                                    let Descriptionen = requestResponse.data?.Descriptionvn
                                                    var descriptionString = Descriptionen
                                                    if Descriptionvn != "" {
                                                        descriptionString = Descriptionvn
                                                    }
                                                    self.handlePaymentError(NSLocalizedString("PAYMENT FAILED", comment: "PAYMENT FAILED"), detail: NSLocalizedString(descriptionString!, comment: descriptionString!))
                                                }
                                            }
                                        }.always {
                                            self.stopLoadingWithoutAnimation()
                                            self.isLoading = false
                                        }.error { error -> Void in
                                            if let errorType = error as NSError?{
                                                self.handleError(errorType)
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    
    func handlePaymentError(title: String, detail: String) {
        self.stopLoadingWithoutAnimation()
        self.isLoading = false
        self.navigationController?.popViewControllerAnimated(true)
        self.delegate?.didPaymentFailed(title, detail: detail)
    }
    
    func handleZoomInWebview() {
        let contentSize = webView.scrollView.contentSize;
        let viewSize = webView.bounds.size;
        if (contentSize.width - 50.0 > viewSize.width) {
            let rw = viewSize.width / contentSize.width;
            webView.scrollView.zoomScale = rw;

        }
    }
    
    func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
        self.zoomScale *= scale;
        if(self.zoomScale - 1.0 < 0.001 && self.zoomScale - 1.0 > -0.001) {
            // Not zooming
            self.zoomInButton.hidden = true
        }
        else {
            self.zoomInButton.hidden = false
        }
    }
}
