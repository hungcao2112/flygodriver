//
//  DriverInfoView.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class DriverInfoView: UIView {
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var vihicleTypeLbl: UILabel!
    @IBOutlet weak var vihicleNumberLbl: UILabel!
    
    var callDriverAction : (() -> ())?
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @IBAction func callDriverTapped(sender: AnyObject) {
        if let action = callDriverAction{
            action()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
