//
//  TransportTypeSelectionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/29/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import AlamofireImage

class TransportTypeSelectionCell: UICollectionViewCell {

    static let cellIdentifier = "TransportTypeSelectionCellID"
    
    @IBOutlet weak var transportTypeLbl: UILabel!
    @IBOutlet weak var transportTypeImgV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        transportTypeLbl.textColor = UIColor.defaultAppColor()
    }
    
    func setImageView(urlString: String) {
        transportTypeImgV.af_setImageWithURL(NSURL.init(string: urlString)!)
    }

    func setTransportTypeLabel(labelString: String) {
        transportTypeLbl.text = labelString
    }
}
