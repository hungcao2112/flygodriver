//
//  EndTripMessageView.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class EndTripMessageView: UIView, UITextViewDelegate {
    var viewController: UIViewController?
    @IBOutlet weak var startAddressView: AddressView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var endAddressView: AddressView!
    @IBOutlet weak var distanceLbl: MarqueeLabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBAction func noCommentAction(sender: AnyObject) {
        self.removeFromSuperview()
        self.handleDismissCustomerTripVC()
    }
    @IBOutlet weak var commentTextView: UITextView!
    @IBAction func commentAction(sender: AnyObject) {
        self.removeFromSuperview()
        self.handleDismissCustomerTripVC()
    }
    @IBOutlet weak var extraFeeLbl: MarqueeLabel!
    @IBOutlet weak var priceLbl: MarqueeLabel!
    
    private var tripHistory: Trip?
    private var backupContainerFrame = CGRectZero
    var tapGesture: UITapGestureRecognizer?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = Constants.UI.defaulCornerRadius
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(EndTripMessageView.dismissKeyboard))
        backupContainerFrame = containerView.frame
        containerView.layer.cornerRadius = 5
        self.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        if let startAddressView = NSBundle.mainBundle().loadNibNamed("AddressView", owner: self, options: nil)![0] as? AddressView{
            startAddressView.frame = self.startAddressView.frame
            startAddressView.iconImageView.image = UIImage(named: "icon_diemdi")
            self.startAddressView = startAddressView
            self.containerView.addSubview(startAddressView)
        }
        
        if let endAddressView = NSBundle.mainBundle().loadNibNamed("AddressView", owner: self, options: nil)![0] as? AddressView{
            endAddressView.frame = self.endAddressView.frame
            endAddressView.iconImageView.image = UIImage(named: "icon_diemden")
            self.endAddressView = endAddressView
            self.containerView.addSubview(endAddressView)
        }
        
        commentTextView.layer.cornerRadius = 5
        commentTextView.layer.borderColor = UIColor.grayColor().CGColor
        commentTextView.layer.borderWidth = 1
        commentTextView.delegate = self
        if let tripHistory = self.tripHistory{
            startAddressView.addressLabel.text = tripHistory.startPoint ?? ""
            endAddressView.addressLabel.text = tripHistory.endPointActual ?? ""
            distanceLbl.text = tripHistory.getDistanceString(tripHistory.kilometerActual)
            priceLbl.text = tripHistory.getPriceString(tripHistory.priceActual)
            extraFeeLbl.text = NSLocalizedString("0K", comment: "0K")
            totalPriceLbl.text = tripHistory.getPriceString(tripHistory.priceActual)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func loadView(tripHistory tripHistory: Trip){
        self.tripHistory = tripHistory
        
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        containerView.frame = CGRectMake(backupContainerFrame.minX, backupContainerFrame.minY - 216, backupContainerFrame.width, backupContainerFrame.height)
        self.addGestureRecognizer(tapGesture!)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        containerView.frame = CGRectMake(backupContainerFrame.minX, backupContainerFrame.minY, backupContainerFrame.width, backupContainerFrame.height)
        self.removeGestureRecognizer(tapGesture!)
    }
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    func handleDismissCustomerTripVC(){
        if let vc = self.viewController as? CustomerTripViewController{
            vc.dismissViewControllerAnimated(true, completion: nil)
        }
        else{
            if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                if let vc = topController as? CustomerTripViewController{
                    vc.dismissViewControllerAnimated(true, completion: nil)
                }
                else if let nvc = topController as? UINavigationController{
                    for vc in nvc.viewControllers{
                        if let customerTripViewController = vc as? CustomerTripViewController{
                            customerTripViewController.dismissViewControllerAnimated(true, completion: nil)
                        }
                    }
                }
            }
        }
    }
}
