//
//  DriverInfoCollectionViewCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/8/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class DriverInfoCollectionViewCell: UICollectionViewCell {

    static let cellIdentifier = "DriverInfoCollectionViewCellID"
    static let defaultHeight: CGFloat = 125
    
    //@IBOutlet weak var customBackgroundView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.customBackgroundView.layer.borderWidth = 2
//        self.customBackgroundView.layer.borderColor = UIColor.defaultBorderColor().CGColor
//        
//        self.customBackgroundView.layer.cornerRadius = 5
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
