//
//  CustomerBookingConfirmViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/23/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import PromiseKit
import ObjectMapper

class CustomerBookingConfirmViewController: BaseCollectionViewController {

    var cellIdentifiers = [String]()
    var trip: Trip!
    var datePickerContainer: UIView!
    var pickerDataSource = [String]()
    var pickerIndex = 0
    var transportList: [Transport]!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("BOOKING CONFIRM", comment: "BOOKING CONFIRM")
        transportList = GlobalAppData.sharedObject.getTransportAvailableListBySlotNumber(trip.getTransport().slot_number)
        
        self.setUpCollectionView()
        self.setUpDataSource()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpDataSource() {
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        
        if (trip.isMotobikeTrip()) {
            cellIdentifiers.append(CBCMotobikeCell.cellIdentifier)
            for tmp in 1...6 {
                pickerDataSource.append(String(tmp) + " " + NSLocalizedString("bike", comment: "bike"))
            }
        }
        else {
            for transport in transportList {
                pickerDataSource.append(transport.brand)
            }
            cellIdentifiers.append(CBCCarSelectionCell.cellIdentifier)
            cellIdentifiers.append(CBCCarRoundTripCell.cellIdentifier)
        }
        
        cellIdentifiers.append(CBCPromotionCell.cellIdentifier)
        cellIdentifiers.append(IntroCollectionCell.cellIdentifier)
        cellIdentifiers.append(ActionCollectionCell.cellIdentifier)
    }

    func setUpCollectionView(){
        self.collectionView.registerNib(UINib.init(nibName:"CBCCommonCell", bundle: nil), forCellWithReuseIdentifier: CBCCommonCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCMotobikeCell", bundle: nil), forCellWithReuseIdentifier: CBCMotobikeCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCarSelectionCell", bundle: nil), forCellWithReuseIdentifier: CBCCarSelectionCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCarRoundTripCell", bundle: nil), forCellWithReuseIdentifier: CBCCarRoundTripCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCPromotionCell", bundle: nil), forCellWithReuseIdentifier: CBCPromotionCell.cellIdentifier)
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
    }

    // MARK: CollectionView delegate
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return cellIdentifiers.count - 3
        default:
            return 3
        }

    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.width() - 20, Constants.UI.CollectionCellHeight.NormalCellHeight)
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let rowNumber = indexPath.row + (collectionView.numberOfItemsInSection(0) * indexPath.section)
        let cellIdentifier = cellIdentifiers[rowNumber]
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        if let inputCell = cell as? BaseCollectionViewCell {
            inputCell.fixContainerView()
        }

        if let inputCell = cell as? CBCCommonCell{
            var imageName = ""
            switch rowNumber {
            case 0:     //origin
                inputCell.titleLabel.text = trip.originPlace.name
                imageName = "icon_diemdi"
                break;
            case 1:     //destination
                inputCell.titleLabel.text = trip.destinationPlace.name
                imageName = "icon_diemden"
                break;
            case 2:     //price
                inputCell.titleLabel.text = trip.getPriceExpectedString()
                imageName = "icon_money"
                break;
            case 3:     //time
                inputCell.titleLabel.text = trip.startDate.toDayTimeString()
                imageName = "icon_calendar"
                break;
            default:
                break;
            }
            let imageView = inputCell.imageView
            let image = UIImage.init(named: imageName)
            let imageSizeAspect = CGFloat(image!.size.width/(image!.size.height))
            imageView.image = image
            for constraint in imageView.constraints {
                if (constraint.identifier == CBCCommonCell.imageConstraintID) {
                    constraint.setMultiplier(imageSizeAspect)
                }
            }
            
            inputCell.hideBoardEdge(BorderEdge.Bottom)
            if indexPath.row == 0{
                inputCell.roundCornersForCell([.TopLeft, .TopRight], radius: 5)
            }

            return inputCell
        }
        else if let inputCell = cell as? CBCMotobikeCell{
            inputCell.distanceLbl.text = trip.getKilometerExpectedString()
            inputCell.durationLbl.text = trip.getDurationExpectedString()
            inputCell.loadMotobikeImage(trip.getTransport().icon)
            inputCell.bikeNumberBtn.setTitle(String(trip.numberOfBike) + " " + NSLocalizedString("bike", comment: "bike"), forState: .Normal)
            inputCell.bikeNumberBtn.addTarget(self, action: #selector(CustomerBookingConfirmViewController.addNumOfBikePicker), forControlEvents: UIControlEvents.TouchUpInside)
            inputCell.roundCornersForBottomCell()
            return inputCell
        }
        else if let inputCell = cell as? CBCCarSelectionCell{
            print("icon: \(trip.getTransport().icon)")
            inputCell.iconImageView.af_setImageWithURL(NSURL.init(string: trip.getTransport().icon)!)
            inputCell.brandLbl.text = trip.getTransport().brand
            inputCell.hideBoardEdge(BorderEdge.Bottom)
            return inputCell
        }
        else if let inputCell = cell as? CBCCarRoundTripCell{
            inputCell.distanceLbl.text = trip.getKilometerExpectedString()
            inputCell.durationLbl.text = trip.getDurationExpectedString()
            inputCell.roundCornersForBottomCell()
            switch trip.isRoundTrip {
            case 0:
                inputCell.roundTripBtn.setImage(UIImage.init(named: "icon_uncheck"), forState: UIControlState.Normal)
                break
            default:
                inputCell.roundTripBtn.setImage(UIImage.init(named: "icon_check"), forState: UIControlState.Normal)
                break
            }
            let destinationViewTap = UITapGestureRecognizer.init(target: self, action: #selector(CustomerBookingConfirmViewController.handleRoundTripBtn(_:)))
            inputCell.roundTripView.addGestureRecognizer(destinationViewTap)
            inputCell.roundTripBtn.addTarget(self, action: #selector(CustomerBookingConfirmViewController.handleRoundTripBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            return inputCell
        }
        else if let inputCell = cell as? CBCPromotionCell {
            inputCell.promotionCodeLbl.text = trip.promotion.code
            inputCell.roundCornersForCell([.TopLeft, .TopRight, .BottomLeft, .BottomRight], radius: 5.0)
            return inputCell
        }
        else if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = NSLocalizedString("Confirmation code will be sent to your mobile phone number.", comment: "Confirmation code will be sent to your mobile phone number.")
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            inputCell.actionButonDatas = [ActionButtonData(title: NSLocalizedString("CONFIRM", comment: "CONFIRM"), backgroundColor: UIColor.defaulButtonColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                if Context.getAuthKey().characters.count > 0{
                    self.handleConfirm()
                }
                else{
                    GlobalAppData.sharedObject.setPendingOrderTrip(self.trip)
                    self.showAlertWindow(NSLocalizedString("Please login to use this feature", comment: "Please login to use this feature"), okAction: { () -> Void in
                        NavigationManager.sharedObject.navigateToLoginView()
                    })
                }
            })
            ]
            
            return inputCell
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let rowNumber = indexPath.row + (collectionView.numberOfItemsInSection(0) * indexPath.section)
        let cellIdentifier = cellIdentifiers[rowNumber]
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        if (rowNumber == 3) {
            self.addDatePicker()
        }
        else if (rowNumber == 4 && !trip.isMotobikeTrip()) {
            self.addCarPicker()
        }
        if cell is CBCPromotionCell {
            let altMessage = UIAlertController(title: NSLocalizedString("Promotion code", comment: "Promotion code"), message: NSLocalizedString("Please enter promotion code", comment: "Please enter promotion code"), preferredStyle: UIAlertControllerStyle.Alert)
            altMessage.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .Default, handler: { (action) -> Void in
                    print("text: \(altMessage.textFields![0].text)")
                    self.checkPromotionCode(altMessage.textFields![0].text!)
                }))
            altMessage.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .Cancel, handler: { (action) -> Void in
                
            }))
            altMessage.addTextFieldWithConfigurationHandler{ (textField) in
                    textField.placeholder = NSLocalizedString("Promotion code", comment: "Promotion code")
                }
            self.presentViewController(altMessage, animated: true, completion: nil)
        }
    }
    
    func checkPromotionCode(code: String) {
        if !code.isEmpty {
            self.showLoading()
            firstly {
                return TripManager.checkPromotionCode(code)
                }.then { requestResponse -> Void in
                    if let requestResponse = requestResponse as? SinglePromotionResponse {
                        print(requestResponse)
                        self.trip.promotion = requestResponse.data!
                        self.collectionView.reloadData()
                    }
                }.always {
                    self.stopLoadingWithoutAnimation()
                }.error { error -> Void in
                    if let errorType = error as NSError?{
                        if errorType.code == 400 {
                            self.showMessageAtCenter(NSLocalizedString("Invalid promotion code", comment: "Invalid promotion code"))
                        }
                        else {
                            self.handleError(errorType)
                        }
                    }
            }
        }
        else {
            self.trip.promotion = PromotionResponseData()
            self.collectionView.reloadData()
        }
    }
    
    func addDatePicker() {
        //Create the view
        let datePicker = UIDatePicker()
        datePickerContainer = UIView()
        
        datePickerContainer.frame = CGRectMake(0.0, self.view.height()/2, self.view.width(), self.view.height()/2)
        datePickerContainer.backgroundColor = UIColor.whiteColor()
        
        let pickerSize : CGSize = datePicker.sizeThatFits(CGSizeZero)
        datePicker.frame = CGRectMake((self.view.width() - pickerSize.width)/2, 20, pickerSize.width, pickerSize.height)
        datePicker.setDate(trip.startDate, animated: true)
        datePicker.minimumDate = NSDate()
        datePicker.datePickerMode = UIDatePickerMode.DateAndTime
        datePicker.addTarget(self, action: #selector(CustomerBookingConfirmViewController.dateChangedInDate(_:)), forControlEvents: UIControlEvents.ValueChanged)
        datePickerContainer.addSubview(datePicker)
        
        let doneButton = UIButton()
        doneButton.setTitle("Done", forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        doneButton.addTarget(self, action: #selector(CustomerBookingConfirmViewController.dismissPicker(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.frame    = CGRectMake(self.view.width() - 100.0, 5.0, 70.0, 37.0)
        
        datePickerContainer.addSubview(doneButton)
        
        self.view.addSubview(datePickerContainer)
    }
    
    /**
     * MARK - observer to get the change in date
     */
    
    func dateChangedInDate(sender:UIDatePicker){
        print(sender.date)
        trip.startDate = sender.date
        print(trip.startDate)
        let indexPaths = [NSIndexPath(forRow: 3, inSection: 0)]
        self.collectionView.reloadItemsAtIndexPaths(indexPaths)
    }// end dateChangedInDate
    
    /*
     * MARK - dismiss the date picker value
     */
    func dismissPicker(sender: UIButton) {
        datePickerContainer.removeFromSuperview()
    }// end dismissPicker

    func addCarPicker() {
        let pickerView = UIPickerView.init(frame: CGRectMake(0.0, self.view.height()/2, self.view.width(), self.view.height()/2))
        pickerView.tag = 100;
        pickerView.backgroundColor = UIColor.whiteColor()
        pickerView.dataSource = self;
        pickerView.delegate = self;
        pickerView.selectRow(pickerIndex, inComponent: 0, animated: true)
        self.view.addSubview(pickerView)
    }
    
    func addNumOfBikePicker() {
        let pickerView = UIPickerView.init(frame: CGRectMake(0.0, self.view.height()/2, self.view.width(), self.view.height()/2))
        pickerView.tag = 200;
        pickerView.backgroundColor = UIColor.whiteColor()
        pickerView.dataSource = self;
        pickerView.delegate = self;
        pickerView.selectRow(pickerIndex, inComponent: 0, animated: true)
        self.view.addSubview(pickerView)
    }
    
    @IBAction func handleRoundTripBtn(sender: UIButton) {
        trip.isRoundTrip = (trip.isRoundTrip + 1)%2
        trip.calculatePrice()
        let indexPaths = [NSIndexPath(forRow: 2, inSection: 0), NSIndexPath(forRow: 5, inSection: 0)]
        self.collectionView.reloadItemsAtIndexPaths(indexPaths)
    }
    
    private func handleConfirm() {
        if (trip.isMotobikeTrip()) {
            self.showLoading()
            firstly {
                return TripManager.bookTickets(trip)
                }.then { requestResponse -> Void in
                    if let requestResponse = requestResponse as? TripArrayResponse{
                        print(requestResponse)
                        if let responseTrip = requestResponse.data[0] as Trip! {
                            self.showMessage(NSLocalizedString("Booking successfully", comment: "Logout successfully"))
                            self.trip.codeNumber = responseTrip.codeNumber
                            self.trip.paymentMethod = responseTrip.paymentMethod
                            self.trip.invoiceStatus = responseTrip.invoiceStatus
                            let bookingCompleteVC = self.navigator.navigateToCustomerBookingCompleteView()
                            bookingCompleteVC.trip = self.trip
                        }
                    }
                }.always {
                    self.stopLoading()
                }.error { error -> Void in
                    if let errorType = error as NSError?{
                        self.handleError(errorType)
                    }
            }
        }
        else {
            let bookingPaymentVC = self.navigator.navigateToCustomerBookingPaymentViewController()
            bookingPaymentVC.trip = self.trip
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
}

extension CustomerBookingConfirmViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerIndex = row
        var indexPaths = [NSIndexPath]()
        switch pickerView.tag {
        case 100:
            trip.vehicleId = transportList[pickerIndex].id
            trip.calculatePrice()
            indexPaths = [NSIndexPath(forRow: 2, inSection: 0), NSIndexPath(forRow: 4, inSection: 0)]
            break
        case 200:
            trip.numberOfBike = pickerIndex + 1
            trip.calculatePrice()
            indexPaths = [NSIndexPath(forRow: 2, inSection: 0), NSIndexPath(forRow: 4, inSection: 0)]
            break
        default:
            break
        }
        pickerView.removeFromSuperview()
        self.collectionView.reloadItemsAtIndexPaths(indexPaths)
    }
}