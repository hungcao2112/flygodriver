//
//  CustomerBankSelectionViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 8/12/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import PromiseKit

class CustomerBankSelectionViewController: BaseCollectionViewController {
    
    var cellIdentifiers = [String]()
    var trip: Trip!
    var currentSelectedRow = 0
    
    let bankIDs = [99001,99002,99003,99004,99005,99006,99007,99008,99009,99010,99011,99012,99013,99014,99015,99016,99017,99018,99019,99020,99021,99022,99023,99027,99031,99026,99029]
    let bankNames = ["Agribank","Saigonbank","PG Bank","GP Bank","Sacombank","Nam Á Bank","Đông Á bank","Vietinbank","Techcombank","VIB","HDBank","Eximbank","TienphongBank","Maritime Bank","BIDV","MB","Seabank","SHB","Việt Á Bank","OceanBank","Vietcombank","VP Bank","ACB","NaviBank","Visa - Master","Bắc á","AnBinhBank"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("PAYMENT METHOD", comment: "PAYMENT METHOD")
        self.setUpCollectionView()
        self.setUpDataSource()
    
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.delegate = self
    }
    
    func setUpCollectionView(){
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCBankSelectionCell", bundle: nil), forCellWithReuseIdentifier: CBCBankSelectionCell.cellIdentifier)
    }
    
    func setUpDataSource() {
        cellIdentifiers.append(IntroCollectionCell.cellIdentifier)
        for _ in 1...bankIDs.count {
            cellIdentifiers.append(CBCBankSelectionCell.cellIdentifier)
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellIdentifiers.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:10.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if (indexPath.row == 0) {
            return CGSizeMake(self.view.width() - 20, Constants.UI.CollectionCellHeight.BankingIntroCellHeight)
        }
        return CGSizeMake((self.view.width() - 30)/3, Constants.UI.CollectionCellHeight.BankingCellHeight)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 5.0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let rowNumber = indexPath.row
        let cellIdentifier = cellIdentifiers[rowNumber]
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        if let inputCell = cell as? IntroCollectionCell {
            inputCell.introLabel.text = NSLocalizedString("IntroBanking", comment: "IntroBanking")
            return inputCell
        }
        else if let inputCell = cell as? CBCBankSelectionCell {
//            inputCell.setBorderDefaultColor(1.0)
//            if rowNumber == currentSelectedRow {
//                inputCell.setBorder(1.0, borderColor: UIColor.orangeColor().CGColor)
//            }
            inputCell.logoIV.image = UIImage(named: String(bankIDs[rowNumber - 1]) + ".JPG")
            inputCell.nameLbl.text = bankNames[rowNumber - 1]
            return inputCell
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let rowNumber = indexPath.row
//        let oldSelectionRow = currentSelectedRow
        currentSelectedRow = rowNumber
//        if currentSelectedRow != oldSelectionRow {
//            let indexPaths = [NSIndexPath(forRow: oldSelectionRow, inSection: 0), NSIndexPath(forRow: currentSelectedRow, inSection: 0)]
//            self.collectionView.reloadItemsAtIndexPaths(indexPaths)
//        }
        self.handleContinueBtn()
    }
    
    func handleContinueBtn() {
        if (currentSelectedRow > 0) {
            self.showLoading()
            firstly {
                return PaymentManager.payment(trip.priceExpected, phone: GlobalAppData.sharedObject.getLoginUser().account, bankID: bankIDs[currentSelectedRow - 1], codeNumber: trip.codeNumber)
                }.then { requestResponse -> Void in
                    if let requestResponse = requestResponse as? PaymentResponse{
                        print(requestResponse)
                        let responseCode = requestResponse.data!.responsecode
                        if responseCode == "00" {
                            let url = requestResponse.data!.url
                            if url != "" {
                                let controller = self.navigator.navigateToCustomerPaymentProcessView()
                                controller.paymentURL = url
                                controller.trip = self.trip
                                controller.delegate = self
                                controller.bankID = self.bankIDs[self.currentSelectedRow - 1]
                            }
                        }
                        else {
                            let Descriptionvn = requestResponse.data?.Descriptionvn
                            let Descriptionen = requestResponse.data?.Descriptionvn
                            if Descriptionvn != "" {
                                self.showMessage(Descriptionvn)
                            }
                            else {
                                self.showMessage(Descriptionen)
                            }
                        }
                    }
                }.always {
                    self.stopLoadingWithoutAnimation()
                }.error { error -> Void in
                    if let errorType = error as NSError?{
                        self.handleError(errorType)
                    }
            }
        }
        else {
            self.showMessage(NSLocalizedString("Please select a bank to pay", comment: "Please select a bank to pay"))
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.collectionView.delegate = nil
    }
}

extension CustomerBankSelectionViewController: CustomerPaymentFailedDelegate {
    func didPaymentFailed(title: String, detail: String) {
        self.showMessageAtCenter(title + ". " + detail)
    }
}