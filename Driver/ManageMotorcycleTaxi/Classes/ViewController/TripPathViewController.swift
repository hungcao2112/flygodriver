//
//  TripPathViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/26/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import GoogleMaps

class TripPathViewController: BaseViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var mapView: CustomMapView!
    
    var tripHistory = TripHistoryResponseData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("TRIP PATH", comment: "TRIP PATH")
        
        mapView.currentMarker.icon = Utils.resizeImage(UIImage(named: "icon_run") ?? UIImage(), targetSize: CGSize(width: 40, height: 40))
        mapView.isAutoDrawPolyline = false
        mapView.initLocationManager()
        mapView.polylineColor = UIColor.defaultAppColor()
        //mapView.startUpdatingLocation()
        
//        if tripHistory.status == "1" {
//                self.mapView.drawRoute(CLLocationCoordinate2D(latitude: tripHistory.coordinatesStart.latitude, longitude: tripHistory.coordinatesStart.longitude), originAddress:tripHistory.startPoint, destination: CLLocationCoordinate2D(latitude: tripHistory.coordinatesEndActual.latitude, longitude: tripHistory.coordinatesEndActual.longitude), destinationAddress: tripHistory.endPointActual, travelMode: TravelModes.driving) { (status, success) in
//                    self.mapView.fixBoundCamera()
//                }
//        }
//        else {
//            self.mapView.drawRoute(CLLocationCoordinate2D(latitude: tripHistory.coordinatesStart.latitude, longitude: tripHistory.coordinatesStart.longitude), originAddress:tripHistory.startPoint, destination: CLLocationCoordinate2D(latitude: tripHistory.coordinatesEndExpected.latitude, longitude: tripHistory.coordinatesEndExpected.longitude), destinationAddress: tripHistory.endPointExpected, travelMode: TravelModes.driving) { (status, success) in
//                self.mapView.fixBoundCamera()
//            }
//        }
    }
}
