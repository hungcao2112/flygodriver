//
//  AutoLoginViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/7/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit

class AutoLoginViewController: BaseViewController {
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var currentUserType = UserType.Driver
    private var timer: NSTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: #selector(AutoLoginViewController.endSplash), userInfo: nil, repeats: false)
        self.addDefaultGradientLayer()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //1324:265
        let gifImage = UIImage.gifWithName("splash_avi")
        let imageView = UIImageView(image: gifImage)
        let launchImageWidth = self.view.frame.width/4 * 3
        let launchImageHeight = launchImageWidth*265/1324
        imageView.frame = CGRect(x: self.view.frame.midX - launchImageWidth/2, y: self.view.frame.midY - launchImageHeight/2, width: launchImageWidth, height: launchImageHeight)
        view.addSubview(imageView)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(true)
    }
    
    func endSplash() {
        print("AA: \(Context.getAuthKey())")
        if let timer = self.timer{
            timer.invalidate()
                if Context.getAuthKey() == ""{
                    self.navigator.navigateToLoginView()
                }
                else{
                    AuthManager.registerMobile(Context.getPushKey())
                   self.navigator.navigateToDriverView()
                }
        }
    }
    
    private func handleAutoLogin(){
        firstly {
            return AuthManager.verifyToken()
            }.then { [weak self] loginResponse -> Void in
                if let strongSelf = self{
                    if let loginResponse = loginResponse as? LoginResponse<User>{
                        //GlobalAppData.sharedObject.setLoginUser(loginResponse.data?.user ?? User())
                        strongSelf.navigator.navigateToCustomerRevealView()
                    }
                }
            }.always {
                //self.indicatorView.stopAnimating()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.navigator.navigateToCustomerHomeView()
        }
    }
}
