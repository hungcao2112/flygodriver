//
//  AddressView.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class AddressView: UIView {

    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var addressLabel: MarqueeLabel!
    var addressText = ""{
        didSet{
            addressLabel.text = addressText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
