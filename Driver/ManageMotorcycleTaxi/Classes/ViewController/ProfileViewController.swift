//
//  ProfileViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import TOCropViewController
import UIKit

class ProfileViewController: BaseCollectionViewController, ImagePickerManagerDelegate, TOCropViewControllerDelegate {
    
    var updateProfileButtonBar: UIBarButtonItem!
    
    enum ProfileItemRow: Int{
        case Name = 0,
        PhoneNumer = 2,
        Email = 4
    }
    
    private let numberOfInputRow = 3
    
    var settingDatas = [SettingData]()
    
    var loginUser = ProfileResponseData()
    
    private var imagePickerManager: ImagePickerManager?
    
    private var profileHeaderView: ProfileHeaderView?
    
    private var nameTextField = UITextField()
    private var emailTextField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("PROFILE", comment: "PROFILE")
        
        updateProfileButtonBar = UIBarButtonItem(title: NSLocalizedString("Update", comment: "Update"), style: .Plain, target: self, action: #selector(ProfileViewController.UpdateProfileTapped(_:)))
        
        self.setUpDataSource()
        
        self.addDefaultGradientLayer()
        
        self.setUpCollectionView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
    }
    
    func setUpCollectionView(){
        self.collectionView.backgroundColor = UIColor.clearColor()
        self.collectionView!.registerClass(ProfileHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ProfileHeaderView.cellIdentifier)
        
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ProfileCollectionCell.self, forCellWithReuseIdentifier: InputCollectionViewCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
    }
    
    func setUpDataSource(){
        //name
        settingDatas.append(SettingData(placeHolder: NSLocalizedString("Full Name", comment: "Full Name"), identifier: InputCollectionViewCell.cellIdentifier))
        
        //space
        settingDatas.append(SettingData(title: NSLocalizedString("", comment: ""), identifier: IntroCollectionCell.cellIdentifier , height:20))
        
        //phone
        settingDatas.append(SettingData(title: NSLocalizedString("", comment: ""), image: UIImage(named: "icon-vietnam-flag"), placeHolder: NSLocalizedString("Mobile Phone", comment: "Mobile Phone"), identifier: InputCollectionViewCell.cellIdentifier))
        
        //space
        settingDatas.append(SettingData(title: NSLocalizedString("", comment: ""), identifier: IntroCollectionCell.cellIdentifier, height:20))
        
        //email
        settingDatas.append(SettingData(placeHolder: NSLocalizedString("Email", comment: "Email"), identifier: InputCollectionViewCell.cellIdentifier))
        
        //space
        settingDatas.append(SettingData(title: NSLocalizedString("", comment: ""), identifier: IntroCollectionCell.cellIdentifier, height:20))
        
        //password
        settingDatas.append(SettingData(title: NSLocalizedString("CHANGE PASSWORD", comment: "CHANGE PASSWORD"), identifier: ActionCollectionCell.cellIdentifier) {
                self.navigateToUpdatePasswordView()
            })
        
        //space
        settingDatas.append(SettingData(title: NSLocalizedString("", comment: ""), identifier: IntroCollectionCell.cellIdentifier, height:20))
        
        settingDatas.append(SettingData(title: NSLocalizedString("SIGN OUT", comment: "SIGN OUT"), identifier: ActionCollectionCell.cellIdentifier) {
            self.showAlertWindow(NSLocalizedString("Do you really want to log out?", comment: "Do you really want to log out?"), okAction: { () -> Void in
                    self.logout()
                })
            })
    }
    
    //MARK: - CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if (kind == UICollectionElementKindSectionHeader) {
            if let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: ProfileHeaderView.cellIdentifier, forIndexPath: indexPath) as? ProfileHeaderView{
                view.backgroundColor = UIColor.clearColor()
                view.nameLabel.text = "\(loginUser.fullname ?? "")"
                
                view.avatarImageView.af_setImageWithURL(
                    NSURL(string: loginUser.avartar ?? "")!,
                    placeholderImage: UIImage(named: "user"),
                    filter: nil,
                    imageTransition: .None,
                    completion: { (response) -> Void in
                })
                
                view.tapHandler = { () -> Void in
                    self.showImagePickerView()
                }
                
                profileHeaderView = view
                
                return view
            }
        }
        
        return collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: ProfileHeaderView.cellIdentifier, forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSizeMake(self.view.frame.width, ProfileHeaderView.defaultHeight)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20.0, left: 10.0, bottom:20.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let settingData = settingDatas[indexPath.row]
        return CGSizeMake(self.view.width() - 40, settingData.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settingDatas.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let settingData = settingDatas[indexPath.row]
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(settingData.identifier, forIndexPath: indexPath)
        
        if let inputCell = cell as? ProfileCollectionCell{
            inputCell.roundCornersForCell([.BottomLeft, .BottomRight, .TopLeft, .TopRight], radius: 5)
            
            inputCell.title = settingData.title
            inputCell.image = settingData.image
            inputCell.inputTextField.placeholder = settingData.placeHolder
            inputCell.inputTextField.tag = indexPath.row
            inputCell.inputTextField.delegate = self
            inputCell.inputTextField.addTarget(self, action: #selector(ProfileViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
            if let profileRow = ProfileItemRow(rawValue: indexPath.row){
                switch profileRow {
                case .Name:
                    nameTextField = inputCell.inputTextField
                    inputCell.inputTextField.text = "\(loginUser.fullname ?? "")"
                    break
                case .PhoneNumer:
                    inputCell.inputTextField.text = "\(loginUser.phone ?? "")"
                    inputCell.inputTextField.userInteractionEnabled = false
                    break
                case .Email:
                    emailTextField = inputCell.inputTextField
                    inputCell.inputTextField.text = "\(loginUser.email ?? "")"
                    break
                    
                }
            }
            
            return inputCell
        }
        else if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = settingData.title
            
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            
            if settingData.title == NSLocalizedString("CHANGE PASSWORD", comment: "CHANGE PASSWORD"){
                inputCell.actionButonDatas = [ActionButtonData(title: settingData.title, backgroundColor: UIColor.whiteColor(), titleColor: UIColor.defaultAppColor(), borderWidth: 2, borderColor: UIColor.whiteColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center,
                    action: { () -> Void in
                        if let action = settingData.action{
                            action()
                        }
                })
                ]
            }else if settingData.title == NSLocalizedString("SIGN OUT", comment: "SIGN OUT"){
                inputCell.actionButonDatas = [ActionButtonData(title: settingData.title, backgroundColor: UIColor.whiteColor(), titleColor: UIColor.redColor(), borderWidth: 2, borderColor: UIColor.defaultButtonBorderColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center,
                    action: { () -> Void in
                        if let action = settingData.action{
                            action()
                        }
                })
                ]
            }
            
            return inputCell
        }
        
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    func showImagePickerView(){
        if imagePickerManager == nil{
            imagePickerManager = ImagePickerManager(viewController: self)
            imagePickerManager?.delegate = self
        }
        
        imagePickerManager?.presentPickerPhotoOptionView(cameraDevice: .Rear)
    }
    
    //MARK: - UITextField delegate

    func textFieldDidChange(textField: UITextField){
        let changedText = textField.text
        if let profileRow = ProfileItemRow(rawValue: textField.tag){
            switch profileRow {
            case .Name:
                if changedText != loginUser.fullname && changedText != ""{
                    self.showUpdateBarItem(true)
                }
                else{
                    self.showUpdateBarItem(false)
                }
                break
            case .Email:
                if changedText != loginUser.email{
                    self.showUpdateBarItem(true)
                }
                else{
                    self.showUpdateBarItem(false)
                }
                break
            default:
                break
            }
        }
    }
    
    //MARK: - ImagePickerManager delegate
    func didPickImage(image: UIImage!) {
        if image != nil{
            
            var width: CGFloat = 500

            if image.size.width < width{
                width = image.size.width
            }
            
            let ratio = width/image.size.width
            
            let newImage = Utils.resizeImage(image, targetSize: CGSize(width: width, height: image.size.height*ratio))
            let cropViewController = TOCropViewController(image: newImage)
            cropViewController.delegate = self
            self.presentViewController(cropViewController, animated: true, completion: nil)
        }
    }
    
    //MARK: - TOCropViewControllerDelegate
    func cropViewController(cropViewController: TOCropViewController!, didFinishCancelled cancelled: Bool) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cropViewController(cropViewController: TOCropViewController!, didCropToImage image: UIImage!, withRect cropRect: CGRect, angle: Int) {
        self.dismissViewControllerAnimated(true, completion: nil)
        let imageData = UIImageJPEGRepresentation(image , 1.0)
        self.showLoading()
        firstly {
            return  UserManager.uploadAvatar(imageData!)
            }.then { [weak self] requestResponse -> Void in
                if let strongSelf = self{
                    if let requestResponse = requestResponse as? LoginResponse<LoginResponseData>{
                        
//                        if let loginUser = requestResponse.data[0].user{
//                            GlobalAppData.sharedObject.setLoginUser(loginUser)
//                        }
                        
                        strongSelf.showMessage(NSLocalizedString("Upload avatar successfully", comment: "Upload avatar successfully"))
                        
                        strongSelf.profileHeaderView?.avatarImageView.image = image
                    }
                }
            }.always {
                self.stopLoading()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.showMessage(NSLocalizedString("Upload avatar failed", comment: "Upload avatar failed"))
        }
    }

    //MARK: - Actions
    func UpdateProfileTapped(sender: UIBarButtonItem){
        self.handleUpdateProfile()
    }
    
    //MARK: - Methods
    func navigateToUpdatePasswordView() {
        let vc = self.navigator.navigateToUpdatePasswordView()
        vc.viewMode = UpdatePasswordMode.Update
        vc.title = NSLocalizedString("UPDATE PASSWORD", comment: "UPDATE PASSWORD")
        vc.phone = self.loginUser.phone ?? ""
        vc.name = self.loginUser.fullname ?? ""
    }
    
    func navigateToHomeView() {
        self.navigator.navigateToLoginView()
    }
    
    private func logout(){
        self.showLoading()
        firstly {
            return UserManager.logout()
            }.then { requestResponse -> Void in
                if let response = requestResponse as? ProfileResponse{
                    self.showMessage(NSLocalizedString("Logout successfully", comment: "Logout successfully"))
                    Context.setAuthKey("")
                    Context.setToken("")
                }
                
                self.stopLoading({ finished -> Void in
                    self.navigateToHomeView()
                })
                
            }.always {
                self.stopLoading()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.showMessage(NSLocalizedString("Logout failed", comment: "Logout failed"))
        }
    }
    
    private func handleUpdateProfile(){
        if !(emailTextField.text ?? "").isValidEmail(){
            self.showMessage(NSLocalizedString("Please enter a valid email", comment: "Please enter a valid email"))
            return
        }
        self.showLoading()
        firstly {
            return CustomerManager.updateProfile(nameTextField.text ?? "", email: emailTextField.text ?? "")
            }.then { loginResponse -> Void in
                if let loginResponse = loginResponse as? LoginResponse<User>{
                    self.showMessage(NSLocalizedString("Update profile successfully", comment: "Update profile successfully"))
                    GlobalAppData.sharedObject.setLoginUser(loginResponse.data[0].user ?? User())
                    //self.loginUser = GlobalAppData.sharedObject.getLoginUser()
                    self.collectionView.reloadData()
                    self.showUpdateBarItem(false)
                }
            }.always {
                self.stopLoading()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.showMessage(NSLocalizedString("Update profile failed", comment: "Update profile failed"))
        }
    }
    
    //MARK: - Helpers
    func showUpdateBarItem(isShow: Bool){
        if isShow{
            self.navigationItem.rightBarButtonItem = updateProfileButtonBar
        }
        else{
            self.navigationItem.rightBarButtonItem = nil
        }
    }
}
