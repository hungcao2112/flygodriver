//
//  HistoryDetailViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/26/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit

class HistoryDetailViewController: BaseViewController {
    
    @IBOutlet weak var briefHistoryView: BriefHistoryView!
    @IBOutlet weak var driverAvatarImageView: UIImageView!
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var callDriverBtn: UIButton!
    @IBOutlet weak var amountLbl: UILabel!
    
    var tripHistory = TripHistoryResponseData()
    var currentTarget = Target.Driver
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("DETAILED JOURNEY", comment: "DETAILED JOURNEY")
        currentTarget = GlobalAppData.sharedObject.currentTarget
        
        self.callDriverBtn.format2()
        
        self.driverNameLbl.text = "Cao Hung"
        self.distanceLbl.text = tripHistory.distance
        
        self.amountLbl.text = tripHistory.total
        
        //self.getDriverInfo()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let view = NSBundle.mainBundle().loadNibNamed("BriefHistoryView", owner: self, options: nil)![0] as? UIView
        
        if let view = view as? BriefHistoryView{
            view.frame = CGRectMake(self.briefHistoryView.minX(), (self.navigationController?.navigationBar.maxY())!, self.view.frame.width, self.briefHistoryView.height())
            self.view.addSubview(view)
            briefHistoryView = view as BriefHistoryView
            view.titleLabel.text = NSLocalizedString("Order code", comment: "Order code")
            view.valueLabel.text = tripHistory.code
            view.valueLabel.font = UIFont.boldSystemFontOfSize(briefHistoryView.valueLabel.font.pointSize)
            view.valueLabel.textColor = UIColor.redColor()
            view.startAddressLabel.text = tripHistory.address_a
            view.endAddressLabel.text = tripHistory.address_b
            view.endAddressLabel.textColor = .blackColor()
            if (tripHistory.status == "1") {
                briefHistoryView.endAddressLabel.text = tripHistory.address_b
            }
            view.addTapGestureOnView()
//            briefHistoryView.tapHandler = { () -> Void in
//                if (self.navigator != nil) {
//                    let vc = self.navigator.navigateToTripPathView()
//                    if let tripPathVC = vc as? TripPathViewController{
//                        tripPathVC.tripHistory = self.tripHistory
//                    }
//                }
//            }
        }
    }
    
    @IBAction func callDriverAction(sender: AnyObject) {
        
    }
    
    //MARK: - Helpers
    
//    func getDriverInfo(){
//        self.showLoading()
//        firstly {
//            return CustomerManager.getDriverInfo(driverNumber: self.tripHistory.driverNumber)
//            }.then { [weak self] response -> Void in
//                if let strongSelf = self{
//                    if let response = response as? LoginResponse<Driver>{
//                        if let driver = response.data[0].user as? Driver{
//                            strongSelf.driverNameLbl.text = driver.lastName
//                            strongSelf.driverAvatarImageView.af_setImageWithURL(NSURL.init(string: driver.avatar)!)
//                        }
//                    }
//                    
//                }
//            }.always {
//                self.stopLoading()
//            }.error { error -> Void in
//                if let errorType = error as NSError?{
//                    self.handleError(errorType)
//                }
//        }
//    }
}
