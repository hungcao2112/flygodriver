//
//  CustomerHomeViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import PromiseKit

class CustomerHomeViewController: BaseViewController {

    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
//    @IBOutlet weak var introLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addDefaultGradientLayer()
        //self.visibleDefaultBackgroundImage(true)
        //self.getTransportListItems()
        self.setUpView()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(true)
    }
    
    func setUpView(){
        reserveBtn.formatWhiteBackground()
        registerBtn.formatWhiteBackground()
        loginBtn.formatWhiteBackground()
//        introLabel.textColor = UIColor.whiteColor()
//        introLabel.text = NSLocalizedString("AVIGo Introduce", comment: "AVIGo Introduce")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - User actions
    
    @IBAction func reserveTapped(sender: AnyObject) {
//        self.navigator.navigateToCustomerTripView()
//        self.navigator.navigateToCustomerBookingView()
        self.navigator.switchStoryboard(Storyboards.customerBooking)
        self.navigator.navigateToCustomerRevealView()
//        self.navigator.navigateToCustomerRevealView()
    }

    @IBAction func registerTapped(sender: AnyObject) {
        self.navigator.switchStoryboard(Storyboards.customer)
        self.navigator.navigateToRegisterView()
    }
    
    @IBAction func loginTapped(sender: AnyObject) {
        self.navigator.switchStoryboard(Storyboards.customer)
        self.navigator.navigateToLoginView()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

}
