//
//  DriverReadyViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/14/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import PromiseKit

class DriverAcceptViewController: BaseViewController {
    
    @IBOutlet weak var orderCodeLabel: UILabel!
//    @IBOutlet weak var circleProgressBar: CircleProgressBar!
    
    var timer: NSTimer!
    var assignTrip: Trip!
    var code = ""
    
    @IBOutlet weak var progressView: UAProgressView!
    @IBOutlet weak var countDownLabel: UILabel!
    var driverTrip = DriverTrip()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //self.title = NSLocalizedString("WAITING ACCEPT TRIP", comment: "WAITING ACCEPT TRIP")
        timer = NSTimer.scheduledTimerWithTimeInterval(Constants.Timer.TimerInterval, target: self, selector: #selector(DriverAcceptViewController.timerHandler), userInfo: nil, repeats: true)
        //assignTrip = GlobalAppData.sharedObject.getCurrentTrip()
        self.title = NSLocalizedString("WAITING ACCEPT TRIP", comment: "WAITING ACCEPT TRIP")
        orderCodeLabel.text = code
        self.hideBackBarBtn(true)
        self.initProgressView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(DriverAcceptViewController.handleBackButtonTap))
        NavigationManager.sharedObject.isDriverAcceptViewAppear = true
        
        self.getTripDetail(code)
    }
    
    func handleBackButtonTap() {
        self.showAlertWindow(NSLocalizedString("Do you really want to reject trip?", comment: "Do you really want to reject trip?"), okAction: { () -> Void in
            self.handleRejectTrip()
        })
    }
    
    func initProgressView() {
        self.progressView.tintColor = UIColor.defaultAppColor()
        self.progressView.borderWidth = 10.0;
        self.progressView.lineWidth = 10.0;
        self.progressView.fillOnTouch = false;
        countDownLabel.frame = self.progressView.frame
        countDownLabel.text = String(Int(Constants.Timer.TimerDriverRejectTrip)) + "s"
        self.progressView.centralView = countDownLabel
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if (timer != nil) {
            timer.invalidate()
            timer = nil
        }
        NavigationManager.sharedObject.isDriverAcceptViewAppear = false
    }

    func timerHandler() {
        progressView.progress = progressView.progress + CGFloat(Constants.Timer.TimerInterval/Constants.Timer.TimerDriverRejectTrip)
//        let minute = Int(Constants.Timer.TimerDriverRejectTrip/60.0)
        countDownLabel.text = String(Int(CGFloat(Constants.Timer.TimerDriverRejectTrip)*(1.0 - progressView.progress))) + "s"
        if progressView.progress > 0.99 {
            if (timer != nil) {
                timer.invalidate()
                timer = nil
            }
            self.handleRejectTrip()
        }
    }
    
    
    @IBAction func acceptTripHandler(sender: UIButton) {
        print("acceptTrip")
        if (self.checkLocationEnabled() == true) {
            self.handleAcceptTrip()
        }
    }
    
    func handleAcceptTrip() {
        self.showLoading()
        firstly {
            return DriverManager.acceptTrip(code)
            }.then { responseObject -> Void in
                if let response = responseObject as? SingleTripResponse {
                    //GlobalAppData.sharedObject.setCurrentTrip(assignTrip!)
                    self.stopLoadingWithoutAnimation()
                    self.navigateToDriverTripView()
                }
            }.always {
                self.stopLoadingWithoutAnimation()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }
    }
    
    func handleRejectTrip () {
        self.showLoading()
        firstly {
            return DriverManager.rejectTrip(code)
            }.then { response -> Void in
                if let response = response as? SingleTripResponse {
                    let assignTrip = response.data?.trip
                    //GlobalAppData.sharedObject.setCurrentTrip(assignTrip)
                    self.stopLoadingWithoutAnimation()
                    self.navigateToDriverHomeView()
                }
            }.always {
                self.stopLoadingWithoutAnimation()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }
    }
    
    func navigateToDriverHomeView() {
        if let nv = self.navigationController{
            nv.popViewControllerAnimated(true)
        }
    }
    
    func navigateToDriverTripView() {
        let controller = Storyboards.driver.instantiateViewControllerWithIdentifier(Constants.StoryBoard.Driver.DriverTripViewID)
            as! DriverTripViewController
        controller.navigator = self.navigator
        if let nv = self.navigationController{
            //nv.presentViewController(controller, animated: true, completion: nil)
            nv.pushViewController(controller, animated: true)
        }
    }
    func getTripDetail(code: String){
        firstly {
            return DriverManager.getTripDetail(code)
            }.then { response -> Void in
                if let response = response as? DriverTripResponse {
                    let assignTrip = response.data[0]
                    GlobalAppData.sharedObject.setCurrentTrip(assignTrip)
                }
            }.always {
                self.stopLoadingWithoutAnimation()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }
    }
}
