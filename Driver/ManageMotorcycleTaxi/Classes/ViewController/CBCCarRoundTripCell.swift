//
//  CBCCarRoundTripCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/24/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class CBCCarRoundTripCell: BaseCollectionViewCell {

    static let cellIdentifier = "CBCCarRoundTripCellID"
    
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var roundTripBtn: UIButton!
    @IBOutlet weak var roundTripView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        centerView.layer.borderWidth = 1
        centerView.layer.borderColor = UIColor.defaultBorderColor().CGColor
    }

}
