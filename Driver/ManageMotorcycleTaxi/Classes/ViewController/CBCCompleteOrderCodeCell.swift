//
//  CBCCompleteOrderCodeCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/28/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class CBCCompleteOrderCodeCell: UICollectionViewCell {
    static let cellIdentifier = "CBCCompleteOrderCodeCellID"

    @IBOutlet weak var orderCodeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        orderCodeLbl.textColor = UIColor.defaultAppColor()
    }

}
