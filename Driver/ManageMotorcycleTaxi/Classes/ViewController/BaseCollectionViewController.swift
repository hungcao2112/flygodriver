//
//  BaseCollectionViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/6/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class BaseCollectionViewController: BaseViewController, UITextFieldDelegate {
    var collectionView: UICollectionView!
    
    var tapGesture: UITapGestureRecognizer?
    
    var activeTextField: UITextField?
    
    private var collectionViewOriginUIEdgeInsets: UIEdgeInsets?
    
    var didScrollToBottom: (() -> ())?
    var didScrollToTop: (() -> ())?
    var didScrollToBottomOnce = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let frame = self.view.frame
        let layout: UICollectionViewFlowLayout = getCustomFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: self.view.frame.width, height: 100)
        
        collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = UIColor.defaultBackgroundColor()
        
        self.view.addSubview(collectionView)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(BaseCollectionViewController.dismissKeyboard))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BaseCollectionViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BaseCollectionViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionViewOriginUIEdgeInsets = self.collectionView.contentInset
        
    }
    func getCustomFlowLayout() -> UICollectionViewFlowLayout {
        return UICollectionViewFlowLayout()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return super.preferredStatusBarStyle()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: - Notifications
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            if collectionViewOriginUIEdgeInsets == nil {
                collectionViewOriginUIEdgeInsets = self.collectionView.contentInset
            }
            let contentInsets = UIEdgeInsetsMake(collectionViewOriginUIEdgeInsets!.top , 0.0, keyboardSize.size.height, 0.0)
            self.collectionView.contentInset = contentInsets
            self.collectionView.scrollIndicatorInsets = contentInsets
            
            if let activeTextField = self.activeTextField {
                let rect = self.collectionView.convertRect(activeTextField.bounds, fromView: activeTextField)
                self.collectionView.scrollRectToVisible(rect, animated: true)
            }
        }
        self.collectionView.addGestureRecognizer(tapGesture!)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.collectionView.contentInset = collectionViewOriginUIEdgeInsets ?? UIEdgeInsetsZero;
        self.collectionView.scrollIndicatorInsets = UIEdgeInsetsZero;
        self.collectionView.removeGestureRecognizer(tapGesture!)
    }
    
    //MARK: UITextField delegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.activeTextField = textField
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        self.activeTextField = nil
        return true
    }
}

extension BaseCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        didScrollToBottomOnce = false
        // getting the scroll offset
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
        
        let scrollOffset = scrollView.contentOffset.y;
        print("\(bottomEdge), \(scrollView.contentOffset.y), \(scrollView.frame.maxY), \(collectionView.frame.height), \(scrollView.contentSize.height)")
        if scrollView.contentOffset.y == 0
        {
            if let action = self.didScrollToBottom{
                action()
                didScrollToBottomOnce = true
            }
        }
        if self.navigationController != nil {
            if (scrollOffset <= (0 - self.navigationController!.navigationBar.frame.size.height - 60)) { // TOP
                if let action = self.didScrollToTop{
                    action()
                }
            }
        }
    }
}
