//
//  BriefHistoryView.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/26/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class DriverEndTripHeadCell: UICollectionViewCell {

    var view: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startAddressLabel: UILabel!
    @IBOutlet weak var endAddressLabel: UILabel!
    
    @IBOutlet weak var headerImageView: UIImageView!
    static let cellIdentifier = "DriverEndTripHeadCellID"

    var tapGesture: UITapGestureRecognizer?
    
    var tapHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addTapGestureOnView(){
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(BriefHistoryView.viewDidTap))
        self.addGestureRecognizer(tapGesture!)
    }
    
    func viewDidTap() {
        if let action = tapHandler{
            action()
        }
    }
}
