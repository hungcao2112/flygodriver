//
//  CBCCommonCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/9/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class CBCCommonCell: BaseCollectionViewCell {

    static let cellIdentifier = "CBCCommonCellID"
    static let imageConstraintID = "ImageAspectConstraintID"
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: MarqueeLabel!
    
    @IBOutlet weak var sizeAspectConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
