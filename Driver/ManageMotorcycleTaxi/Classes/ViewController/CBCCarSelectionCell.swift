//
//  CBCCarSelectionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/23/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class CBCCarSelectionCell: BaseCollectionViewCell {

    static let cellIdentifier = "CBCCarSelectionCellID"
    static let defaultHeight: CGFloat = 60
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var brandLbl: UILabel!
    @IBOutlet weak var indicatorImgV: UIImageView!
    
}
