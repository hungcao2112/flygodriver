//
//  DriverManagerCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/21/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class DriverManagerCell: UICollectionViewCell {

    @IBOutlet weak var avatarImgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var codeNumberLabel: UILabel!
    static let cellIdentifier = "DriverManagerCellID"
    static let defaultHeight: CGFloat = 50.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        actionButton.roundCornersWithBorder(5.0)
    }

}
