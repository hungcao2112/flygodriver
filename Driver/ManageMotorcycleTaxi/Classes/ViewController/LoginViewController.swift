//
//  CustomerLoginViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/5/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import UIKit
import Alamofire
import FBSDKLoginKit
import FBSDKShareKit

enum UserType: Int {
    case Customer = 2,
    Driver = 3,
    Coordinator = 5
}

class LoginViewController: BaseCollectionViewController {
    
    @IBOutlet weak var registerBtn: UIButton!
    var phoneNumberTextField: UITextField?
    var passwordTextField: UITextField?
    
    @IBOutlet weak var registerQuestionLabel: UILabel!
    
    enum InputRow: Int {
        case PhoneNumber = 0,
        Password
    }
    let numberOfInputRow = 2
    
    private var settingDatas = [SettingData]()
    var currentTarget = Target.Driver
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("LOGIN", comment: "LOGIN")
        self.registerBtn.layer.borderColor = UIColor.defaulButtonColor().CGColor
        self.registerBtn.layer.borderWidth = 1
        self.registerBtn.layer.cornerRadius = Constants.UI.defaulCornerRadius
        self.registerBtn.setTitleColor(UIColor.defaulButtonColor(), forState: UIControlState.Normal)
        
        self.setUpDataSource()
        
        self.setUpCollectionView()
        
        self.view.backgroundColor = UIColor.defaultBackgroundColor()
        
        if currentTarget == .Driver{
            self.hideBackBarBtn(true)
            registerBtn.hidden = false
            registerQuestionLabel.hidden = false
        }
        print("AAAA: \(Context.getPushKey())")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
    }
    
    func setUpCollectionView(){
        self.collectionView.frame = CGRectMake(0, self.collectionView.frame.minY + 65, self.view.width(), self.view.height() - 150)
        self.collectionView.registerClass(InputCollectionViewCell.self, forCellWithReuseIdentifier: InputCollectionViewCell.cellIdentifier)
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"SeperateCollectionCell", bundle: nil), forCellWithReuseIdentifier: SeperateCollectionCell.cellIdentifier)
    }
    
    func setUpDataSource(){
        settingDatas.append(SettingData(title: NSLocalizedString("Mobile phone", comment: "Mobile phone"), placeHolder: NSLocalizedString("Mobile phone", comment: "Mobile phone"), identifier: InputCollectionViewCell.cellIdentifier))
        settingDatas.append(SettingData(title: NSLocalizedString("Password", comment: "Password"), placeHolder: NSLocalizedString("Password", comment: "Password"), identifier: InputCollectionViewCell.cellIdentifier))
        settingDatas.append(SettingData(title: NSLocalizedString("Forgot password?", comment: "Forgot password?"), identifier: ActionCollectionCell.cellIdentifier))
        settingDatas.append(SettingData(title: NSLocalizedString("LOGIN", comment: "LOGIN"), identifier: ActionCollectionCell.cellIdentifier))
        
        if currentTarget == .Customer {
            settingDatas.append(SettingData(identifier: SeperateCollectionCell.cellIdentifier, height: InputCollectionViewCell.defaultHeight))
            settingDatas.append(SettingData(title: NSLocalizedString("FACEBOOK LOGIN", comment: "FACEBOOK LOGIN"), identifier: ActionCollectionCell.cellIdentifier))
        }
    }
    
    //MARK: - CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let settingData = settingDatas[indexPath.row]
        return CGSizeMake(self.view.width() - 20, settingData.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settingDatas.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let settingData = settingDatas[indexPath.row]
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(settingData.identifier, forIndexPath: indexPath)
        
        
        if let inputCell = cell as? InputCollectionViewCell{
            if indexPath.row == 0{
                inputCell.hideBoardEdge(BorderEdge.Bottom)
                inputCell.roundCornersForCell([.TopLeft, .TopRight], radius: 5)
            }else if indexPath.row == numberOfInputRow - 1{
                inputCell.roundCornersForCell([.BottomLeft, .BottomRight], radius: 5)
            }
            else{
                inputCell.hideBoardEdge(BorderEdge.Bottom)
            }
            
            inputCell.titleLabel.text = settingData.title
            inputCell.inputTextField.placeholder = settingData.placeHolder
            
            if let inputRow = InputRow(rawValue: indexPath.row){
                switch inputRow{
                case .PhoneNumber:
                    phoneNumberTextField = inputCell.inputTextField
                    phoneNumberTextField?.keyboardType = UIKeyboardType.PhonePad
                case .Password:
                    passwordTextField = inputCell.inputTextField
                    passwordTextField?.secureTextEntry = true
                    passwordTextField?.autocapitalizationType = .None
                }
            }
            
            return inputCell
        }
        else if let inputCell = cell as? SeperateCollectionCell{
            inputCell.backgroundColor = UIColor.clearColor()
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            if settingData.title ==  NSLocalizedString("Forgot password?", comment: "Forgot password?"){
                inputCell.actionButonDatas = [
                    ActionButtonData(title: settingData.title, backgroundColor: UIColor.clearColor(), titleColor:  UIColor.blackColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Right,
                        action: { () -> Void in
                            self.view.endEditing(true)
                            self.navigator.navigateToForgotPasswordView()
                    })
                ]
            }
            else if settingData.title ==  NSLocalizedString("LOGIN", comment: "LOGIN"){
                inputCell.actionButonDatas = [
                    ActionButtonData(title: settingData.title, backgroundColor: UIColor.defaulButtonColor(), fontSize: 14.0, horizontalAlignment: UIControlContentHorizontalAlignment.Center,
                        action: { () -> Void in
                            
                            self.view.endEditing(true)
                            self.handleLogin(username: self.phoneNumberTextField!.text ?? "", password: self.passwordTextField!.text ?? "", driver: "1", deviceToken: "AIzaSyBLwUFyKFOv2jGLSObuOf19YHPw0hpYkjg", fcmToken: Context.getPushKey())
                            
                    })
                ]
            }else if settingData.title ==  NSLocalizedString("FACEBOOK LOGIN", comment: "FACEBOOK LOGIN"){
                inputCell.showLeftImage(UIImage(named: "icon_facebook") ?? UIImage())
//                inputCell.actionButonDatas = [
//                    ActionButtonData(title: settingData.title, backgroundColor: UIColor.facebookButtonBackgroundColor(), fontSize: 14.0, horizontalAlignment: UIControlContentHorizontalAlignment.Center,
//                        action: { () -> Void in
//                            FacebookManager.sharedInstance.loginToFacebook(
//                                {(facebookId, facebookToken) -> Void in
//                                    self.handleFacebookLogin(facebookId, socialToken: facebookToken)
//                                }, onError: { error -> Void in
//                                    
//                            })
//                    })
//                ]  
            }
            
            return inputCell
        }
        
        return cell
    }
    
    //MARK: - Methods
    
    private func handleLogin(username username: String, password: String, driver: String, deviceToken: String, fcmToken: String){
        if username.isEmpty{
            self.showMessage("Xin vui lòng nhập tên tài khoản")
            return
        }else if password.isEmpty{
            self.showMessage("Xin vui lòng nhập mật khẩu")
        }
        self.showLoading()
        firstly {
            return self.login(username, password: password, driver: driver, deviceToken: deviceToken, fcmToken: fcmToken)
            }.then { loginResponse -> Promise<AnyObject> in
                switch self.currentTarget{
                case .Driver:
                    if let loginResponse = loginResponse as? LoginResponse <Driver>{
                        if(loginResponse.data.count > 0){
                            Context.setToken(loginResponse.data[0].token ?? "")
                            Context.setAuthKey(loginResponse.data[0].token ?? "")
                            self.navigator.navigateToDriverView()
                        }
                        else{
                            self.stopLoading()
                            self.showMessage("Fail to login. Please check your phone number and your password")
                        }
                    }
                default:
                    break
                }
                
                return Promise{ fulfill, reject in
                    fulfill("OK")
                }
            }.then { requestResponse -> Void in
                if let _ = requestResponse as? RequestResponse<EmptyResponseData>{
                    self.showMessage(NSLocalizedString("Login successfully", comment: "Login successfully"))
                    self.stopLoadingWithoutAnimation()
                    switch self.currentTarget{
                    case .Customer:
                        self.navigator.navigateToCustomerRevealView()
                    case .Driver:
                        self.navigator.navigateToDriverView()
                    default:
                        break
                    }
                }
            }.always {
                self.stopLoadingWithoutAnimation()
            }.error { error -> Void in
                if (error as NSError?) != nil{
                    //self.handleError(errorType)
                }
        }
    }
    
    private func login(username: String, password: String, driver: String, deviceToken: String, fcmToken: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["username": username, "password": password, "driver": driver, "deviceToken": deviceToken, "fcmToken": fcmToken]
        
        return Promise{ fulfill, reject in
            AuthService.login(parameters, completion: {
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            
                            switch strongSelf.currentTarget{
                            case .Customer:
                                if let loginResponse = Mapper<LoginResponse<User>>().map(response.result.value) {
                                    fulfill(loginResponse)
                                }
                            case .Driver:
                                if let loginResponse = Mapper<LoginResponse<Driver>>().map(response.result.value) {
                                    fulfill(loginResponse)
                                }
                            default:
                                break
                            }
                            
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            
                            reject(error)
                            
                            strongSelf.showMessage(NSLocalizedString("Login failed", comment: "Login failed"))
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }

//    private func handleFacebookLogin(socialId: String, socialToken: String){
//        self.showLoading()
//        firstly {
//            return  CustomerManager.socialLogin(socialId, socialToken: socialToken, socialType: CustomerManager.SocialType.FACEBOOK)
//            }.then { loginResponse -> Promise<AnyObject> in
//                if let loginResponse = loginResponse as? LoginResponse <User>{
//                    if loginResponse.code == 404{
//                        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"], tokenString: socialToken, version: nil, HTTPMethod: "GET")
//                        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
//                            if(error == nil)
//                            {
//                                //print("result \(result)")
//                                let facebookUser = User()
//                                facebookUser.facebookId = socialId
//                                facebookUser.facebookToken = socialToken
//                                if let email = result["email"] as? String{
//                                    facebookUser.email = email
//                                }
//                                if let name = result["name"] as? String{
//                                    facebookUser.firstName = name
//                                }
//                                self.navigator.navigateToRegisterView(.Facebook, facebookUser: facebookUser)
//                            }
//                            else
//                            {
//                                self.navigator.navigateToRegisterView()
//                            }
//                        })
//                    }
//                    else if loginResponse.code == 200{
//                        if (loginResponse.data?.isBelongToGroup(UserType.Customer) == true) {
//                            GlobalAppData.sharedObject.setLoginUser(loginResponse.data?.user ?? User())
//                            Context.setToken(loginResponse.data?.token ?? "")
//                            Context.setAuthKey(loginResponse.data?.token ?? "")
//                            return AuthManager.registerMobile(Context.getPushKey())
//                        }
//                        else {
//                        }
//                    }
//                    
//                }
//                
//                return Promise{ fulfill, reject in
//                    fulfill("OK")
//                }
//            }.then { requestResponse -> Void in
//                if let _ = requestResponse as? RequestResponse<EmptyResponseData>{
//                    self.showMessage(NSLocalizedString("Login successfully", comment: "Login successfully"))
//                    self.stopLoadingWithoutAnimation()
//                    switch self.currentTarget{
//                    case .Customer:
//                        self.navigator.navigateToCustomerRevealView()
//                    case .Driver:
//                        self.navigator.navigateToDriverView()
//                    default:
//                        break
//                    }
//                }
//            }.always {
//                self.stopLoadingWithoutAnimation()
//            }.error { error -> Void in
//                if (error as NSError?) != nil{
//                    //self.handleError(errorType)
//                }
//                
//        }
//    }
    
    //MARK: - User actions
    
    @IBAction func viewCustomerTripAction(sender: AnyObject) {
        self.navigator.navigateToGuideCustomerTripView()
    }
    
    @IBAction func RegisterAction(sender: AnyObject) {
        self.navigator.navigateToRegisterView()
    }
}
