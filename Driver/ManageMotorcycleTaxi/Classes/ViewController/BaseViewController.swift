//
//  BaseViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/5/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    var navigator: Navigator!
    
    var endTripMessageView: EndTripMessageView!
    var messageView: MessageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        self.navigationController?.navigationBar.format()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let endTripMessageView = NSBundle.mainBundle().loadNibNamed("EndTripMessageView", owner: self, options: nil)![0] as? EndTripMessageView{
            self.endTripMessageView = endTripMessageView
            self.endTripMessageView.frame = self.view.frame
        }
        if let messageView = NSBundle.mainBundle().loadNibNamed("MessageView", owner: self, options: nil)![0] as? MessageView{
            self.messageView = messageView
            self.messageView.frame = self.view.frame
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if !Reachability.isConnectedToNetwork() {
            self.showAlertWindowWithOK(NSLocalizedString("Please check the Internet connection.", comment: "Please check the Internet connection."), okAction: { () -> Void in
            })
        }
    }
    
    func showLoading(){
        self.stopLoading()
        self.navigationController?.view.userInteractionEnabled = false
        self.navigationController?.view.makeToastActivity((self.navigationController?.view.center)!)
        
//        self.stopLoading({ success -> Void in
//            self.navigationController?.view.userInteractionEnabled = false
//            self.navigationController?.view.makeToastActivity((self.navigationController?.view.center)!)
//        })
    }
    
    func stopLoading(completion: (Bool -> ())? = nil){
        self.navigationController?.view.hideToastActivity({ (finished) -> Void in
            self.navigationController?.view.userInteractionEnabled = true
            if let action = completion{
               action(finished)
            }
        })
    }
    
    func stopLoadingWithoutAnimation(){
        self.navigationController?.view.userInteractionEnabled = true
        self.navigationController?.view.hideToastActivity2()
    }
    
    func showMessage(message: String?){
        self.view.makeToast(message ?? "")
    }
    
    func showMessageAtCenter(message: String?){
        self.view.makeToastAtCenter(message ?? "")
    }
    
    func showErrorMessage() {
        self.showMessage(NSLocalizedString("An error has occurred, please try again later.", comment: "An error has occurred, please try again later."))
    }
    
    func handleError(error: NSError?){
        if (error as NSError?) != nil{
            //self.showMessage(error!.localizedDescription)
            if error?.code == -1009 {
                self.showMessage(NSLocalizedString("Network error", comment: "Network error"))
            }
        }
    }
    
    func addDefaultGradientLayer(){
        let blueLayer = UIColor.defaultGradient()
        blueLayer.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height)
        self.view.layer.insertSublayer(blueLayer, atIndex: 0)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    func hideNavigationBar(isHidden: Bool){
        self.navigationController?.setNavigationBarHidden(isHidden, animated: true)
    }
    
    func visibleDefaultBackgroundImage(isVisible: Bool){
        
        if isVisible{
            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
        }
        else{
            self.view.backgroundColor = UIColor.defaultBackgroundColor()
        }
    }
    
    func showEndTripNotificationView(isShow: Bool, tripHistory: Trip){
        if isShow{
            endTripMessageView.loadView(tripHistory: tripHistory)
            self.view.addSubview(endTripMessageView)
        }
        else{
            endTripMessageView.removeFromSuperview()
        }
    }
    
    func showMessageView(isShow: Bool, title: String? = "", message: String? = "") -> MessageView{
        if isShow{
            messageView.title = title
            messageView.message = message
            self.view.addSubview(messageView)
        }
        else{
            messageView.removeFromSuperview()
        }
        return messageView
    }
    
    func hideBackBarBtn(isHidden: Bool) {
        self.navigationItem.hidesBackButton = isHidden
    }

}
