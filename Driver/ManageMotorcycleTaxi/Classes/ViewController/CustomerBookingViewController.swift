//
//  CustomerBookingViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/13/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import GoogleMaps
import PromiseKit
import ObjectMapper
import AlamofireImage

let kLocationSelectionViewHeight: CGFloat = 50.0
let kLocationSelectionViewIndent: CGFloat = 10.0
let kLocationSelectionViewTopIndent: CGFloat = 10.0

let kDistancePredictViewHeight: CGFloat = 50.0
let kPricePredictViewHeight: CGFloat = 30.0
let kTransportTypeViewHeight: CGFloat = 70.0
let kBookingButtonSize: CGFloat = 80.0

//Tag of view in OriginLocationView
let kOSVOriginTitleLblTag = 100
let kOSVOriginValueLblTag = 101

//Tag of view in DestinationLocationView
let kDSVDestinationTitleLblTag = 100
let kDSVDestinationValueLblTag = 101
let kDSVDestinationBtnTag = 102

//Tag of view in DistancePredictView
let kDPVDistanceLblTag = 100
let kDPVTimeLblTag = 101

//Tag of view in FarePredictView
let kFPVFareEstimateValueTag = 100

//Tag of Booking Button
let kBBBookingBtnTag = 100
let kBBBookingLblTag = 101

let kTagTTVLblFareEst = 110
let kTagTTVViewMotobike = 210
let kTagTTVViewCar4Slot = 220
let kTagTTVViewCar7Slot = 230
let kTagTTVViewCar16Slot = 240

class CustomerBookingViewController: BaseViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UISearchDisplayDelegate {

    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var shareButton: UIBarButtonItem!

//    @IBOutlet var extraButton:UIBarButtonItem!
    
    var viewMap: GMSMapView!
    
    var locationManager = CLLocationManager()
    
    var didFindMyLocation = false
    
    var mapTasks = MapTasks()
    
    var locationMarker: GMSMarker!
    
    var originMarker: GMSMarker!
    
    var destinationMarker: GMSMarker!
    var tmpMarker: GMSMarker!

    var routePolyline: GMSPolyline!
    
    var markersArray: Array<GMSMarker> = []
    
    var waypointsArray: Array<String> = []
    
    var travelMode = TravelModes.driving
    
    //autocomplete Google API
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    // origin & destination selection view
    var originSelectionView: UIView!
    var destinationSelectionView: UIView!
    var detailView:UIView!
    var originValueLbl: MarqueeLabel!
    var destinationValueLbl: MarqueeLabel!
    var destChooseOnMapBtn: UIButton!
    var isOriginBtnTap = false
    var isChooseDesOnMap = false
    
    //distance predict view
    var distancePredictLbl: UILabel!
    var timeDurationPredictLbl: UILabel!
    var distancePredictView: UIView!
    var distanceLbl: UILabel?
    
    //booking button
    var bookingBtnView: UIView!
    var bookingBtn: UIButton!
    
    //fare estimate &
    var farePredictView: UIView!
    var fareEstimateLbl: UILabel!
    
    //transport type view
    var transportTypeCollectionView: UICollectionView!
    var transportPresentativeList = GlobalAppData.sharedObject.getTransportPresentativeList()
    
    var trip = Trip()
    
    var firstTransportImageView: UIImageView!
    var isAddObserverMyLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = NSLocalizedString("BOOKING", comment: "BOOKING")
        self.initRevealViewController()
        self.initShareButton()
        detailView = self.view.viewWithTag(100)! as UIView
        if (self.navigator == nil) {
            self.navigator = Navigator(storyboard: Storyboards.customerBooking, navigationController: self.navigationController!)
        }
        
        PushManager.sharedObject.handleReceivedLaunchingNotification()
        
        self.getCurrentTrip()
        
        if GlobalAppData.sharedObject.getPendingOrderTrip() != nil {
            self.showAlertWindow(NSLocalizedString("You have 1 trip awaiting confirmation?", comment: "You have 1 trip awaiting confirmation?"), message: NSLocalizedString("Would you like to continue?", comment: "Would you like to continue?"), okAction: { () -> Void in
                let bookingConfirmVC = self.navigator.navigateToCustomerBookingConfirmView()
                bookingConfirmVC.trip = GlobalAppData.sharedObject.getPendingOrderTrip()
                }, cancelAction: { () -> Void in
                    GlobalAppData.sharedObject.setPendingOrderTrip(nil)
            })
            
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.initMapView()
        self.initDistancePredictView()
        self.initLocationSelectionView()
        self.initPricePredictView()
        self.initTransportTypeView()
        self.initBookingBtn()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        viewMap.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
        isAddObserverMyLocation = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomerBookingViewController.sideMenuBarTap(_:)), name: Constants.UI.SideMenuBar.SideBarNotifName, object: nil)
        
        // The myLocation attribute of the mapView may be null
        if let mylocation = viewMap.myLocation {
            print("User's location: \(mylocation)")
        } else {
            print("User's location is unknown")
        }
        self.createRoute2({(status, success) -> Void in })
        
        if Context.getFirstLaunchKey() != "NO"{
            Context.setFirstLaunchKey("NO")
            self.showGuideTour()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        if (isAddObserverMyLocation == true) {
            viewMap.removeObserver(self, forKeyPath: "myLocation")
            isAddObserverMyLocation = false
        }
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if !didFindMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
            viewMap.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: 10.0)
            viewMap.settings.myLocationButton = false
            didFindMyLocation = true
        }
    }
    
    //MARK: UI
    func initRevealViewController() {
        if revealViewController() != nil {
            //            revealViewController().rearViewRevealWidth = 62
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    func initShareButton() {
        shareButton.target = self
        shareButton.action = #selector(CustomerBookingViewController.shareButtonTap)
    }
    
    func shareButtonTap() {
        let textToShare = NSLocalizedString("AVIGo Slogan", comment: "AVIGo Slogan");
        
        if let myWebsite = NSURL(string: Constants.AppstoreURL) {
            let objectsToShare = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.showLoading()
            activityVC.popoverPresentationController?.sourceView = self.view
            self.presentViewController(activityVC, animated: true, completion: { () -> Void in
                self.stopLoadingWithoutAnimation()
            })
            
        }
    }
    
    func initMapView() {
        if (viewMap == nil) {
            //request user location
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            
            //default camera to TNS airport
            let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(Constants.TSNAirportInfo.latitude, longitude: Constants.TSNAirportInfo.longitude, zoom: 12.0)
            viewMap = GMSMapView.init(frame: CGRectMake(0, 0, detailView.frame.size.width, detailView.frame.size.height))
            
            viewMap.camera = camera
            viewMap.delegate = self
            viewMap.settings.compassButton = true
            viewMap.settings.myLocationButton = false
            viewMap.myLocationEnabled = true
            //        viewMap.myLocationEnabled = true
            let mapInsets = UIEdgeInsets(top: 200.0, left: 0.0, bottom: 100.0, right: 0)
            viewMap.padding = mapInsets
            
            detailView.addSubview(viewMap)
        }
    }
    
    func initLocationSelectionView() {
        if (originSelectionView == nil) {
            //origin location button
            originSelectionView = NSBundle.mainBundle().loadNibNamed("OriginSelectionView", owner: self, options: nil)![0] as? UIView
            originSelectionView.frame = CGRectMake(kLocationSelectionViewIndent, kLocationSelectionViewTopIndent, detailView.frame.size.width - 2*kLocationSelectionViewIndent, kLocationSelectionViewHeight)
            originSelectionView.roundCornersWithBorder(1.0, borderColor: UIColor.lightGrayColor().CGColor, radius: 2.0)
            originSelectionView.alpha = 0.9
            let originViewTap = UITapGestureRecognizer.init(target: self, action: #selector(CustomerBookingViewController.originSearchTap(_:)))
            originSelectionView.addGestureRecognizer(originViewTap)
            originValueLbl = originSelectionView.viewWithTag(kOSVOriginValueLblTag) as! MarqueeLabel
            originValueLbl.fadeLength = 30.0
            
            detailView.addSubview(originSelectionView)
            trip.originPlace = GMSCustomPlace(name: NSLocalizedString(Constants.TSNAirportInfo.name, comment: "Tan Son Nhat Airport"), latitude: Constants.TSNAirportInfo.latitude, longitude: Constants.TSNAirportInfo.longitude, formattedAddress: Constants.TSNAirportInfo.name)
            viewMap.camera = GMSCameraPosition.cameraWithTarget(trip.originPlace.coordinate, zoom: 14.0)
            originValueLbl.text = self.trip.originPlace.name
            self.configOriginMarker()
        }

        if (destinationSelectionView == nil) {
            //destination location button
            destinationSelectionView = NSBundle.mainBundle().loadNibNamed("DestinationSelectionView", owner: self, options: nil)![0] as? UIView
            destinationSelectionView.frame = CGRectMake(kLocationSelectionViewIndent, kLocationSelectionViewHeight + kLocationSelectionViewTopIndent - 1, detailView.frame.size.width - 2*kLocationSelectionViewIndent, kLocationSelectionViewHeight)
            destinationSelectionView.roundCornersWithBorder(1.0, borderColor: UIColor.lightGrayColor().CGColor, radius: 2.0)
            let destinationViewTap = UITapGestureRecognizer.init(target: self, action: #selector(CustomerBookingViewController.desSearchTap(_:)))
            destinationSelectionView.addGestureRecognizer(destinationViewTap)
            destinationValueLbl = destinationSelectionView.viewWithTag(kDSVDestinationValueLblTag) as! MarqueeLabel
            destinationValueLbl.fadeLength = 30.0
            destinationSelectionView.alpha = 0.9

            destChooseOnMapBtn = destinationSelectionView.viewWithTag(kDSVDestinationBtnTag) as! UIButton
            destChooseOnMapBtn.addTarget(self, action: #selector(CustomerBookingViewController.desChooseOnMapTap(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    
            detailView.addSubview(destinationSelectionView)
        }
    }
    
    func initDistancePredictView() {
        if (distancePredictView == nil) {
            distancePredictView = NSBundle.mainBundle().loadNibNamed("DistancePredictView", owner: self, options: nil)![0] as? UIView
            distancePredictView.frame = CGRectMake(kLocationSelectionViewIndent, kLocationSelectionViewHeight + kLocationSelectionViewTopIndent - 2, detailView.frame.size.width - 2*kLocationSelectionViewIndent, kLocationSelectionViewHeight)
            distancePredictView.alpha = 0.9

            distancePredictView.roundCornersWithBorder(1.0, borderColor: UIColor.lightGrayColor().CGColor, radius: 2.0)
            detailView.addSubview(distancePredictView)
            distancePredictView.hidden = true
            distancePredictLbl = distancePredictView.viewWithTag(kDPVDistanceLblTag) as! UILabel
            timeDurationPredictLbl = distancePredictView.viewWithTag(kDPVTimeLblTag) as! UILabel
        }
    }

    func initPricePredictView() {
        if (farePredictView == nil) {
            farePredictView = NSBundle.mainBundle().loadNibNamed("FarePredictView", owner: self, options: nil)![0] as? UIView
            farePredictView.frame = CGRectMake(0, detailView.frame.size.height - kTransportTypeViewHeight, detailView.frame.size.width, kPricePredictViewHeight)

            fareEstimateLbl = farePredictView.viewWithTag(kFPVFareEstimateValueTag) as! UILabel
            detailView.addSubview(farePredictView)
        }
    }
    
    func initTransportTypeView() {
        if (transportTypeCollectionView == nil) {
            let layout = UICollectionViewFlowLayout()
            layout.itemSize = CGSizeMake(100, 100)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
            layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
            if transportPresentativeList.count > 0{
                if let transport = transportPresentativeList[0] as? Transport {
                    self.trip.vehicleId = transport.id
                    transportTypeCollectionView = UICollectionView.init(frame: CGRectMake(0, detailView.frame.size.height - kTransportTypeViewHeight, detailView.frame.size.width, kTransportTypeViewHeight), collectionViewLayout: layout)
                    transportTypeCollectionView.pagingEnabled = true
                    transportTypeCollectionView.dataSource = self
                    transportTypeCollectionView.delegate = self
                    transportTypeCollectionView.registerNib(UINib.init(nibName:"TransportTypeSelectionCell", bundle: nil), forCellWithReuseIdentifier: TransportTypeSelectionCell.cellIdentifier)
                    transportTypeCollectionView.backgroundColor = UIColor.whiteColor()
                    transportTypeCollectionView.showsHorizontalScrollIndicator = false
                    detailView.addSubview(transportTypeCollectionView)
                }
            }
        }
    }
    
    func initBookingBtn() {
        if (bookingBtnView == nil) {
            bookingBtnView = NSBundle.mainBundle().loadNibNamed("BookingButtonView", owner: self, options: nil)![0] as? UIView
            let detailViewHeight = detailView.frame.size.height
            let detailViewWidth = detailView.frame.size.width
            bookingBtnView.frame = CGRectMake(detailViewWidth - kBookingButtonSize - 10, detailViewHeight-kTransportTypeViewHeight - kBookingButtonSize, kBookingButtonSize, kBookingButtonSize)
            bookingBtn = bookingBtnView.viewWithTag(kBBBookingBtnTag) as! UIButton
            bookingBtn.addTarget(self, action: #selector(CustomerBookingViewController.bookingBtnTap), forControlEvents: UIControlEvents.TouchUpInside)
            detailView.addSubview(bookingBtnView)
        }

    }
    
    // MARK: Init Data
    func getCurrentTrip(){
        self.showLoading()
        firstly {
            return CustomerManager.getCurrentTrip()
            }.then { [weak self] getCurrentTripResponse -> Void in
                if let strongSelf = self{
                    strongSelf.stopLoading({ (finished) in
                        if let getCurrentTripResponse = getCurrentTripResponse as? TripArrayResponse {
                            let tripHistories = getCurrentTripResponse.data ?? [Trip]()
                            
                            if let tripHistory = tripHistories.first{
                                let tripStatus = tripHistory.tripStatus
                                switch tripStatus {
                                case .INITIAL:
                                    let historyDetailVC = OrderHistoryDetailViewController()
                                    historyDetailVC.tripHistory = tripHistory
                                    strongSelf.navigator.navigationController.pushViewController(historyDetailVC, animated: true)
                                case .WAITING, .READY, .RUNNING, .WAITING_CUSTOMER:
                                    let vc = strongSelf.navigator.navigateToCustomerTripView()
                                    vc.tripHistory = tripHistory
                                    break
                                case .END:
                                    strongSelf.navigator.navigateToHistoryDetailView(tripHistory)
                                    break
                                case .TERMINATED:
                                    break
                                default:
                                    break
                                }
                            }
                        }
                    })
                }
                
            }.always {
                self.stopLoading()
            }.error { error -> Void in
                self.stopLoading()
                if let errorType = error as NSError?{
                    self.showMessage(errorType.localizedDescription)
                }
        }
    }
    
    // MARK: CLLocationManagerDelegate method implementation
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            viewMap.myLocationEnabled = true
        }
    }
    
    // MARK: MAPTASK method implementation
    
    func createRoute(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, completionHandler: ((status: String, success: Bool) -> Void)) {
        self.mapTasks.getDirections(origin, destination: destination, waypoints: nil, travelMode: self.travelMode, completionHandler: { (status, success) -> Void in
            if success {
                self.trip.initWithMaptask(self.mapTasks)
                self.configureMapAndMarkersForRoute()
                self.drawRoute()
                self.displayRouteInfo()
                completionHandler(status: status, success: true)
            }
            else {
                print(status)
                completionHandler(status: status, success: false)
            }
        })
    }
    
    func createRoute2(completionHandler: ((status: String, success: Bool) -> Void)) {
        if (trip.originPlace != nil && trip.destinationPlace != nil) {
            self.clearRoute()
            //            self.createRoute(originPlace.coordinate, destination: trip.destinationPlace.coordinate)
            self.createRoute(trip.originPlace.coordinate, destination: trip.destinationPlace.coordinate, completionHandler: { (status, success) -> Void in
                completionHandler(status: status, success:success)
            })
        }
        else if trip.originPlace != nil {
            self.mapTasks.originCoordinate = trip.originPlace.coordinate
            self.configOriginMarker()
        }
        else if trip.destinationPlace != nil {
            self.mapTasks.destinationCoordinate = trip.destinationPlace.coordinate
            self.configDestinationMaker()
        }
    }
    
    func configureMapAndMarkersForRoute() {
        self.configOriginMarker()
        self.configDestinationMaker()
        
        if waypointsArray.count > 0 {
            for waypoint in waypointsArray {
                let lat: Double = (waypoint.componentsSeparatedByString(",")[0] as NSString).doubleValue
                let lng: Double = (waypoint.componentsSeparatedByString(",")[1] as NSString).doubleValue
                
                let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                marker.map = viewMap
                marker.icon = GMSMarker.markerImageWithColor(UIColor.purpleColor())
                
                markersArray.append(marker)
            }
        }
    }
    
    //fix bound camera for origion mark & destination mark
    func fixBoundCamera() {
        let bound = GMSCoordinateBounds.init(coordinate: self.mapTasks.originCoordinate, coordinate: self.mapTasks.destinationCoordinate)
        viewMap.moveCamera(GMSCameraUpdate.fitBounds(bound))
    }
    
    func configOriginMarker() {
        if (originMarker != nil) {
            originMarker.map = nil
            originMarker = nil
        }
        originMarker = GMSMarker(position: trip.originPlace.coordinate)
        originMarker.map = self.viewMap
        originMarker.icon = Utils.resizeImage(UIImage(named: "icon_diemdi") ?? UIImage(), targetSize: CGSize(width: 30, height: 30*206/150))
        
        originMarker.title = trip.originPlace.name
    }
    
    func configDestinationMaker() {
        if (destinationMarker == nil) {
            destinationMarker = GMSMarker(position: trip.destinationPlace.coordinate)
            destinationMarker.map = self.viewMap
            destinationMarker.icon = Utils.resizeImage(UIImage(named: "icon_diemden") ?? UIImage(), targetSize: CGSize(width: 30, height: 30*206/150))
            
            destinationValueLbl.textColor = UIColor.blackColor()
        }

        destinationMarker.position = trip.destinationPlace.coordinate
        destinationMarker.title = trip.destinationPlace.name
        destinationValueLbl.text = trip.destinationPlace.name
    }
    
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)!
        routePolyline = GMSPolyline(path: path)
        routePolyline.strokeWidth = Constants.UI.MapView.StrokeWidth
        routePolyline.strokeColor = UIColor.defaultAppColor()
        routePolyline.map = viewMap
    }
    
    func displayRouteInfo() {
        distancePredictLbl.text = trip.getKilometerExpectedString()
        timeDurationPredictLbl.text = trip.getDurationExpectedString()
        if trip.isMotobikeTrip() {
            fareEstimateLbl.text = trip.getPriceExpectedString()
        }
        else {
            fareEstimateLbl.text = trip.getPriceRangeExpectedString(trip.vehicleId)
        }
        if (trip.originPlace != nil && trip.destinationPlace != nil) {
            if (self.distancePredictView.frame.origin.y < 2*kLocationSelectionViewHeight + 2) {
                UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                    self.distancePredictView.hidden = false
                    self.distancePredictView.frame = CGRectMake(kLocationSelectionViewIndent, 2*kLocationSelectionViewHeight + kLocationSelectionViewTopIndent - 2, self.detailView.frame.size.width - 2*kLocationSelectionViewIndent, kDistancePredictViewHeight)
                    }, completion: nil)
            }
            if (self.farePredictView.frame.origin.y > detailView.frame.size.height - kTransportTypeViewHeight - kPricePredictViewHeight) {
                UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                    self.farePredictView.frame = CGRectMake(0, self.detailView.frame.size.height - kTransportTypeViewHeight - kPricePredictViewHeight, self.detailView.frame.size.width, kPricePredictViewHeight)
                    }, completion: nil)
            }
        }
    }
    
    func clearRoute() {
        if (originMarker != nil) {
            originMarker.map = nil
            originMarker = nil
        }
        if (destinationMarker != nil) {
            destinationMarker.map = nil
            destinationMarker = nil
        }
        if (routePolyline != nil) {
            routePolyline.map = nil
            routePolyline = nil
        }
        if markersArray.count > 0 {
            for marker in markersArray {
                marker.map = nil
            }
            
            markersArray.removeAll(keepCapacity: false)
        }
    }
    
    func findDestinationPlace(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
//                print(address)
                self.trip.destinationPlace = GMSCustomPlace(name: address.lines![0], latitude: address.coordinate.latitude, longitude: address.coordinate.longitude, formattedAddress: address.lines![0])
                self.configDestinationMaker()
                self.createRoute2({(status, success) -> Void in })
            } else {
                // Here the response is 'nil'
            }
        }
    }
    
    // MARK: GMSMapViewDelegate method implementation
    func mapView(mapView: GMSMapView, idleAtCameraPosition cameraPosition: GMSCameraPosition) {
//        print("idleAtCameraPosition \(cameraPosition)")
        if (isChooseDesOnMap) {
            self.findDestinationPlace(cameraPosition.target)
        }
    }
    
    func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition) {
//        self.trip.destinationPlace = GMSCustomPlace(name: "", latitude: position.target.latitude, longitude: position.target.longitude, formattedAddress: "")
        if (isChooseDesOnMap) {
            self.trip.destinationPlace = GMSCustomPlace(name: "", latitude: position.target.latitude, longitude: position.target.longitude, formattedAddress: "")
            self.configDestinationMaker()
        }
    }
    
    // MARK: IBAction method implementation
    @IBAction func originSearchTap(sender: AnyObject) {
        isOriginBtnTap = true
//        self.pushGMSAutocompleteViewController()
    }

    @IBAction func desSearchTap(sender: AnyObject) {
        isOriginBtnTap = false
        isChooseDesOnMap = false
        self.pushGMSAutocompleteViewController()
    }
    
    //choose destination on Map
    @IBAction func desChooseOnMapTap(sender: AnyObject) {
        self.handleDesChooseOnMap()
    }
    
    private func handleDesChooseOnMap(){
        isChooseDesOnMap = !isChooseDesOnMap
        var imageName = "icon_search"
        if (isChooseDesOnMap) {
            imageName = "icon_auto_search"
            let cameraPosition = viewMap.camera.target
            self.findDestinationPlace(cameraPosition)
        }
        destChooseOnMapBtn.setImage(UIImage(named: imageName), forState: .Normal)
    }
    
    private func enableAutoChooseDesOnMap(){
        isChooseDesOnMap = true
        let cameraPosition = viewMap.camera.target
        self.findDestinationPlace(cameraPosition)
        destChooseOnMapBtn.setImage(UIImage(named: "icon_auto_search"), forState: .Normal)
    }
    
    //booking button tap
    @IBAction func bookingBtnTap(sender: AnyObject) {
        if (trip.originPlace != nil && trip.destinationPlace != nil && trip.destinationPlace.name != "") {
            let bookingConfirmVC = self.navigator.navigateToCustomerBookingConfirmView()
            self.trip.originPlace.name = NSLocalizedString(Constants.TSNAirportInfo.name, comment: "Tan Son Nhat Airport")
            bookingConfirmVC.trip = self.trip
        }
        else {
            self.showMessage(NSLocalizedString("Please select specify your destination", comment: "Please select specify your destination"))
        }
    }
    
    private func pushGMSAutocompleteViewController() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.NoFilter  //suitable filter type
        filter.country = "VN"  //appropriate country code
        autocompleteController.autocompleteFilter = filter
        self.presentViewController(autocompleteController, animated: true, completion: nil)
    }
    
    //MARK: observe MenuBar notification
    func sideMenuBarTap(notification: NSNotification){
        //Take Action on Notification
        var userInfo = notification.userInfo
        let value = userInfo!["key"] as! String
        switch value {
        case Constants.UI.SideMenuBar.SideBarNotifProfile:
            if Context.getAuthKey() != ""{
                self.navigator.navigateToProfileView()
            }else{
                self.showAlertWindow(NSLocalizedString("Please login to use this feature", comment: "Please login to use this feature"), okAction: { () -> Void in
                    NavigationManager.sharedObject.navigateToLoginView()
                })
            }
            break
        case Constants.UI.SideMenuBar.SideBarNotifBooking:
            break
        case Constants.UI.SideMenuBar.SideBarNotifGuide:
            
            self.showGuideTour()

            break
        case Constants.UI.SideMenuBar.SideBarNotifHistory:
            if Context.getAuthKey() != ""{
                self.navigator.navigateToHistoryView()
            }else{
                self.showAlertWindow(NSLocalizedString("Please login to use this feature", comment: "Please login to use this feature"), okAction: { () -> Void in
                    NavigationManager.sharedObject.navigateToLoginView()
                })
            }
            break
        case Constants.UI.SideMenuBar.SideBarNotifSupport:
            self.navigator.navigateToSupportView()
            break
        case Constants.UI.SideMenuBar.SideBarNotifPromotion:
            self.navigator.navigateToPromotionView()
            break
        case Constants.UI.SideMenuBar.SideBarNotifLogOut:
            if Context.getAuthKey().isEmpty{
                self.navigator.navigateToCustomerHomeView()
            }
            else{
                self.showAlertWindow(NSLocalizedString("Do you really want to log out?", comment: "Do you really want to log out?"), okAction: { () -> Void in
                    self.logout()
                })
            }
            
            break
        default:
            break
        }
    }
    
    private func logout(){
        self.showLoading()
        firstly {
            return UserManager.logout()
        }.then { requestResponse -> Void in
            if let _ = requestResponse as? RequestResponse<EmptyResponseData>{
                self.showMessage(NSLocalizedString("Logout successfully", comment: "Logout successfully"))
                Context.setAuthKey("")
                GlobalAppData.sharedObject.setLoginUser(User())
                
            }
            self.stopLoading({ finished -> Void in
                self.navigator.navigateToCustomerHomeView()
            })
            
        }.always {
                self.stopLoading()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.showMessage(NSLocalizedString("Logout failed", comment: "Logout failed"))
                self.navigator.navigateToCustomerHomeView()
        }
    }
    
    private func showGuideTour(){
        self.enableAutoChooseDesOnMap()
        
        GuideTourManager.sharedObject.resetGuideTourItems()
        
        GuideTourManager.sharedObject.addGuideTourItem(GuideTourItem(view: destChooseOnMapBtn, superView: detailView, title: NSLocalizedString("Specify the end point", comment: "Specify the end point"), message: NSLocalizedString("Touch the icon to begin specifing the end point", comment: "Touch the icon to begin specifing the end point"), action: { () -> () in
            
        }))
        
        let markerView = UIView(frame: CGRectMake(detailView.frame.midX - 50,detailView.frame.midY - 100, 100, 100))
        detailView.addSubview(markerView)
        GuideTourManager.sharedObject.addGuideTourItem(GuideTourItem(view: markerView, superView: detailView, title: NSLocalizedString("Specify the end point",comment: "Specify the end point") , message: NSLocalizedString("Drag the map to determine the destination is indicated by a red icon",comment: "Drag the map to determine the destination is indicated by a red icon"), action: { () -> () in
            markerView.removeFromSuperview()
        }))
        
        if firstTransportImageView != nil{
            GuideTourManager.sharedObject.addGuideTourItem(GuideTourItem(view: firstTransportImageView, superView: detailView, title: NSLocalizedString("Specify vehicle type",comment: "Specify vehicle type"), message: NSLocalizedString("Touch this icon to specify vehicle type",comment: "Touch this icon to specify vehicle type"), action: { () -> () in
                
            }))
        }
        
        
        GuideTourManager.sharedObject.addGuideTourItem(GuideTourItem(view: bookingBtn, superView: detailView, title: NSLocalizedString("Booking", comment: "Booking"), message: NSLocalizedString("Touch this icon to begin booking",comment: "Touch this icon to begin booking"), action: { () -> () in
            
        }))
        
        GuideTourManager.sharedObject.startTour()
    }
}

// MARK: GMSAutocompleteViewControllerDelegate
extension CustomerBookingViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(viewController: GMSAutocompleteViewController, didAutocompleteWithPlace place: GMSPlace) {
        self.dismissViewControllerAnimated(true, completion: nil)
        if isOriginBtnTap {
            originValueLbl.text = NSLocalizedString(Constants.TSNAirportInfo.name, comment: "Tan Son Nhat Airport")
            trip.originPlace = GMSCustomPlace(place: place)
            trip.originPlace.name = NSLocalizedString(Constants.TSNAirportInfo.name, comment: "Tan Son Nhat Airport")
//            print(place.coordinate)
        }
        else {
//           print(place)
            destinationValueLbl.text = place.name
            destinationValueLbl.textColor = UIColor.blackColor()
            trip.destinationPlace = GMSCustomPlace(place: place)
//            print(place.coordinate)
        }
        self.createRoute2({ (status, success) -> Void in
            if (success) {
                self.fixBoundCamera()
            }
        })
    }
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        print("Error: ", error.description)
    }
    
    // User canceled the operation.
    func wasCancelled(viewController: GMSAutocompleteViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
//        self.navigationController?.popToViewController(self, animated: true)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
}

extension CustomerBookingViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return transportPresentativeList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let rowNum = indexPath.row
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(TransportTypeSelectionCell.cellIdentifier, forIndexPath: indexPath)
        if let inputCell = cell as? TransportTypeSelectionCell{
            inputCell.backgroundColor = UIColor.whiteColor()
            inputCell.alpha = 0.3
            inputCell.transportTypeImgV.image = UIImage.init(named: "ic_transport" + String(rowNum))
            let transport = transportPresentativeList[rowNum] as! Transport
            inputCell.setImageView(transport.icon)
            inputCell.setTransportTypeLabel(NSLocalizedString(transport.name, comment: ""))
            if (trip.getTransport().slot_number == transport.slot_number) {
                inputCell.alpha = 1.0
            }
            
            if rowNum == 0{
                firstTransportImageView = inputCell.transportTypeImgV
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if (transportPresentativeList.count < 5 && transportPresentativeList.count > 0) {
            return CGSizeMake(self.view.width()/CGFloat(transportPresentativeList.count) - 1.0, collectionView.frame.size.height)
        }
        else {
            return CGSizeMake(self.view.width()/4.0 - 1.0, collectionView.frame.size.height)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let rowNum = indexPath.row
        if let transport = transportPresentativeList[rowNum] as? Transport {
            self.trip.vehicleId = transport.id
            let offset = collectionView.contentOffset
            print(offset)
            collectionView.reloadData()
            collectionView.layoutIfNeeded()
            collectionView.setContentOffset(offset, animated: true)
            //collectionView.contentOffset = offset
            trip.numberOfBike = 1
            trip.isRoundTrip = 0
            trip.calculatePrice()
            self.displayRouteInfo()
        }
    }
}

