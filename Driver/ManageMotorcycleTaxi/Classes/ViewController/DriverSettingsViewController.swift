//
//  DriverSettingsViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/5/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import TOCropViewController
import UIKit

class DriverSettingsViewController: ProfileViewController {
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.collectionView.frame = self.view.frame
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 40.0, 0)
        self.handleGetProfile()
    }
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let settingData = settingDatas[indexPath.row]
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(settingData.identifier, forIndexPath: indexPath)
        
        if let inputCell = cell as? ProfileCollectionCell{
            inputCell.roundCornersForCell([.BottomLeft, .BottomRight, .TopLeft, .TopRight], radius: 5)
            
            inputCell.title = settingData.title
            inputCell.image = settingData.image
            inputCell.inputTextField.placeholder = settingData.placeHolder
            inputCell.inputTextField.tag = indexPath.row
            inputCell.inputTextField.delegate = self
            inputCell.inputTextField.userInteractionEnabled = false
            if let profileRow = ProfileItemRow(rawValue: indexPath.row){
                switch profileRow {
                case .Name:
                    inputCell.inputTextField.text = "\(loginUser.fullname ?? "")"
                    break
                case .PhoneNumer:
                    inputCell.inputTextField.text = "\(loginUser.phone ?? "")"
                    break
                case .Email:
                    inputCell.inputTextField.text = "\(loginUser.email ?? "")"
                    break
                    
                }
            }
            
            return inputCell
        }
        else if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = settingData.title
            
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            
            if settingData.title == NSLocalizedString("CHANGE PASSWORD", comment: "CHANGE PASSWORD"){
                inputCell.actionButonDatas = [ActionButtonData(title: settingData.title, backgroundColor: UIColor.whiteColor(), titleColor: UIColor.defaultAppColor(), borderWidth: 2, borderColor: UIColor.defaultButtonBorderColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center,
                    action: { () -> Void in
                        if let action = settingData.action{
                            action()
                        }
                })
                ]
            }else if settingData.title == NSLocalizedString("SIGN OUT", comment: "SIGN OUT"){
                inputCell.actionButonDatas = [ActionButtonData(title: settingData.title, backgroundColor: UIColor.whiteColor(), titleColor: UIColor.redColor(), borderWidth: 2, borderColor: UIColor.defaultButtonBorderColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center,
                    action: { () -> Void in
                        if let action = settingData.action{
                            action()
                        }
                })
                ]
            }
            
            return inputCell
        }
        
        return cell
    }
    
    override func showImagePickerView() {
        //do nothing
    }
    
    override func navigateToHomeView(){
        NavigationManager.sharedObject.navigateToLoginView()
    }
    func handleGetProfile(){
        self.showLoading()
        firstly {
            return self.getProfile()
            }.then { profileResponse -> Promise<AnyObject> in
                    if let profileResponse = profileResponse as? ProfileResponse{
                        if(profileResponse.data.count > 0){
                            self.loginUser = profileResponse.data[0]
                        }
                    }
                return Promise{ fulfill, reject in
                    fulfill("OK")
                }
            }.then { requestResponse -> Void in
                if let _ = requestResponse as? RequestResponse<EmptyResponseData>{
                    self.showMessage(NSLocalizedString("Login successfully", comment: "Login successfully"))
                    self.stopLoadingWithoutAnimation()
                }
            }.always {
                self.stopLoadingWithoutAnimation()
                 self.collectionView.reloadData()
            }.error { error -> Void in
                if (error as NSError?) != nil{
                    //self.handleError(errorType)
                }
                
        }
    }
    func getProfile() -> Promise<AnyObject>{
        let parameters: [String: AnyObject] = [:]
        
        return Promise{ fulfill, reject in
            DriverService.getProfile(parameters, completion: {
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            if let profileResponse = Mapper<ProfileResponse>().map(response.result.value) {
                                fulfill(profileResponse)
                            }
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            
                            reject(error)
                            
                            strongSelf.showMessage(NSLocalizedString("Get profile failed", comment: "Get profile failed"))
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }
}
