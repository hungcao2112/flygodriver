//
//  DriverEndTripInfoCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class DriverEndTripInfoCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    static let cellIdentifier = "DriverEndTripInfoCellID"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
