//
//  MobileVerificationViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/7/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit

class MobileVerificationViewController: BaseCollectionViewController {
    
    enum VerificationMode: Int{
        case Unknown = 0,
        PhoneNumber,
        ForgotPass
    }
    
    private let numberOfRow = 3
    
    private var settingDatas = [SettingData]()
    
    var codeData: CodeResponseData?
    var registerResponseData: RegisterResponseData?
    
    var confirmCodeCell: ConfirmCodeCollectionCell?
    
    var currentVerificationMode = VerificationMode.Unknown
    
    var registerMode: RegisterMode = .Normal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpDataSource()
        
        self.setUpCollectionView()
    }
    
    func setUpCollectionView(){
        self.collectionView.registerClass(InputCollectionViewCell.self, forCellWithReuseIdentifier: InputCollectionViewCell.cellIdentifier)
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ConfirmCodeCollectionCell.self, forCellWithReuseIdentifier: ConfirmCodeCollectionCell.cellIdentifier)
    }
    
    func setUpDataSource(){
        settingDatas.append(SettingData(title: NSLocalizedString("Vui lòng nhập mã xác nhận đã được gởi qua số điện thoại di động của quý khách", comment: "Vui lòng nhập mã xác nhận đã được gởi qua số điện thoại di động của quý khách"), identifier: IntroCollectionCell.cellIdentifier))
        
        settingDatas.append(SettingData(identifier: ConfirmCodeCollectionCell.cellIdentifier, height: ConfirmCodeCollectionCell.defaultHeight))
        
        settingDatas.append(SettingData(title: NSLocalizedString("Gởi lại mã xác nhận", comment: "Gởi lại mã xác nhận"), identifier: IntroCollectionCell.cellIdentifier))
        
        settingDatas.append(SettingData(title: NSLocalizedString("CONFIRM", comment: "CONFIRM"), identifier: ActionCollectionCell.cellIdentifier))
    }
    
    //MARK: - CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.row == 0{
            return CGSizeMake(self.view.width() - 20, InputCollectionViewCell.defaultHeight + 20)
        }
        return CGSizeMake(self.view.width() - 20, InputCollectionViewCell.defaultHeight)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settingDatas.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let settingData = settingDatas[indexPath.row]
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(settingData.identifier, forIndexPath: indexPath)
        
        if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = settingData.title
            
            return inputCell
        }
        else if let inputCell = cell as? ConfirmCodeCollectionCell{
            confirmCodeCell = inputCell
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            inputCell.actionButonDatas = [ActionButtonData(title: settingData.title, backgroundColor: UIColor.defaulButtonColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center)]
            inputCell.actionHandler = { (button) -> Void in
                self.activeTextField?.resignFirstResponder()
                
                switch self.currentVerificationMode {
                case .PhoneNumber:
                    self.verifyPhoneNumber(self.confirmCodeCell?.getCode() ?? "" , id: (self.registerResponseData?.id)!)
                    break
                case .ForgotPass:
                    //self.verifyCode(self.confirmCodeCell?.getCode() ?? "" , phone: self.codeData?.account ?? "")
                    break
                default:
                    break
                }
                
            }
            
            return inputCell
        }
        
        return cell
    }
    
//    private func verifyCode(code: String, phone: String){
//        if code.characters.count < 4{
//            self.showMessage(NSLocalizedString("Please enter verification code", comment: "Please enter verification code"))
//            return
//        }
//        
//        self.showLoading()
//        
//        firstly {
//            return AuthManager.verifyCode(code: code, account: phone)
//            }.then { codeResponse -> Void in
//                if let codeResponse = codeResponse as? CodeResponse{
//                    let vc = self.navigator.navigateToUpdatePasswordView(codeResponse.data ?? CodeResponseData())
//                    vc.viewMode = UpdatePasswordMode.Forgot
//                    vc.title = NSLocalizedString("FORGOT PASSWORD", comment: "FORGOT PASSWORD")
//                }
//                
//            }.always {
//                self.stopLoading()
//            }.error { error -> Void in
//                if let errorType = error as NSError?{
//                    self.handleError(errorType)
//                }
//                self.showMessage(NSLocalizedString("Verify code failed", comment: "Verify code failed"))
//                
//        }
//    }
    
    private func verifyPhoneNumber(code: String, id: String){
        if code.characters.count < 4{
            self.showMessage(NSLocalizedString("Please enter verification code", comment: "Please enter verification code"))
            return
        }
        
        self.showLoading()
        
        if registerMode == .Facebook{
//            firstly {
//                return AuthManager.socialPhoneVerification(code: code, phone: phone)
//                }.then { loginResponse -> Void in
//                    self.stopLoading()
//                    if let _ = loginResponse as? LoginResponse<User>{
//                        NavigationManager.sharedObject.navigateToLoginView()
//                    }
//                    
//                }.always {
//                    self.stopLoading()
//                }.error { error -> Void in
//                    if let errorType = error as NSError?{
//                        self.handleError(errorType)
//                    }
//                    self.stopLoading()
//                    self.showMessage(NSLocalizedString("Verify code failed", comment: "Verify code failed"))
//                    
//            }
        }
        else{
            firstly {
                return AuthManager.phoneVerification(code: code, id: id)
                }.then { loginResponse -> Void in
                    self.stopLoading()
                    if let _ = loginResponse as? LoginResponse<LoginResponseData>{
                        NavigationManager.sharedObject.navigateToLoginView()
                    }
                    
                }.always {
                    self.stopLoading()
                }.error { error -> Void in
                    if let errorType = error as NSError?{
                        self.handleError(errorType)
                    }
                    self.stopLoading()
                    self.showMessage(NSLocalizedString("Verify code failed", comment: "Verify code failed"))
                    
            }
        }
    }
}
