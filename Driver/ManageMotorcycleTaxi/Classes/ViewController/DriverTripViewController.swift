//
//  DriverHomeViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/5/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import GoogleMaps
import PromiseKit
//import FirebaseDatabase
import Firebase

class DriverTripViewController: BaseViewController {
    @IBOutlet weak var mapView: CustomMapView!
    var endAddressView = AddressView()
    
    @IBOutlet weak var startEndButton: UIButton!
    @IBOutlet weak var timeCountingLbl: UILabel!
    var isFameSet = false
    
    var driverTrip = DriverTrip()
    var trip = Trip()

    var completedTripCollectionView: UICollectionView!
    var blackBackgroundView = UIView()
    var cellIdentifiers = [String]()
    var ref: FIRDatabaseReference!
    var lookupAddressResults = Dictionary<NSObject, AnyObject>()
    var fetchedAddressLatitude: Double! = 0
    var fetchedAddressLongitude: Double! = 0
    var fetchedFormattedAddress: String! = ""
    var destinationMarker: GMSMarker!
    var myLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        driverTrip = GlobalAppData.sharedObject.getCurrentTrip()
        
        self.hideBackBarBtn(true)
        self.title = driverTrip.code
        self.ref = FIRDatabase.database().reference()
        getLatLong(driverTrip.address_b)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        mapView.isAutoDrawPolyline = false
        mapView.isEnableCurrentMarker = false
        mapView.initLocationManager()
        
        if self.checkLocationEnabled(){
            mapView.startUpdatingLocation()
        }
        
        let mapInsets = UIEdgeInsets(top: 100.0, left: 0.0, bottom: 50.0, right: 0)
        mapView.padding = mapInsets
        mapView.customMapViewDelegate = self
        self.handleTripStatus()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let addressView = NSBundle.mainBundle().loadNibNamed("AddressView", owner: self, options: nil)![0] as? UIView
        
        if !isFameSet{
            if let addressView = addressView as? AddressView{
                addressView.frame = CGRectMake(10, self.mapView.frame.minY + 10, self.view.frame.width - 20, 50)
                addressView.backgroundColor = UIColor(white: 1.0, alpha: 0.8)
                addressView.layer.cornerRadius = Constants.UI.defaulCornerRadius
                addressView.layer.borderWidth = 1.0
                addressView.layer.borderColor = UIColor.defaultBorderColor().CGColor
                addressView.addressLabel.fadeLength = 10.0
                addressView.addressText = driverTrip.address_b ?? ""
                addressView.iconImageView.image = UIImage(named: "icon_diemden")
                self.endAddressView = addressView
                self.view.addSubview(endAddressView)
            }
            
            isFameSet = true
        }
        self.initCompletedTripCollectionView()
    }
    
    func initCompletedTripCollectionView() {
        if var completedTripCollectionView = completedTripCollectionView{
                cellIdentifiers.append(DriverEndTripHeadCell.cellIdentifier)
                cellIdentifiers.append(DriverEndTripInfoCell.cellIdentifier)
                cellIdentifiers.append(DriverEndTripInfoCell.cellIdentifier)
                cellIdentifiers.append(DriverEndTripInfoCell.cellIdentifier)
                cellIdentifiers.append(DriverEndTripInfoCell.cellIdentifier)
                cellIdentifiers.append(DriverEndTripFareCell.cellIdentifier)
                cellIdentifiers.append(IntroCollectionCell.cellIdentifier)
                cellIdentifiers.append(ActionCollectionCell.cellIdentifier)
                
                let layout = UICollectionViewFlowLayout()
                layout.itemSize = CGSizeMake(100, 100)
                layout.minimumInteritemSpacing = 0
                layout.minimumLineSpacing = 0
                completedTripCollectionView = UICollectionView.init(frame: CGRectMake(20, self.mapView.frame.minY + 10, self.view.frame.width - 40, self.mapView.frame.maxY), collectionViewLayout: layout)
                completedTripCollectionView.dataSource = self
                completedTripCollectionView.delegate = self
                completedTripCollectionView.registerNib(UINib.init(nibName:"DriverEndTripHeadCell", bundle: nil), forCellWithReuseIdentifier: DriverEndTripHeadCell.cellIdentifier)
                completedTripCollectionView.registerNib(UINib.init(nibName:"DriverEndTripInfoCell", bundle: nil), forCellWithReuseIdentifier: DriverEndTripInfoCell.cellIdentifier)
                completedTripCollectionView.registerNib(UINib.init(nibName:"DriverEndTripFareCell", bundle: nil), forCellWithReuseIdentifier: DriverEndTripFareCell.cellIdentifier)
                completedTripCollectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
                completedTripCollectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
                
                completedTripCollectionView.backgroundColor = UIColor.whiteColor()
                completedTripCollectionView.hidden = true
                completedTripCollectionView.roundCornersWithBorder(1.0, borderColor: UIColor.lightGrayColor().CGColor, radius: 10.0)
                blackBackgroundView = UIView.init(frame: CGRectMake(0, 0, self.view.width(), self.view.height()))
                blackBackgroundView.backgroundColor = UIColor.blackColor()
                blackBackgroundView.alpha = 0.8
                blackBackgroundView.hidden = true
                self.view.addSubview(blackBackgroundView)
                self.view.addSubview(completedTripCollectionView)
                
            }
    }
    func getLatLong(strAddress: String) -> Void {
        
        self.geocodeAddress(strAddress, withCompletionHandler: { (status, success) -> Void in
            if !success {
                print(status)
                
                if status == "ZERO_RESULTS" {
                }
            }
            else {
                let coordinate = CLLocationCoordinate2D(latitude: self.fetchedAddressLatitude, longitude: self.fetchedAddressLongitude)
                //                self.viewMap.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 14.0)
                
                self.trip.destinationPlace = GMSCustomPlace(name: self.fetchedFormattedAddress, latitude: self.fetchedAddressLatitude, longitude:self.fetchedAddressLongitude, formattedAddress: self.fetchedFormattedAddress)
                self.configDestinationMaker()
                //self.createRoute2({(status, success) -> Void in })
                self.mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
                self.mapView.myLocationEnabled = true
                print("myLocation: \(self.myLocation), destination: \(self.trip.destinationPlace.coordinate)")
                print("myLocation: \(self.mapView.myLocation)")
            }
        })
        
    }
    func geocodeAddress(address: String!, withCompletionHandler completionHandler: ((status: String, success: Bool) -> Void)) {
        let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
        
        if let lookupAddress = address {
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress
            geocodeURLString = geocodeURLString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            
            let geocodeURL = NSURL(string: geocodeURLString)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let geocodingResultsData = NSData(contentsOfURL: geocodeURL!)
                
                var error: NSError?
                let dictionary: Dictionary<NSObject, AnyObject>
                
                do {
                    dictionary = try NSJSONSerialization.JSONObjectWithData(geocodingResultsData!, options: NSJSONReadingOptions.MutableContainers) as! Dictionary<NSObject, AnyObject>
                    
                    if (error != nil) {
                        print(error)
                        completionHandler(status: "", success: false)
                    }
                    else {
                        // Get the response status.
                        let status = dictionary["status"] as! String
                        
                        if status == "OK" {
                            let allResults = dictionary["results"] as! Array<Dictionary<NSObject, AnyObject>>
                            self.lookupAddressResults = allResults[0]
                            //
                            //                        // Keep the most important values.
                            self.fetchedFormattedAddress = self.lookupAddressResults["formatted_address"] as! String
                            let geometry = self.lookupAddressResults["geometry"] as! Dictionary<NSObject, AnyObject>
                            self.fetchedAddressLongitude = ((geometry["location"] as! Dictionary<NSObject, AnyObject>)["lng"] as! NSNumber).doubleValue
                            self.fetchedAddressLatitude = ((geometry["location"] as! Dictionary<NSObject, AnyObject>)["lat"] as! NSNumber).doubleValue
                            
                            completionHandler(status: status, success: true)
                        }
                        else {
                            completionHandler(status: status, success: false)
                        }
                    }
                } catch {
                    // failure
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                
            })
        }
        else {
            completionHandler(status: "No valid address.", success: false)
        }
    }
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
         let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
        self.myLocation = myLocation
        print("mylocation: \(myLocation.coordinate)")
        self.createRouteByPlace(GMSCustomPlace(name: "myLocation", coordinate: self.myLocation.coordinate), destinationPlace: self.trip.destinationPlace, completionHandler: { success -> Void in
            print("create route success")
        })
    }
    func configDestinationMaker() {
        if (destinationMarker == nil) {
            destinationMarker = GMSMarker(position: trip.destinationPlace.coordinate)
            destinationMarker.map = self.mapView
            destinationMarker.icon = Utils.resizeImage(UIImage(named: "icon_diemden") ?? UIImage(), targetSize: CGSize(width: 30, height: 30*206/150))
            
            self.endAddressView.addressLabel.textColor = UIColor.blackColor()
        }
        
        destinationMarker.position = trip.destinationPlace.coordinate
        destinationMarker.title = trip.destinationPlace.name
        self.endAddressView.addressLabel.text = trip.destinationPlace.name
    }
    func handleTripStatus () {
//        trip = GlobalAppData.sharedObject.getCurrentTrip()
//        switch trip.tripStatus {
//        case TripStatus.READY:
//            print("trip READY: \(trip.tripStatus)")
//            startEndButton.setBackgroundImage(UIImage.init(named: "icon_trip_start"), forState: UIControlState.Normal)
//            startEndButton.setTitle(NSLocalizedString("START", comment: "START"), forState: UIControlState.Normal)
//            break
//        default:
//            print("trip default: \(trip.tripStatus)")
//            startEndButton.setBackgroundImage(UIImage.init(named: "icon_trip_stop"), forState: UIControlState.Normal)
//            startEndButton.setTitle(NSLocalizedString("END", comment: "END"), forState: UIControlState.Normal)
//            break
//        }
    }
    
    @IBAction func startEndButtonTap(sender: UIButton) {
//        if (self.checkLocationEnabled() == true) {
//            switch trip.tripStatus {
//            case TripStatus.READY:
//                self.handleStartDrive()
//                break
//            default:
//                self.showAlertWindow(NSLocalizedString("Do you really want to complete trip?", comment: "Do you really want to complete trip?"), okAction: { () -> Void in
//                    self.handleGetEndLocation()
//                })
//                break
//            }
//        }
        print("AAA")
        self.showLoading()
        firstly {
            return DriverManager.acceptTrip("180202CUWD")
            }.then { response -> Void in
                if let response = response as? SingleTripResponse {
                    let assignTrip = response.data?.trip
                    //GlobalAppData.sharedObject.setCurrentTrip(assignTrip)
                    self.stopLoadingWithoutAnimation()
                    self.showMessage(response.message ?? "")
                    //self.navigateToDriverHomeView()
                }
            }.always {
                self.stopLoadingWithoutAnimation()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
        }
    }
    
    func handleStartDrive() {
//        self.showLoading()
//        firstly {
//            return DriverManager.startDrive(trip.codeNumber)
//            }.then { responseObject -> Void in
//                if let response = responseObject as? SingleTripResponse {
//                    let assignTrip = response.data?.trip
//                    //GlobalAppData.sharedObject.setCurrentTrip(assignTrip!)
//                    self.handleTripStatus()
//                }
//            }.always {
//                self.stopLoadingWithoutAnimation()
//            }.error { error -> Void in
//                if let errorType = error as NSError?{
//                    self.handleError(errorType)
//                }
//        }
    }
    
    func handleGetEndLocation() {
//        self.ref.child("driverConfig").observeSingleEventOfType(.Value, withBlock: { (snapshot) in
//            if (snapshot.value as? [String : AnyObject]) != nil {
//                if let value = snapshot.value as? [String: NSNumber] {
//                    if let isUsingExpectLocation = value["isUsingExpectLocation"] as NSNumber! {
//                        if isUsingExpectLocation == 1 {
//                            self.trip.destinationActualPlace = self.trip.destinationPlace
//                            self.handleEndTrip()
//                            return
//                        }
//                    }
//                }
//            }
//            
//            self.mapView.getBestLocation()
//            if (self.mapView.currentLocation as CLLocation?) != nil {
//                self.mapView.findPlaceByCoordinate((self.mapView.currentLocation?.coordinate)!, completionHandler: { (address, success) -> Void in
//                    if success {
//                        print(address)
//                        self.trip.destinationActualPlace = GMSCustomPlace(name: address.lines![0], latitude: address.coordinate.latitude, longitude: address.coordinate.longitude, formattedAddress: address.lines![0])
//                        self.createRouteByPlace(self.trip.originPlace, destinationPlace: self.trip.destinationActualPlace, completionHandler: {(status, success) -> Void in
//                            self.handleEndTrip()
//                        })
//                    }
//                    else {
//                        
//                    }
//                })
//            }
//            else {
//                //self.showMessage(NSLocalizedString("Please turn on Location Service to perform this operation.", comment: ""))
//            }
//        })
    }
    
    func handleEndTrip() {
//        if (trip.destinationActualPlace as GMSCustomPlace?) != nil {
//            self.showLoading()
//            firstly {
//                return DriverManager.endTrip(trip.codeNumber, endPoint: trip.destinationActualPlace.name, endLatitude: trip.destinationActualPlace.latitude, endLongitude: trip.destinationActualPlace.longitude, kilometer: Double(mapView.mapTasks.totalDistanceInMeters) / 1000.0, duration: Int(mapView.mapTasks.totalDurationInSeconds / 60))
//                }.then { responseObject -> Void in
//                    print(responseObject)
//                    if let response = responseObject as? SingleTripResponse {
//                        if let assignTrip = response.data?.trip as Trip! {
//                            //GlobalAppData.sharedObject.setCurrentTrip(assignTrip)
//                            self.trip = assignTrip
//                            self.blackBackgroundView.hidden = false
//                            self.completedTripCollectionView.hidden = false
//                            self.completedTripCollectionView.reloadData()
//                        }
//                    }
//                }.always {
//                    self.stopLoadingWithoutAnimation()
//                }.error { error -> Void in
//                    if let errorType = error as NSError?{
//                        self.handleError(errorType)
//                    }
//            }
//        }
    }
    
    @IBAction func timeCountTapped(sender: AnyObject) {
    }
    
    @IBOutlet weak var addTapped: UIButton!
    
    func createRouteByPlace(originPlace: GMSCustomPlace, destinationPlace: GMSCustomPlace, completionHandler: ((status: String, success: Bool) -> Void)) {
        self.mapView.clear()
        self.mapView.drawRoute(CLLocationCoordinate2D(latitude: originPlace.latitude, longitude: originPlace.longitude), originAddress: originPlace.formattedAddress, destination: CLLocationCoordinate2D(latitude: destinationPlace.latitude, longitude: destinationPlace.longitude), destinationAddress: destinationPlace.formattedAddress, travelMode: TravelModes.driving) { (status, success) in
                self.mapView.fixBoundCamera()
                completionHandler(status: status, success:success)
        }

    }
}

extension DriverTripViewController: CustomMapViewDelegate {
    func didUpdateLocation(location: CLLocation?) {
//        if let strongLocation = location{
//            if self.trip.tripStatus == TripStatus.RUNNING {
//                print("didUpdateLocations")
//                let locationDict = ["latitude":strongLocation.coordinate.latitude, "longitude":strongLocation.coordinate.longitude]
//                let currentTime = Int64(NSDate().timeIntervalSince1970 * 1000)
//                print(locationDict)
//                print(currentTime)
//                self.ref.child("locations/\(trip.codeNumber)/\(currentTime)").setValue(locationDict)
//            }
//        }
    }
    
    func didUpdateLocations(locations: [CLLocation]) {

    }
    
    func didChangeAuthorizationStatus(status: CLAuthorizationStatus) {
        
    }
}

extension DriverTripViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellIdentifiers.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let rowNumber = indexPath.row
        let cellIdentifier = cellIdentifiers[rowNumber]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        if let inputCell = cell as? DriverEndTripHeadCell {
            inputCell.startAddressLabel.text  = driverTrip.address_a
            inputCell.endAddressLabel.text = driverTrip.address_b
            inputCell.titleLabel.text = NSLocalizedString("Ended trip", comment: "Ended trip")
            return inputCell
        }
        else if let inputCell = cell as? DriverEndTripInfoCell {
            switch rowNumber {
            case 1:
                inputCell.titleLabel.text = NSLocalizedString("Distance", comment: "Ended trip")
                inputCell.detailLabel.text = driverTrip.distance
                break
            case 2:
                inputCell.titleLabel.text = NSLocalizedString("Transport fee", comment: "Ended trip")
                inputCell.detailLabel.text = driverTrip.total
                break
            case 3:
                inputCell.titleLabel.text = NSLocalizedString("Incurred fee", comment: "Ended trip")
                inputCell.detailLabel.text = "0K"

                break
            case 4:
                inputCell.titleLabel.text = NSLocalizedString("Total fee", comment: "Ended trip")
                inputCell.detailLabel.text = ""

                break
            default:
                break
            }
            return inputCell
        }
        else if let inputCell = cell as? DriverEndTripFareCell {
            inputCell.fareLabel.text = driverTrip.total
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            inputCell.actionButonDatas = [ActionButtonData(title: NSLocalizedString("TRIP ENDS", comment: "TRIP ENDS"), backgroundColor: UIColor.defaulButtonColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                self.handleCompleteTripButton()
            })
            ]
            
            return inputCell
        }
        return cell
    }
    
    func handleCompleteTripButton() {
        let controller = Storyboards.driver.instantiateViewControllerWithIdentifier(Constants.StoryBoard.Driver.DriverViewID)
            as! DriverViewController
        controller.navigator = self.navigator
        if let nv = self.navigationController{
            nv.setViewControllers([controller], animated: true)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let rowNumber = indexPath.row
        if (rowNumber == 0) {
            return CGSizeMake(collectionView.frame.size.width, Constants.UI.CollectionCellHeight.HistoryCellHeight)
        }
        return CGSizeMake(collectionView.frame.size.width - 20, Constants.UI.CollectionCellHeight.DriverEndTripInfoCellHeight)
    }

}
