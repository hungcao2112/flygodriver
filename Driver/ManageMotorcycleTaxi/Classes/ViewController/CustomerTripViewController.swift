//
//  CustomerTripViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import GoogleMaps
import PromiseKit
import FirebaseDatabase
import Firebase

class CustomerTripViewController: BaseViewController {
    
    @IBOutlet weak var driverInfoView: DriverInfoView!
    @IBOutlet weak var mapView: CustomMapView!
    var endAddressView = AddressView()
    @IBOutlet weak var tripStatusLabel: UILabel!
    @IBOutlet weak var tripStatusView: UIView!
    private var isFameSet = false
    var tripHistory = Trip()
    private var driver : Driver?
    var ref: FIRDatabaseReference!
    var lastCoordinate: CLLocationCoordinate2D?
    var lastMarker: GMSMarker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("TRIP", comment: "TRIP")
        
        mapView.currentMarker.icon = Utils.resizeImage(UIImage(named: "icon_run") ?? UIImage(), targetSize: CGSize(width: 40, height: 40))
        mapView.customMapViewDelegate = self
        mapView.isAutoDrawPolyline = true
        mapView.initLocationManager()
        mapView.polylineColor = UIColor.defaultAppColor()
        mapView.startUpdatingLocation()
        self.ref = FIRDatabase.database().reference()

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if tripHistory.tripStatus == TripStatus.RUNNING {
            self.ref.child("locations/\(tripHistory.codeNumber)").observeEventType(.ChildAdded, withBlock: { (snapshot) in
                if (snapshot.value as? [String : AnyObject]) != nil {
                    self.handleNewLocation(snapshot)
                }
            })
        }
    }
    
    func handleNewLocation(snapshot: FIRDataSnapshot) {
        if let title = snapshot.key as String? {
            if let value = snapshot.value as? [String: NSNumber] {
                if let long = value["longitude"] as NSNumber! {
                    if let lat = value["latitude"] as NSNumber! {
                        print(value)
                        let coor = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                        let cameraPosition = GMSCameraPosition.cameraWithTarget(coor, zoom: self.mapView.camera.zoom)
                        self.mapView.camera = cameraPosition
                        
                        if let coordinate = lastCoordinate{
                            self.mapView.addPolyline(coordinate, endCoordinate: coor)
                        }
                        
                        if let marker = lastMarker{
                            marker.map = nil
                        }
                        
                        lastMarker = self.mapView.addNewMarker(coor, title: title)
                        
                        lastCoordinate = coor
                    }
                }
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let view1 = NSBundle.mainBundle().loadNibNamed("DriverInfoView", owner: self, options: nil)![0] as? UIView
        
        let endAddressView = NSBundle.mainBundle().loadNibNamed("AddressView", owner: self, options: nil)![0] as? UIView
        
        if !isFameSet{
            if let driverInfoView = view1 as? DriverInfoView{
                driverInfoView.driverImageView.roundImage()
                driverInfoView.frame = CGRectMake(self.driverInfoView.minX(), self.driverInfoView.minY(), self.view.frame.width, self.driverInfoView.height())
                driverInfoView.callDriverAction = { () -> Void in
                    if let driver = self.driver{
                        if let url = NSURL(string: "tel://" + driver.phone){
                            UIApplication.sharedApplication().openURL(url)
                        }
                    }
                }
                self.driverInfoView = driverInfoView
                driverInfoView.hidden = true
                self.view.addSubview(driverInfoView)
            }
            
            if let endAddressView = endAddressView as? AddressView{
                endAddressView.frame = CGRectMake(10, self.mapView.frame.minY + 10, self.view.frame.width - 20, 50)
                endAddressView.backgroundColor = UIColor(white: 1.0, alpha: 0.8)
                endAddressView.layer.cornerRadius = Constants.UI.defaulCornerRadius
                endAddressView.layer.borderWidth = 1.0
                endAddressView.layer.borderColor = UIColor.defaultBorderColor().CGColor
                endAddressView.iconImageView.image = UIImage(named: "icon_diemden")
                endAddressView.addressLabel.text = tripHistory.endPointExpected ?? ""
                endAddressView.addressLabel.fadeLength = 10.0
                self.endAddressView = endAddressView
                self.view.addSubview(endAddressView)
            }

            isFameSet = true
            
            switch tripHistory.tripStatus ?? TripStatus.UNKNOWN {
            case .WAITING, .READY:
                tripStatusLabel.text = NSLocalizedString("Waiting for driver", comment: "Waiting for driver")
                tripStatusView.backgroundColor = UIColor.defaultAppColor()
            case .RUNNING, .WAITING_CUSTOMER:
                tripStatusLabel.text = NSLocalizedString("Ongoing trip", comment: "Ongoing trip")
                tripStatusView.backgroundColor = UIColor.defaultGreenAppColor()
            default:
                tripStatusLabel.text = ""
                tripStatusView.backgroundColor = UIColor.defaultGreenAppColor()
            }
            
//            if tripHistory.coordinatesStart != nil && tripHistory.coordinatesEndExpected != nil{
                self.mapView.drawRoute(CLLocationCoordinate2D(latitude: tripHistory.coordinatesStart.latitude, longitude: tripHistory.coordinatesStart.longitude), originAddress: tripHistory.startPoint, destination: CLLocationCoordinate2D(latitude: tripHistory.coordinatesEndExpected.latitude, longitude: tripHistory.coordinatesEndExpected.longitude), destinationAddress: tripHistory.endPointExpected, travelMode: TravelModes.driving) { (status, success) in
                    
                }
//            }
            
            self.getDriverInfo()
        }
    }
    
    func getDriverInfo(){
        self.showLoading()
        firstly {
            return CustomerManager.getDriverInfo(driverNumber: self.tripHistory.driverNumber)
            }.then { [weak self] response -> Void in
                if let strongSelf = self{
                    strongSelf.stopLoading()
                    if let response = response as? LoginResponse<Driver>{
                        if let driver = response.data[0].user as? Driver{
                            strongSelf.driver = driver
                            if let nsUrl = NSURL(string: driver.avatar){
                                strongSelf.driverInfoView.driverImageView.af_setImageWithURL(
                                    nsUrl,
                                    placeholderImage: UIImage(named: "user"),
                                    filter: nil,
                                    imageTransition: .None,
                                    completion: { response in
                                })
                            }
                            
                            strongSelf.driverInfoView.driverName.text = driver.lastName
                            
                            let vehicle = driver.vehicle.filter{$0.vehicleStatus == 1}.first
                            
                            if let vehicle = vehicle as Vehicle? {
                                strongSelf.driverInfoView.vihicleTypeLbl.text = vehicle.vehicleType
                                strongSelf.driverInfoView.vihicleNumberLbl.text = vehicle.vehicleNumber
                            }
                        }
                    }
                    
                }
            }.always {
                self.stopLoading()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.stopLoading()
        }
    }
}

extension CustomerTripViewController : CustomMapViewDelegate{
    
    func didUpdateLocation(location: CLLocation?) {
        
    }
    
    func didUpdateLocations (locations: [CLLocation]){
        
    }
    
    func didChangeAuthorizationStatus(status: CLAuthorizationStatus) {
        
    }
}
