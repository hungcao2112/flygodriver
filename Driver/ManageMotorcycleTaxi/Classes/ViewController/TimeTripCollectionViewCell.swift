//
//  TimeTripCollectionViewCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/8/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class TimeTripCollectionViewCell: UICollectionViewCell {
    static let cellIdentifier = "TimeTripCollectionViewCellID"
    static let defaultHeight: CGFloat = 130
    
    @IBOutlet weak var timeLbl: UILabel!
    
    @IBOutlet weak var distanceLbl: UILabel!
    
    @IBOutlet weak var customBackgroundView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.customBackgroundView.layer.borderWidth = 2
//        self.customBackgroundView.layer.borderColor = UIColor.defaultBorderColor().CGColor
//        
//        self.customBackgroundView.layer.cornerRadius = 5
        
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.defaultBorderColor().CGColor
        
        self.layer.cornerRadius = 5
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
            }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
