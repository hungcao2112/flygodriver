//
//  InputCollectionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/6/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

//enum BorderEdge: Int {
//    case Left = 0,
//    Top,
//    Right,
//    Bottom
//}

class InputCollectionViewCell: UICollectionViewCell {
    
    static let cellIdentifier = "InputCollectionViewCellID"
    static let defaultHeight: CGFloat = 50
    
    private var containerView = UIView()
    
    var titleLabel = UILabel()
    var inputTextField = UITextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.defaultBorderColor()
        
        containerView.frame = CGRectMake(1, 1, self.width() - 2, self.height() - 2)
        containerView.backgroundColor = UIColor.whiteColor()
        addSubview(containerView)
        
        titleLabel.frame = CGRectMake(Constants.UI.Margin.Left, 0, self.width()/3 + 20, self.height())
        titleLabel.textColor = UIColor.primaryTextColor()
        titleLabel.textAlignment = NSTextAlignment.Right
        titleLabel.text = NSLocalizedString("", comment: "")
        containerView.addSubview(titleLabel)
        
        inputTextField.frame = CGRectMake(self.width()/3 + 2*Constants.UI.Margin.Left + 20, 0, 2 * self.width()/3 - 4*Constants.UI.Margin.Left, self.height())
        //inputTextField.font = UIFont(name: "arial", size: 17)
        //inputTextField.placeholder = NSLocalizedString("", comment: "")

        containerView.addSubview(inputTextField)
 
        //self.layer.borderColor = UIColor.defaultBorderColor().CGColor
        //self.layer.borderWidth = 2.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func roundCornersForCell(corners: UIRectCorner, radius: CGFloat) {
        containerView.roundCorners(corners, radius: radius - 2)
        self.roundCorners(corners, radius: radius)
    }
    
    func hideBoardEdge(borderEdge: BorderEdge){
        switch borderEdge {
        case .Left:
            containerView.frame = CGRectMake(0, 1, self.width() - 1, self.height() - 2)
            return
        case .Top:
            containerView.frame = CGRectMake(1, 0, self.width() - 2, self.height() - 1)
            return
        case .Right:
            containerView.frame = CGRectMake(1, 1, self.width() - 1, self.height() - 2)
            return
        case .Bottom:
            containerView.frame = CGRectMake(1, 1, self.width() - 2, self.height() - 1)
            return
        }
    }
}