//
//  DriverEndTripFareCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class DriverEndTripFareCell: UICollectionViewCell {

    @IBOutlet weak var fareLabel: UILabel!
    static let cellIdentifier = "DriverEndTripFareCellID"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
