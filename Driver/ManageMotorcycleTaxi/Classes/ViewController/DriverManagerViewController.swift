//
//  DriverManagerViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/21/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import PromiseKit
import KYNavigationProgress

class DriverManagerViewController: BaseCollectionViewController {

    var drivers = [Driver]()
    var filterDrivers = [Driver]()
    var isLoading = false
    var timer: NSTimer!
    @IBOutlet weak var searchingView: UIView!
    @IBOutlet weak var searchingTextField: UITextField!
    let kSearchingViewHeight = CGFloat(56.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUpCollectionView()
        self.hideBackBarBtn(true)
        self.title = NSLocalizedString("DRIVER MANAGEMENT", comment: "DRIVER MANAGEMENT")
        self.hideNavigationBar(false)
        self.setUpBarButtonItems()
        self.handleGetDrivers()
        self.didScrollToTop = { () -> Void in
            if self.isLoading == false {
                self.handleRefreshBtnTap()
            }
        }
        self.navigationController?.progressHeight = 3.0
        self.navigationController?.progressTintColor = UIColor.defaultAppYellowColor()
        timer = NSTimer.scheduledTimerWithTimeInterval(Constants.Timer.TimerInterval, target: self, selector: #selector(DriverManagerViewController.timerHandler), userInfo: nil, repeats: true)
        self.searchingView.backgroundColor = UIColor.defaultBackgroundColor()
        self.searchingTextField.addTarget(self, action: #selector(DriverManagerViewController.textFieldDidChangeHandler), forControlEvents: UIControlEvents.EditingChanged)
        self.searchingTextField.placeholder = NSLocalizedString("Searching", comment: "Searching")
        self.view.backgroundColor = UIColor.defaultBackgroundColor()
    }

    func setUpBarButtonItems() {
        let button: UIButton = UIButton(type: UIButtonType.Custom)
        button.setImage(UIImage(named: "icon_logout"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(DriverManagerViewController.handleLogoutBtnTap), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton

        let button2: UIButton = UIButton(type: UIButtonType.Custom)
        button2.setImage(UIImage(named: "icon_refresh"), forState: UIControlState.Normal)
        button2.addTarget(self, action: #selector(DriverManagerViewController.handleRefreshBtnTap), forControlEvents: UIControlEvents.TouchUpInside)
        button2.frame = CGRectMake(0, 0, 30, 30)
        let barButton2 = UIBarButtonItem(customView: button2)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton2
    }
    
    func setUpCollectionView(){
        self.collectionView!.registerNib(UINib(nibName: "DriverManagerCell", bundle: nil), forCellWithReuseIdentifier: DriverManagerCell.cellIdentifier)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let startPoint = (self.navigationController?.navigationBar.height())! + UIApplication.sharedApplication().statusBarFrame.height
        self.searchingView.frame = CGRectMake(0, startPoint, self.view.width(), kSearchingViewHeight)
        self.collectionView.frame = CGRectMake(0, self.searchingView.frame.height + kSearchingViewHeight, self.view.width(), self.view.height() - kSearchingViewHeight - startPoint)
        self.collectionView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if (timer != nil) {
            timer.invalidate()
            timer = nil
        }
        self.navigationController?.progress = 0.0
        self.navigationController?.cancelProgress()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func timerHandler() {
        let progress = navigationController!.progress
        navigationController!.setProgress(progress + Float(Constants.Timer.TimerInterval/Constants.Timer.TimerCoordinatorRefreshDriverStatus), animated: true)
        if progress > 0.99 {
            self.handleRefreshBtnTap()
        }
    }
    
    func handleGetDrivers() {
        if !self.isLoading {
            navigationController!.setProgress(0.0, animated: true)
            self.isLoading = true
            self.showLoading()
            firstly {
                return CoordinatorManager.getDrivers(1)
                }.then { response -> Void in
                    if let response = response as? CoordinatorGetDriversResponse<Driver> {
                        self.drivers.removeAll()
                        self.filterDrivers.removeAll()
                        for item in response.data {
                            if (item.isBelongToGroup(UserType.Driver) == true /*&& (item.user.isManageable())!*/) {
                                self.drivers.append(item.user as! Driver)
                                if item.user.code_number.containsString(self.searchingTextField.text!) == true {
                                    self.filterDrivers.append(item.user as! Driver)
                                }
                                else if self.searchingTextField.text == "" {
                                    self.filterDrivers.append(item.user as! Driver)
                                }
                            }
                        }
                        self.collectionView.reloadData()
                    }
                    self.isLoading = false
                    print("handleGetDrivers response")
                }.always {
                    print("handleGetDrivers always")
                    self.stopLoadingWithoutAnimation()
                }.error { error -> Void in
                    self.isLoading = false
                    if let errorType = error as NSError?{
                        self.handleError(errorType)
                    }
            }
        }
    }

    func handleLogoutBtnTap() {
        self.showAlertWindow(NSLocalizedString("Do you really want to log out?", comment: "Do you really want to log out?"), okAction: { () -> Void in
            self.logOut()
        })
    }
    
    func logOut() {
        self.showLoading()
        firstly {
            return UserManager.logout()
            }.then { requestResponse -> Void in
                if let _ = requestResponse as? RequestResponse<EmptyResponseData>{
                    self.showMessage(NSLocalizedString("Logout successfully", comment: "Logout successfully"))
                    Context.setAuthKey("")
                    GlobalAppData.sharedObject.setLoginUser(User())
                    self.stopLoadingWithoutAnimation()
                    self.navigationController?.cancelProgress()
                    NavigationManager.sharedObject.navigateToLoginView()
                }
            }.always {
                self.stopLoadingWithoutAnimation()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.showMessage(NSLocalizedString("Logout failed", comment: "Logout failed"))
        }
    }
    
    func handleRefreshBtnTap() {
        if (self.isLoading == false) {
            self.handleGetDrivers()
        }
    }
    
    //MARK: collectionview delegate
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterDrivers.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.width(), DriverManagerCell.defaultHeight)
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(DriverManagerCell.cellIdentifier, forIndexPath: indexPath)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        let rowNumer = indexPath.row
        let driver = filterDrivers[indexPath.row]
        print("driver: \(driver.account); status: \(driver.status); name: \(driver.lastName + " " + driver.middleName + " " + driver.firstName)")
        if let inputCell = cell as? DriverManagerCell{
            inputCell.avatarImgView.af_setImageWithURL(NSURL.init(string: driver.avatar)!)
            inputCell.nameLabel.text = driver.middleName + " " + driver.firstName
            inputCell.nameLabel.textColor = UIColor.defaultAppColor()
            if let codeNumber = driver.code_number as String? {
                if codeNumber.characters.count >= 3 {
                    let index: String.Index = codeNumber.endIndex.advancedBy(-3) // Swift 2
                    let lastCode = codeNumber.substringFromIndex(index)
                    inputCell.codeNumberLabel.text = lastCode
                }
            }
            inputCell.actionButton.addTarget(self, action: #selector(DriverManagerViewController.handleStatusBtnTap), forControlEvents: .TouchUpInside)
            inputCell.actionButton.tag = rowNumer
            let currentStatus = Int(driver.status!)
            switch currentStatus {
            case DriverStatus.Idle.rawValue:
                inputCell.actionButton.setTitle(NSLocalizedString("READY", comment: "READY"), forState: .Normal)
                inputCell.actionButton.backgroundColor = UIColor.defaultAppButtonGreenColor()
                break
            case DriverStatus.Ready.rawValue:
                inputCell.actionButton.setTitle(NSLocalizedString("CANCEL", comment: "CANCEL"), forState: .Normal)
                inputCell.actionButton.backgroundColor = UIColor.defaultAppYellowColor()
                break
            case DriverStatus.Driving.rawValue:
                inputCell.actionButton.setTitle(NSLocalizedString("END", comment: "END"), forState: .Normal)
                inputCell.actionButton.backgroundColor = UIColor.defaultAppRedColor()
                break
            default:
                inputCell.actionButton.setTitle("STATUS: \(currentStatus)", forState: .Normal)
                inputCell.actionButton.backgroundColor = UIColor.defaultAppColor()
                break
            }
        }
    }
    
    func handleStatusBtnTap(sender: UIButton) {
        let index = sender.tag
        let currentDriver = filterDrivers[index]
        let oldStatus = currentDriver.status
        if (self.isLoading == false) {
            self.handleGetDriverInfo(index, driver: currentDriver.account, oldStatus: oldStatus!)
        }
    }
    
    //update status driver
    func handleGetDriverInfo(index: Int, driver: String, oldStatus: Int) {
        self.isLoading = true
        self.showLoading()
        firstly {
            return CoordinatorManager.getDriverInfo(driver)
            }.then { loginResponse -> Promise<AnyObject> in
                if let response = loginResponse as? LoginResponse <Driver> {
                    if let currentDriver = (response.data[0].user) as? Driver {
                        let currentStatus = currentDriver.status
                        if currentStatus == oldStatus {
                            switch currentStatus! {
                            case DriverStatus.Idle.rawValue:
                                return CoordinatorManager.updateStatus(.Ready, driverAccount: currentDriver.account)
                            case DriverStatus.Ready.rawValue:
                                return CoordinatorManager.updateStatus(.Idle, driverAccount: currentDriver.account)
                            case DriverStatus.Driving.rawValue:
                                return CoordinatorManager.endTrip(currentDriver.account)
                            default:
                                break
                            }
                        }
                        else {
                            self.isLoading = false
                            self.filterDrivers[index] = currentDriver
                            self.collectionView.reloadItemsAtIndexPaths([NSIndexPath.init(forRow: index, inSection: 0)])
                        }
                    }
                    
                }
                return Promise{ fulfill, reject in
                    fulfill("OK")
                }
            }.then { requestResponse -> Void in
                self.stopLoadingWithoutAnimation()
                self.isLoading = false
                
                //update status response
                if let response = requestResponse as? LoginResponse <Driver> {
                    let currentDriver = response.data[0].user ?? Driver()
                    self.filterDrivers[index] = (currentDriver as? Driver)!
                    self.collectionView.reloadItemsAtIndexPaths([NSIndexPath.init(forRow: index, inSection: 0)])
                }
                
                //end trip response
                if requestResponse is SingleTripResponse {
                    self.handleGetDrivers()
                }
            }.always {

            }.error { error -> Void in
                self.stopLoadingWithoutAnimation()
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.isLoading = false
        }
    }
    
    //UITextField did change
    func textFieldDidChangeHandler() {
        let pattern = searchingTextField.text
        filterDrivers.removeAll()
        for driver in drivers {
            if driver.code_number.containsString(pattern!) == true || pattern == "" {
                filterDrivers.append(driver)
            }
        }
        self.collectionView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
