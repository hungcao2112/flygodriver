//
//  MenuController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit

class MenuController: UITableViewController {

    let menuTitle = [NSLocalizedString("Booking", comment: "Booking"),NSLocalizedString("Guide", comment: "Guide"),NSLocalizedString("History", comment: "History"),NSLocalizedString("Support", comment: "Support"),NSLocalizedString("Promotion", comment: "Promotion"),NSLocalizedString("Logout", comment: "Logout")]
    let menuImage = ["menu_datxe","menu_huongdan","menu_lichsu","menu_hotro","menu_khuyenmai","menu_logout"]

    override func viewDidLoad() {
        super.viewDidLoad()
        let backgroundView = UIView(frame: self.tableView.frame)
        let backgroundView1 = UIView(frame: CGRectMake(self.tableView.frame.minX,self.tableView.frame.minY, self.tableView.frame.width, self.tableView.frame.height/2))
        backgroundView1.addDefaultGradientLayer()
        backgroundView.addSubview(backgroundView1)
        let backgroundView2 = UIView(frame: CGRectMake(self.tableView.frame.minX,self.tableView.frame.maxY/2, self.tableView.frame.width, self.tableView.frame.height/2))
        backgroundView2.backgroundColor = UIColor.whiteColor()
        backgroundView.addSubview(backgroundView2)
        self.tableView.backgroundView = backgroundView
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (menuTitle.count + 1)
    }
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAtIndexPath: indexPath)
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        // Configure the cell...
        cell.selectionStyle = UITableViewCellSelectionStyle.Default
        cell.backgroundColor = UIColor.whiteColor()
        let rowNumber = indexPath.row
        if rowNumber == 0 {
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.backgroundColor = UIColor.clearColor()
            let avatarImageView = cell.viewWithTag(100) as! UIImageView
            let nameLabel = cell.viewWithTag(101) as! UILabel
            let loginUser = GlobalAppData.sharedObject.getLoginUser()
            nameLabel.text = loginUser.lastName
            avatarImageView.roundImage()
            if let avatar = loginUser.avatar as String? {
                let url = NSURL(string: avatar ?? "")!
                avatarImageView.af_setImageWithURL(
                    url,
                    placeholderImage: UIImage(named: "user"),
                    filter: nil,
                    imageTransition: .None,
                    completion: { (response) -> Void in
                })
            }
            let containView = cell.viewWithTag(104)!
            if let myConstraint = containView.constraints.filter( { c in return c.identifier == "centerHorizontalID" }).first {
                myConstraint.constant = -40.0
            }
            
//            avatarImageView.frame = CGRectMake(containView.frame.width/2 - avatarImageView.frame.width/2,avatarImageView.frame.minY,avatarImageView.frame.width,avatarImageView.frame.height)
        }
        else {
            let imageView = cell.viewWithTag(100) as! UIImageView
            let label = cell.viewWithTag(101) as! UILabel
            let origImage = UIImage.init(named: menuImage[rowNumber - 1])
            imageView.image = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            imageView.tintColor = UIColor.darkGrayColor()
            label.text = menuTitle[rowNumber - 1]
            label.textColor = UIColor.darkGrayColor()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let revealViewController = self.revealViewController
        revealViewController().revealToggle(nil)
        
        let rowNumber = indexPath.row
        var userInfoValue = Constants.UI.SideMenuBar.SideBarNotifBooking
        switch rowNumber {
        case 0:
            userInfoValue = Constants.UI.SideMenuBar.SideBarNotifProfile
            break
        case 1:
            userInfoValue = Constants.UI.SideMenuBar.SideBarNotifBooking
            break
        case 2:
            userInfoValue = Constants.UI.SideMenuBar.SideBarNotifGuide
            break
        case 3:
            userInfoValue = Constants.UI.SideMenuBar.SideBarNotifHistory
            break
        case 4:
            userInfoValue = Constants.UI.SideMenuBar.SideBarNotifSupport
            break
        case 5:
            userInfoValue = Constants.UI.SideMenuBar.SideBarNotifPromotion
            break
        case 6:
            userInfoValue = Constants.UI.SideMenuBar.SideBarNotifLogOut
            break
        default:
            break
        }
        let userInfo = NSMutableDictionary()
        userInfo.setValue(userInfoValue, forKey: "key")
        let nc = NSNotificationCenter.defaultCenter()
        nc.postNotificationName(Constants.UI.SideMenuBar.SideBarNotifName, object: self, userInfo: userInfo as [NSObject : AnyObject])
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let row = indexPath.row
        if (row > 0) {
            return Constants.UI.SideMenuBar.kSideBarHeightCellNormal
        }
        return Constants.UI.SideMenuBar.kSideBarHeightCellProfile
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
