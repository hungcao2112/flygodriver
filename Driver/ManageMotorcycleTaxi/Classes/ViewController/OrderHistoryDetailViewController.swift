//
//  OrderHistoryDetailViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/28/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit

protocol OrderHistoryDetailViewDelegate{
    func didCacelTrip(isSuccess: Bool)
}

class OrderHistoryDetailViewController: BaseCollectionViewController {
    var cellIdentifiers = [String]()
    var tripHistory = Trip()
    var transport: Transport! = Transport()
    var delegate: OrderHistoryDetailViewDelegate?
    var currentTarget = Target.Customer
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("DETAILED JOURNEY", comment: "DETAILED JOURNEY")
        transport = GlobalAppData.sharedObject.getTransportByVehicleID(tripHistory.vehicleId)
        if transport != nil {
            self.setUpCollectionView()
            self.setUpDataSource()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.checkPrepaidUnpaidTrip()
    }
    
    func checkPrepaidUnpaidTrip() {
        if (self.isPrepaidUnpaidTrip() == true && transport != nil) {
            self.showAlertWindow(NSLocalizedString("Payment required", comment: ""), message: NSLocalizedString("You must make a payment to booking becomes effective. Pay it now?", comment: ""), okAction: { () -> Void in
                self.handlePayment()
                }, cancelAction: { () -> Void in})
        }
    }
    
    func isPrepaidUnpaidTrip() -> Bool {
        if GlobalAppData.sharedObject.currentTarget == Target.Customer {
            if tripHistory.paymentMethod == PaymentMethod.PrePaid {
                if tripHistory.invoiceStatus == InvoiceStatus.PRE_WAITING {
                    return true
                }
            }
        }
        return false
    }
    
    func handlePayment() {
//        let controler = self.navigator.navigateToCustomerBankSelectionView()
//        controler.trip = self.tripHistory
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpDataSource() {
        cellIdentifiers.append(CBCCompleteOrderCodeCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        cellIdentifiers.append(CBCCompletePriceCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
/*
        if tripHistory.isMotobikeTrip() {
            cellIdentifiers.append(CBCMotobikeCell.cellIdentifier)
        }
*/
        if transport.slot_number == Constants.Transport.motobikeSlotNumber {
            cellIdentifiers.append(CBCMotobikeCell.cellIdentifier)
        }
        else {
            cellIdentifiers.append(CBCCarSelectionCell.cellIdentifier)
            cellIdentifiers.append(CBCCarRoundTripCell.cellIdentifier)
        }
        cellIdentifiers.append(IntroCollectionCell.cellIdentifier)
        cellIdentifiers.append(ActionCollectionCell.cellIdentifier)
    }
    
    func setUpCollectionView(){
        self.collectionView.registerNib(UINib.init(nibName:"CBCCompleteOrderCodeCell", bundle: nil), forCellWithReuseIdentifier: CBCCompleteOrderCodeCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCommonCell", bundle: nil), forCellWithReuseIdentifier: CBCCommonCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCMotobikeCell", bundle: nil), forCellWithReuseIdentifier: CBCMotobikeCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCarSelectionCell", bundle: nil), forCellWithReuseIdentifier: CBCCarSelectionCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCarRoundTripCell", bundle: nil), forCellWithReuseIdentifier: CBCCarRoundTripCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCPromotionCell", bundle: nil), forCellWithReuseIdentifier: CBCPromotionCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCompletePriceCell", bundle: nil), forCellWithReuseIdentifier: CBCCompletePriceCell.cellIdentifier)
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
    }
    
    // MARK: CollectionView delegate
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellIdentifiers.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if (indexPath.row == 0) {
            return CGSizeMake(self.view.width() - 20, Constants.UI.CollectionCellHeight.OrderCodeCellHeight)
        }
        
        let rowNumber = indexPath.row
        let cellIdentifier = cellIdentifiers[rowNumber]
        
        if cellIdentifier == IntroCollectionCell.cellIdentifier{
            return CGSizeMake(self.view.width() - 20, 20)
        }
        
        return CGSizeMake(self.view.width() - 20, Constants.UI.CollectionCellHeight.NormalCellHeight)
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let rowNumber = indexPath.row
        let cellIdentifier = cellIdentifiers[rowNumber]
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        if let inputCell = cell as? CBCCommonCell{
            var imageName = ""
            switch rowNumber {
            case 1:     //origin
                inputCell.titleLabel.text = tripHistory.startPoint
                imageName = "icon_diemdi"
                break;
            case 2:     //destination
                inputCell.titleLabel.text = tripHistory.endPointExpected
                imageName = "icon_diemden"
                break;
            case 4:     //time
                inputCell.titleLabel.text = tripHistory.startDate.toDayTimeString()
                imageName = "icon_calendar"
                break;
            default:
                break;
            }
            let imageView = inputCell.imageView
            let image = UIImage.init(named: imageName)
            let imageSizeAspect = CGFloat(image!.size.width/(image!.size.height))
            imageView.image = image
            
            for constraint in imageView.constraints {
                if (constraint.identifier == CBCCommonCell.imageConstraintID) {
                    constraint.setMultiplier(imageSizeAspect)
                }
            }
            
            if indexPath.row == 1{
                inputCell.hideBoardEdge(BorderEdge.Bottom)
                inputCell.roundCornersForCell([.TopLeft, .TopRight], radius: 5)
            }
            else{
                inputCell.hideBoardEdge(BorderEdge.Bottom)
            }
            
            return inputCell
        }
            
        else if let inputCell = cell as? CBCCompleteOrderCodeCell{
            inputCell.orderCodeLbl.text = tripHistory.codeNumber
            return inputCell
        }
            
        else if let inputCell = cell as? CBCCompletePriceCell{
            inputCell.imageView.image = UIImage.init(named: "icon_money")
            inputCell.priceLbl.text = tripHistory.getPriceString(tripHistory.priceExpected)
            inputCell.paymentStatusLbl.text = InvoiceStatus.getInvoceStatusName(tripHistory.invoiceStatus)
            inputCell.fixContainerView()
            inputCell.hideBoardEdge(BorderEdge.Bottom)
            return inputCell
        }
            
        else if let inputCell = cell as? CBCMotobikeCell{
            inputCell.distanceLbl.text = tripHistory.getKilometerExpectedString()
            inputCell.durationLbl.text = tripHistory.getDurationString(tripHistory.durationExpected)
            inputCell.bikeNumberBtn.setTitle(String(tripHistory.numberOfBike) + NSLocalizedString("bike", comment: "bike"), forState: .Normal)
            inputCell.fixContainerView()
            inputCell.roundCornersForBottomCell()
            return inputCell
        }
        else if let inputCell = cell as? CBCCarSelectionCell{
            inputCell.iconImageView.af_setImageWithURL(NSURL.init(string: transport.icon)!)
            inputCell.brandLbl.text = transport.brand
            inputCell.indicatorImgV.hidden = true
            inputCell.fixContainerView()
            inputCell.hideBoardEdge(BorderEdge.Bottom)
            return inputCell
        }
        else if let inputCell = cell as? CBCCarRoundTripCell{
            inputCell.distanceLbl.text = tripHistory.getKilometerExpectedString()
            inputCell.durationLbl.text = String(tripHistory.durationExpected) + NSLocalizedString("mins", comment: "mins")
            switch tripHistory.isRoundTrip {
            case 0:
                inputCell.roundTripBtn.setImage(UIImage.init(named: "icon_uncheck"), forState: UIControlState.Normal)
                break
            default:
                inputCell.roundTripBtn.setImage(UIImage.init(named: "icon_check"), forState: UIControlState.Normal)
                break
            }
            inputCell.fixContainerView()
            inputCell.roundCornersForBottomCell()
            
            return inputCell
        }
        else if let inputCell = cell as? IntroCollectionCell{
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            if (self.isPrepaidUnpaidTrip() == true) {
                inputCell.actionButonDatas = [ActionButtonData(title: NSLocalizedString("PAYMENT", comment: "PAYMENT"), backgroundColor: UIColor.whiteColor(), titleColor: UIColor.redColor(), borderWidth: 2, borderColor: UIColor.defaultButtonBorderColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                        self.handlePayment()
                })
                ]
            }
            else {
                inputCell.actionButonDatas = [ActionButtonData(title: NSLocalizedString("CANCEL TRIP", comment: "CANCEL TRIP"), backgroundColor: UIColor.whiteColor(), titleColor: UIColor.redColor(), borderWidth: 2, borderColor: UIColor.defaultButtonBorderColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                    self.showAlertWindow(NSLocalizedString("Do you really want to cancel this trip?", comment: "Do you really want to cancel this trip?"), okAction: { () -> Void in
                        self.cancelTrip()
                    })
                })
                ]
            }

        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    //MARK: - Actions
    
    func cancelTrip(){
        self.showLoading()
        firstly {
            return CustomerManager.cancelTrip(invoiceCodeNumber: self.tripHistory.codeNumber)
            }.then { response -> Void in
                self.showMessage(NSLocalizedString("Cancel trip sucessfully", comment: "Cancel trip sucessfully"))
                self.stopLoadingWithoutAnimation()
                self.popViewController()
            }.always {
                self.stopLoadingWithoutAnimation()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.showMessage(NSLocalizedString("Cancel trip failed", comment: "Cancel trip failed"))
        }
    }
    
    func popViewController() {
        self.navigationController?.popViewControllerAnimated(true)
        self.delegate?.didCacelTrip(true)
    }
}
