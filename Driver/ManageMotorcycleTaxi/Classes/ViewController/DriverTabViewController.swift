//
//  DriverTabViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/13/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

protocol DriverTabViewDelegate {
    func didSelectItem(tabBar: UITabBar, didSelectItem item: UITabBarItem)
}

class DriverTabViewController : UITabBarController {
    var navigator: Navigator?
    var driverTabViewDelegate : DriverTabViewDelegate?
    var driverDelegate: DriverChangeTitleDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = UIColor.defaultAppColor()
        for subViewController in self.viewControllers ?? []{
            if let baseViewController = subViewController as? BaseViewController{
                baseViewController.navigator = self.navigator
            }
            
            if let viewController = subViewController as? DriverHomeViewController{
                viewController.delegate = driverDelegate
            }
        }
        
        self.title = self.tabBar.selectedItem?.title
    }
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        driverTabViewDelegate?.didSelectItem(tabBar, didSelectItem: item)
    }

}
