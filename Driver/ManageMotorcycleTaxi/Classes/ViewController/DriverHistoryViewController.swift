//
//  DriverHistoryViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/5/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper
import PromiseKit
import UIKit
import XCGLogger

class DriverHistoryViewController: HistoryCollectionViewController {
    let log = XCGLogger.defaultInstance()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.collectionView.frame = self.view.frame
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 40.0, 0)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopLoadingWithoutAnimation()
    }
    
    override func getTripHistories(page: String, limit: String, order_by: String, order_mode: String) {
        if(Int(page) == 0 || isLoading == true) {
            return
        }
        
        if (self.isLoading == false) {
            self.isLoading = true
            self.showLoading()
            
            firstly {
                return DriverManager.getHistoryTrips(page, limit: limit, order_by: order_by, order_mode: order_mode)
                }.then { [weak self] tripHistoryResponse -> Void in
                    if let strongSelf = self{
                        strongSelf.isLoading = false
                        strongSelf.stopLoadingWithoutAnimation()
                        if let tripHistoryResponse = tripHistoryResponse as? TripHistoryResponse{
                            
                            strongSelf.nextHistoryPage = Int(page)! + 1
                            
                            if (Int(page) == 0) {
                                strongSelf.tripHistories = tripHistoryResponse.data ?? []
                            }
                            else {
                                strongSelf.tripHistories.appendContentsOf(tripHistoryResponse.data ?? [])
                            }
                            if strongSelf.tripHistories.count == 0 {
                                self?.showMessageAtCenter(NSLocalizedString("Empty history.", comment: "Empty history."))
                            }
                            strongSelf.tripHistories = strongSelf.tripHistories.sort{$0.date_pickup > $1.date_pickup}
                            strongSelf.collectionView.reloadData()
                        }
                    }
                }.always {
                    self.isLoading = false
                    self.stopLoadingWithoutAnimation()
                }.error { error -> Void in
                    if let errorType = error as NSError?{
                        self.handleError(errorType)
                    }
                    self.isLoading = false
                    self.stopLoadingWithoutAnimation()
                    self.stopLoadingWithoutAnimation()
            }
        }
    }

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let tripHistory = tripHistories[indexPath.row]
        //self.navigator.navigateToHistoryDetailView(tripHistory)
        let tripStatus = tripHistory.status
        switch tripStatus {
        case "1":
            self.navigateToHistoryDetailView(tripHistory)
            break
        case "2":
            self.navigateToCustomerTripView()
            break
        case "3":
            self.navigateToHistoryDetailView(tripHistory)
            break
        case "4":
            break
        default:
            break
        }
    }
    
    func navigateToCustomerTripView() {
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerTripViewID)
            as! CustomerTripViewController
//        let navigationController = UINavigationController(rootViewController: controller)
//        navigationController.navigationBar.format()
        if let nv = self.navigationController{
            nv.pushViewController(controller, animated: true)
        }
    }
    
    func navigateToHistoryDetailView(tripHistory: TripHistoryResponseData) {
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.HistoryDetailViewID)
            as! HistoryDetailViewController
//        let navigationController = UINavigationController(rootViewController: controller)
//        navigationController.navigationBar.format()
        controller.navigator = self.navigator
        controller.tripHistory = tripHistory
        if let nv = self.navigationController{
            nv.pushViewController(controller, animated: true)
        }
    }

}
