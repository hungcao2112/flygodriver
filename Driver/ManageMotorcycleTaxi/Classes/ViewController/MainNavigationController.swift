//
//  MainNavigationController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    override func viewDidLoad() {
        let navigator = Navigator(storyboard: Storyboards.main, navigationController: self)
        NavigationManager.sharedObject.navigator = navigator
        let targetName = NSBundle.mainBundle().infoDictionary?["TargetName"] as! String
        if targetName == "AVIGO"{
            GlobalAppData.sharedObject.currentTarget = .Customer
        }
        else if targetName == "FlyGo Partner"{
            GlobalAppData.sharedObject.currentTarget = .Driver
        }
        navigator.navigateToAutoLoginView()
    }
}
