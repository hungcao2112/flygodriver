//
//  CBCCompletePriceCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/27/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class CBCCompletePriceCell: BaseCollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLbl: MarqueeLabel!
    @IBOutlet weak var paymentStatusLbl: MarqueeLabel!
    
    static let cellIdentifier = "CBCCompletePriceCellID"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        paymentStatusLbl.fadeLength = 10.0
        paymentStatusLbl.textColor = UIColor.defaultAppColor()
    }

}
