//
//  CustomerBookingCompleteViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/24/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit
import PromiseKit

class CustomerBookingCompleteViewController: BaseCollectionViewController {

    var cellIdentifiers = [String]()
    var trip: Trip!
    var transport: Transport! = Transport()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("BOOKING DETAILS", comment: "BOOKING CONFIRM")
        transport = GlobalAppData.sharedObject.getTransportByVehicleID(trip.vehicleId)
        if transport != nil {
            self.setUpCollectionView()
            self.setUpDataSource()
        }
        self.hideBackBarBtn(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpDataSource() {
        cellIdentifiers.append(CBCCompleteOrderCodeCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        cellIdentifiers.append(CBCCompletePriceCell.cellIdentifier)
        cellIdentifiers.append(CBCCommonCell.cellIdentifier)
        
        if transport.slot_number == Constants.Transport.motobikeSlotNumber {
            cellIdentifiers.append(CBCMotobikeCell.cellIdentifier)
        }
        else {
            cellIdentifiers.append(CBCCarSelectionCell.cellIdentifier)
            cellIdentifiers.append(CBCCarRoundTripCell.cellIdentifier)
        }
        
        cellIdentifiers.append(IntroCollectionCell.cellIdentifier)
        cellIdentifiers.append(ActionCollectionCell.cellIdentifier)
    }
    
    func setUpCollectionView(){
        self.collectionView.registerNib(UINib.init(nibName:"CBCCompleteOrderCodeCell", bundle: nil), forCellWithReuseIdentifier: CBCCompleteOrderCodeCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCommonCell", bundle: nil), forCellWithReuseIdentifier: CBCCommonCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCMotobikeCell", bundle: nil), forCellWithReuseIdentifier: CBCMotobikeCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCarSelectionCell", bundle: nil), forCellWithReuseIdentifier: CBCCarSelectionCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCarRoundTripCell", bundle: nil), forCellWithReuseIdentifier: CBCCarRoundTripCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCPromotionCell", bundle: nil), forCellWithReuseIdentifier: CBCPromotionCell.cellIdentifier)
        self.collectionView.registerNib(UINib.init(nibName:"CBCCompletePriceCell", bundle: nil), forCellWithReuseIdentifier: CBCCompletePriceCell.cellIdentifier)

        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
    }
    
    // MARK: CollectionView delegate
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellIdentifiers.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if (indexPath.row == 0) {
            return CGSizeMake(self.view.width() - 20, Constants.UI.CollectionCellHeight.OrderCodeCellHeight)
        }
        return CGSizeMake(self.view.width() - 20, Constants.UI.CollectionCellHeight.NormalCellHeight)
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let rowNumber = indexPath.row
        let cellIdentifier = cellIdentifiers[rowNumber]
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        if let inputCell = cell as? BaseCollectionViewCell {
            inputCell.fixContainerView()
        }
        if let inputCell = cell as? CBCCommonCell{
            var imageName = ""
            switch rowNumber {
            case 1:     //origin
                inputCell.titleLabel.text = trip.originPlace.name
                imageName = "icon_diemdi"
                break;
            case 2:     //destination
                inputCell.titleLabel.text = trip.destinationPlace.name
                imageName = "icon_diemden"
                break;
            case 4:     //time
                inputCell.titleLabel.text = trip.startDate.toDayTimeString()
                imageName = "icon_calendar"
                break;
            default:
                break;
            }
            let imageView = inputCell.imageView
            let image = UIImage.init(named: imageName)
            let imageSizeAspect = CGFloat(image!.size.width/(image!.size.height))
            imageView.image = image
            for constraint in imageView.constraints {
                if (constraint.identifier == CBCCommonCell.imageConstraintID) {
                    constraint.setMultiplier(imageSizeAspect)
                }
            }
            
            inputCell.hideBoardEdge(BorderEdge.Bottom)
            if indexPath.row == 1{
                inputCell.roundCornersForCell([.TopLeft, .TopRight], radius: 5)
            }
            
            return inputCell
        }
        
        else if let inputCell = cell as? CBCCompleteOrderCodeCell{
            inputCell.orderCodeLbl.text = trip.codeNumber
            return inputCell
        }
            
        else if let inputCell = cell as? CBCCompletePriceCell{
            inputCell.imageView.image = UIImage.init(named: "icon_money")
            inputCell.hideBoardEdge(BorderEdge.Bottom)
            inputCell.priceLbl.text = trip.getPriceExpectedString()
            inputCell.paymentStatusLbl.text = InvoiceStatus.getInvoceStatusName(trip.invoiceStatus)
            return inputCell
        }
            
        else if let inputCell = cell as? CBCMotobikeCell{
            inputCell.distanceLbl.text = trip.getKilometerExpectedString()
            inputCell.durationLbl.text = trip.getDurationExpectedString()
            inputCell.bikeNumberBtn.setTitle(String(trip.numberOfBike) + " " + NSLocalizedString("bike", comment: "bike"), forState: .Normal)
            inputCell.roundCornersForBottomCell()

            return inputCell
        }
        else if let inputCell = cell as? CBCCarSelectionCell{
            if let transport = self.transport as Transport! {
                inputCell.iconImageView.af_setImageWithURL(NSURL.init(string: transport.icon)!)
                inputCell.brandLbl.text = transport.brand
                inputCell.indicatorImgV.hidden = true
                inputCell.hideBoardEdge(BorderEdge.Bottom)
            }
            return inputCell
        }
        else if let inputCell = cell as? CBCCarRoundTripCell{
            inputCell.distanceLbl.text = trip.getKilometerExpectedString()
            inputCell.durationLbl.text = trip.getDurationExpectedString()
            inputCell.roundCornersForBottomCell()
            
            switch trip.isRoundTrip {
            case 0:
                inputCell.roundTripBtn.setImage(UIImage.init(named: "icon_uncheck"), forState: UIControlState.Normal)
                break
            default:
                inputCell.roundTripBtn.setImage(UIImage.init(named: "icon_check"), forState: UIControlState.Normal)
                break
            }
            return inputCell
        }
        else if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = NSLocalizedString("Thank you and have a nice trip.", comment: "Thank you and have a nice trip.")
            
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            
            inputCell.actionButonDatas = [ActionButtonData(title: NSLocalizedString("COMPLETE", comment: "COMPLETE"), backgroundColor: UIColor.defaulButtonColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                self.handleComplete()
            })
            ]
            
            return inputCell
        }
        return cell
    }
    
    private func handleComplete() {
        GlobalAppData.sharedObject.setPendingOrderTrip(nil)
        self.navigator.navigateToCustomerRevealView()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
}
