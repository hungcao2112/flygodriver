//
//  UpdatePasswordViewController.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/18/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import UIKit
import Alamofire

enum UpdatePasswordMode: Int{
    
    case Unknown = 0,
    Forgot,
    Update
}

class UpdatePasswordViewController: BaseCollectionViewController {
    
    let StartInputRow = 1
    var NumberOfInputRow = 2
    
    enum InputRow: Int{
        case Password = 1,
        NewPassword,
        ReenterPassword
    }
    
    var codeData: CodeResponseData?
    
    var passwordTextField: UITextField?
    var newPasswordTextField: UITextField?
    var reenterPasswordTextField: UITextField?

    private var settingDatas = [SettingData]()
    
    var viewMode = UpdatePasswordMode.Unknown
    var name = ""
    var phone = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpDataSource()
        
        self.setUpCollectionView()
        
        self.view.backgroundColor = UIColor.defaultBackgroundColor()
    }
    
    func setUpCollectionView(){
        self.collectionView.registerClass(IntroCollectionCell.self, forCellWithReuseIdentifier: IntroCollectionCell.cellIdentifier)
        self.collectionView.registerClass(InputCollectionViewCell.self, forCellWithReuseIdentifier: InputCollectionViewCell.cellIdentifier)
        self.collectionView.registerClass(ActionCollectionCell.self, forCellWithReuseIdentifier: ActionCollectionCell.cellIdentifier)
    }
    
    func setUpDataSource(){
        settingDatas.append(SettingData(title: NSLocalizedString("Please enter new password", comment: "Please enter new password"), identifier: IntroCollectionCell.cellIdentifier))
        
        switch viewMode {
        case .Forgot:
            settingDatas.append(SettingData(title: NSLocalizedString("Password", comment: "Password"), placeHolder: NSLocalizedString("At least 6 characters", comment: "At least 6 characters"), identifier: InputCollectionViewCell.cellIdentifier))
            settingDatas.append(SettingData(title: NSLocalizedString("Confirm", comment: "Confirm"), placeHolder: NSLocalizedString("Enter password again", comment: "Enter password again"), identifier: InputCollectionViewCell.cellIdentifier))
            NumberOfInputRow = 2
        case .Update:
            settingDatas.append(SettingData(title: NSLocalizedString("Old Password", comment: "Old Password"), placeHolder: NSLocalizedString("Old Password", comment: "Old Password"), identifier: InputCollectionViewCell.cellIdentifier))
            settingDatas.append(SettingData(title: NSLocalizedString("New Password", comment: "New Password"), placeHolder: NSLocalizedString("New Password", comment: "New Password"), identifier: InputCollectionViewCell.cellIdentifier))
            settingDatas.append(SettingData(title: NSLocalizedString("Confirm", comment: "Confirm"), placeHolder: NSLocalizedString("Re-enter new password", comment: "Re-enter new password"), identifier: InputCollectionViewCell.cellIdentifier))
            NumberOfInputRow = 3
        default:
            break
        }
        
        
        settingDatas.append(SettingData(title: NSLocalizedString("CONFIRM", comment: "CONFIRM"), identifier: ActionCollectionCell.cellIdentifier, height: ActionCollectionCell.defaultHeight + 40))
    }
    
    //MARK: - CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom:0.0, right: 10.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let settingData = settingDatas[indexPath.row]
        
        return CGSizeMake(self.view.width() - 20, settingData.height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settingDatas.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let settingData = settingDatas[indexPath.row]
        
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(settingData.identifier, forIndexPath: indexPath)
        
        if let inputCell = cell as? IntroCollectionCell{
            inputCell.introLabel.text = settingData.title
            inputCell.introLabel.textAlignment = NSTextAlignment.Left
            return inputCell
        }
        else if let inputCell = cell as? InputCollectionViewCell{
            if indexPath.row == StartInputRow{
                inputCell.hideBoardEdge(BorderEdge.Bottom)
                inputCell.roundCornersForCell([.TopLeft, .TopRight], radius: 5)
            }else if indexPath.row == NumberOfInputRow{
                inputCell.roundCornersForCell([.BottomLeft, .BottomRight], radius: 5)
            }
            else{
                inputCell.hideBoardEdge(BorderEdge.Bottom)
            }
            
            inputCell.titleLabel.text = settingData.title
            inputCell.inputTextField.placeholder = settingData.placeHolder
            inputCell.inputTextField.secureTextEntry = true
            inputCell.inputTextField.delegate = self
            
            if let inputRow = InputRow(rawValue: indexPath.row){
                switch inputRow {
                case .Password:
                    passwordTextField = inputCell.inputTextField
                    break
                case .NewPassword:
                    newPasswordTextField = inputCell.inputTextField
                    break
                case .ReenterPassword:
                    reenterPasswordTextField = inputCell.inputTextField
                    break
                }
            }
            
            return inputCell
        }
        else if let inputCell = cell as? ActionCollectionCell{
            if settingData.title ==  NSLocalizedString("CONFIRM", comment: "CONFIRM"){
                inputCell.actionButonDatas = [
                    ActionButtonData(title: settingData.title, backgroundColor: UIColor.defaulButtonColor(), horizontalAlignment: UIControlContentHorizontalAlignment.Center, action: { () -> Void in
                        self.activeTextField?.resignFirstResponder()
                        switch self.viewMode{
                        case .Forgot:
                            self.updatePass(confirmCode: self.codeData?.code ?? "", account: self.codeData?.account ?? "", password: self.passwordTextField?.text ?? "")
                        case .Update:
                            self.updatePass(oldPassword: self.passwordTextField?.text ?? "", newPassword: self.newPasswordTextField?.text ?? "", reenterPassword: self.reenterPasswordTextField?.text ?? "")
                            break
                        default:
                            break
                        }
                        
                    })
                ]
            }
            
            return inputCell
        }
        
        return cell
    }
    
    //MARK: - Methods
    
    private func updatePass(confirmCode code: String, account: String, password: String){
        if password != newPasswordTextField?.text{
            self.showMessageAtCenter(NSLocalizedString("Password does not match the confirm password.", comment: "Password does not match the confirm password."))
            return
        }
        self.showLoading()
        firstly {
            return AuthManager.updatePass(code: code, account: account, password: password)
            }.then { _ -> Void in
                self.stopLoading()
                NavigationManager.sharedObject.navigateToLoginView()
            }.always {
                self.stopLoading()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.stopLoading()
        }
    }
    
    private func updatePass(oldPassword oldPassword: String, newPassword: String, reenterPassword: String){
        if (newPassword != reenterPassword) {
            self.showMessageAtCenter(NSLocalizedString("Confirm password does not match the new password.", comment: "Confirm password does not match the new password."))
            return
        }
        if newPassword.characters.count > 20 || newPassword.characters.count < 6{
            self.showMessageAtCenter(NSLocalizedString("Password should have at least 6 to 20 charaters", comment: "Password should have at least 6 to 20 charaters"))
            return
        }
        self.showLoading()
        firstly {
            return UserManager.updatePass(self.phone, fullname: self.name, address: "", country: "", city: "", title: "", birthday: "", new_password: newPassword)
            }.then { response -> Void in
                self.stopLoading()
                self.showMessageAtCenter(NSLocalizedString("Update password successfully", comment: "Update password successfully"))
                self.navigationController?.popViewControllerAnimated(true)
            }.always {
                self.stopLoading()
            }.error { error -> Void in
                if let errorType = error as NSError?{
                    self.handleError(errorType)
                }
                self.showMessageAtCenter(NSLocalizedString("Update password failed", comment: "Update password failed"))
                self.stopLoading()
        }
    }
}

