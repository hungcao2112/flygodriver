//
//  HistoryCollectionViewCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/23/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class HistoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var briefHistoryView: BriefHistoryView!
    static let cellIdentifier = "HistoryCollectionViewCellID"
    
    static let defaultHeight: CGFloat = 110
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let view = NSBundle.mainBundle().loadNibNamed("BriefHistoryView", owner: self, options: nil)![0] as? UIView
        
        if let _ = view as? BriefHistoryView{
            view?.frame = self.frame
            addSubview(view!)
            briefHistoryView = view as? BriefHistoryView
            briefHistoryView.titleLabel.text = NSLocalizedString("Order code", comment: "Order code")
        }
    }

}
