//
//  StoryBoards.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class Storyboards {
    
    static let instance = Storyboards()
    
    static var main : UIStoryboard {
        return instance.mainStoryboard
    }
    
    static var customer : UIStoryboard {
        return instance.customerStoryboard
    }
    
    static var customerBooking : UIStoryboard {
        return instance.customerBookingStoryboard
    }
    
    static var driver : UIStoryboard {
        return instance.driverStoryboard
    }
    
    private let mainStoryboard : UIStoryboard!
    private let customerStoryboard : UIStoryboard!
    private let driverStoryboard : UIStoryboard!
    private let customerBookingStoryboard : UIStoryboard!
    
    init() {
        mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        customerStoryboard = UIStoryboard(name: "Customer", bundle: NSBundle.mainBundle())
        driverStoryboard = UIStoryboard(name: "Driver", bundle: NSBundle.mainBundle())
        customerBookingStoryboard = UIStoryboard(name: "CustomerBooking", bundle: NSBundle.mainBundle())
    }
}