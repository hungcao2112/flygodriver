//
//  Navigator.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit
class Navigator {
    var storyboard: UIStoryboard!
    var navigationController: UINavigationController!
    var navigator: Navigator!
    
    init (storyboard: UIStoryboard, navigationController: UINavigationController) {
        self.storyboard = storyboard
        self.navigationController = navigationController
        self.navigationController.navigationBar.format()
        self.navigator = self;
    }
    
    func switchStoryboard(storyboard: UIStoryboard) {
        self.storyboard = storyboard
    }

    
    func getInstance() -> Navigator {
        return self.navigator
    }
    
    func navigateToAutoLoginView() {
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.AutoLoginViewID)
            as! AutoLoginViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func navigateToCustomerHomeView() {
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerHomeViewID)
            as! CustomerHomeViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func navigateToLoginView() {
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerLoginViewID)
            as! LoginViewController
        controller.navigator = self
        //navigationController.setViewControllers([controller], animated: true)
        navigationController.pushViewController(controller, animated: true)
    }
    
    func navigateToDriverView() {
        let controller = Storyboards.driver.instantiateViewControllerWithIdentifier(Constants.StoryBoard.Driver.DriverViewID)
            as! DriverViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func navigateToDriverManagerView() {
        let controller = Storyboards.driver.instantiateViewControllerWithIdentifier(Constants.StoryBoard.Driver.DriverManagerControllerID)
            as! DriverManagerViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func navigateToRegisterView(registerMode: RegisterMode = .Normal, facebookUser: User? = User()) -> RegisterViewController{
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.RegisterViewID)
            as! RegisterViewController
        controller.registerMode = registerMode
        controller.facebookUser = facebookUser ?? User()
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToForgotPasswordView() {
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.ForgotPasswordViewID) as! ForgotPasswordViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
    }

    func navigateToMobileVerificationView(codeData: CodeResponseData) -> UIViewController{
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.MobileVerificationViewID)
            as! MobileVerificationViewController
        controller.codeData = codeData
        controller.currentVerificationMode = .ForgotPass
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToMobileVerificationView(registerResponseData: RegisterResponseData) -> MobileVerificationViewController{
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.MobileVerificationViewID)
            as! MobileVerificationViewController
        controller.registerResponseData = registerResponseData
        controller.currentVerificationMode = .PhoneNumber
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToUpdatePasswordView(codeData: CodeResponseData = CodeResponseData()) -> UpdatePasswordViewController{
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.UpdatePasswordViewID)
            as! UpdatePasswordViewController
        controller.codeData = codeData
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToTripPathView() -> UIViewController{
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.TripPathViewID)
            as! TripPathViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToGuideCustomerTripView() {
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerGuideTripViewID)
            as! GuideCustomerTripViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func navigateToCustomerTripView() -> CustomerTripViewController{
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerTripViewID)
            as! CustomerTripViewController
        controller.navigator = self
        let newNavVC = UINavigationController(rootViewController: controller)
        //navigationController.pushViewController(controller, animated: true)
        newNavVC.navigationBar.format()
        navigationController.presentViewController(newNavVC, animated: true, completion: nil)
        return controller
    }
    
//    func navigateToCustomerBookingView() {
//        let controller = storyboard.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerBookingViewID)
//            as! CustomerBookingViewController
//        controller.navigator = self
//        navigationController.pushViewController(controller, animated: true)
//    }
    
    func navigateToCustomerRevealView() {
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerRevealViewID)
            as! CustomSWRevealViewController
        controller.navigator = self
//        navigationController.pushViewController(controller, animated: true)
//        let viewControllers:[UIViewController] = [controller]
        navigationController.presentViewController(controller, animated: true, completion: nil)
//        navigationController.setViewControllers(viewControllers, animated: true)
//        navigationController.setNavigationBarHidden(true, animated: false)
        
//        let controller = storyboard.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerBookingViewID)
//            as! CustomerBookingViewController
//        //        controller.navigator = self
//        //        navigationController.pushViewController(controller, animated: true)
//        let viewControllers:[UIViewController] = [controller]
//        navigationController.setViewControllers(viewControllers, animated: true)
        
    }
    
    func navigateToProfileView() -> UIViewController{
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.ProfileViewID)
            as! ProfileViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToHistoryView() -> UIViewController{
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.HistoryCollectionViewID)
            as! HistoryCollectionViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToHistoryDetailView(tripHistory: TripHistoryResponseData) -> UIViewController{
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.HistoryDetailViewID)
            as! HistoryDetailViewController
        controller.navigator = self
        controller.tripHistory = tripHistory
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
}
