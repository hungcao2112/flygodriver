//
//  DriverNavigator.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/8/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

extension Navigator{
    func presentDriverTripView() -> UIViewController{
        let controller = Storyboards.driver.instantiateViewControllerWithIdentifier(Constants.StoryBoard.Driver.DriverTripViewID)
            as! DriverTripViewController
        controller.navigator = self
        navigationController.presentViewController(controller, animated: true, completion: nil)
        return controller
    }
    
    func navigateToCustomerBankSelectionView() -> DriverTripViewController {
        return DriverTripViewController()
    }
}