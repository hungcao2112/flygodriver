//
//  CustomerNavigator.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/8/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

extension Navigator{
    func navigateToCustomerBookingPaymentViewController() -> CustomerBookingPaymentViewController {
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerBookingPaymentViewControllerID)
            as! CustomerBookingPaymentViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToCustomerBookingCompleteView() -> CustomerBookingCompleteViewController {
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerBookingCompleteViewControllerID)
            as! CustomerBookingCompleteViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToCustomerBookingConfirmView() -> CustomerBookingConfirmViewController {
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerBookingConfirmViewID)
            as! CustomerBookingConfirmViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToCustomerBankSelectionView() -> CustomerBankSelectionViewController {
        let controller = CustomerBankSelectionViewController()
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToCustomerPaymentProcessView() -> CustomerPaymentProcessViewController {
        let controller = CustomerPaymentProcessViewController()
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToAppGuideView() -> UIViewController{
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.AppGuideViewControllerID)
            as! AppGuideViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToSupportView() -> UIViewController{
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.SupportViewControllerID)
            as! SupportViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
    
    func navigateToPromotionView() -> UIViewController{
        let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.PromotionViewControllerID)
            as! PromotionViewController
        controller.navigator = self
        navigationController.pushViewController(controller, animated: true)
        return controller
    }
}