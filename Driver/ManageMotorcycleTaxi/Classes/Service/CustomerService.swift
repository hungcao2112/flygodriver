//
//  CustomerService.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/21/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import Alamofire

class CustomerService: BaseService{
    static let CUSTOMER_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.host : Constants.WebService.Path.host) + Constants.WebService.API.Customer.Path
    
    class func phoneVerifyCode(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.PhoneVerification , parameters: parameters, completion: completion)
        
    }
    
    class func socialPhoneVerifyCode(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.SocialPhoneVerification , parameters: parameters, completion: completion)
        
    }
    
    class func register(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.Register , parameters: parameters, completion: completion)
        
    }
    
    class func socialRegister(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.SocialRegister , parameters: parameters, completion: completion)
        
    }
    
    class func getTripHistories(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.GetHistory , parameters: parameters, headers: headers, completion: completion)
    }
    
    class func updateProfile(parameters : [String : AnyObject] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        let headers: [String: String] = ["auth" : Context.getAuthKey()]
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.UpdateProfile , parameters: parameters, headers: headers, completion: completion)
    }
    
    class func cancelTrip(parameters : [String : AnyObject],  headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.CancelTrip , parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getDriverInfo(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.GetDriverInfo , parameters: parameters, headers: headers, completion: completion)
        
    }
    
    class func getCurrentTrip(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return get(CUSTOMER_PATH + Constants.WebService.API.Customer.GetCurrentTrip , parameters: parameters, headers: headers, completion: completion)
        
    }

    class func socialLogin(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(CUSTOMER_PATH + Constants.WebService.API.Customer.SocialLogin , parameters: parameters, headers: headers, completion: completion)
        
    }
    
}
