//
//  PaymentService.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 8/12/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import Alamofire

class PaymentService :BaseService {
    static let HOST_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.TestingHost : Constants.WebService.Path.ProductionHost)
    
    class func payment(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(HOST_PATH + Constants.WebService.API.Payment.Payment, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func paymentConfirm(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(HOST_PATH + Constants.WebService.API.Payment.PaymentConfirm, parameters: parameters, headers: headers, completion: completion)
    }

}
