//
//  FacebookService.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 8/17/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKShareKit

class FacebookManager{
    var fbId: String?
    var fbToken: String?
    private var accessToken: FBSDKAccessToken?
    
    let facebookReadPermissions = ["public_profile", "email"]
    
    class var sharedInstance: FacebookManager {
        get {
            struct Singleton {
                static let instance = FacebookManager()
            }
            return Singleton.instance
        }
    }
    
    func loginToFacebook(success:(facebookId: String, facebookToken: String)->Void, onError:(FacebookError)->Void){
        FBSDKLoginManager().logInWithReadPermissions(self.facebookReadPermissions, fromViewController: nil) {
            (result, error) -> Void in
            if error != nil {
                FBSDKLoginManager().logOut()
                onError(FacebookError.ProcessFail)
            } else if result.isCancelled {
                FBSDKLoginManager().logOut()
                onError(FacebookError.LoginCanceled)
            } else {
                var allPermsGranted = true
                
                let grantedPermissions = Array(result.grantedPermissions).map( {"\($0)"} )
                for permission in self.facebookReadPermissions {
                    if !grantedPermissions.contains(permission) {
                        allPermsGranted = false
                        break
                    }
                }
                if allPermsGranted {
                    self.fbToken = result.token.tokenString
                    self.fbId = result.token.userID
                    self.accessToken = result.token
                    print(result)
                    success(facebookId: self.fbId!, facebookToken: self.fbToken!)
                } else {
                    onError(FacebookError.NoPermission)
                }
            }
        }
    }
    
    // get restore
    func restoreAccessToken() {
        self.accessToken = FBSDKAccessToken.currentAccessToken()
    }
    
    // logout
    func logout() {
        FBSDKLoginManager().logOut()
        
        fbId = nil
        fbToken = nil
        accessToken = nil
    }
    
    // Share a link
    func shareLinkOnFacebook(view: UIViewController!, link: String!, title: String, description: String, content: String) {
        let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = NSURL(string: link)
        content.contentTitle = title
        content.contentDescription = description
        //content.imageURL = NSURL(string: self.contentURLImage)
        FBSDKShareDialog.showFromViewController(view, withContent: content, delegate: nil)
    }
    
    func sharePhotoOnFacebook(view: UIViewController!, image:UIImage!){
        let photo = FBSDKSharePhoto()
        photo.image = image
        photo.userGenerated = true;
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]
        FBSDKShareDialog.showFromViewController(view, withContent: content, delegate: nil)
    }
}