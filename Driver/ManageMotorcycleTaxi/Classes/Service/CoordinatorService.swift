//
//  CoordinatorService.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/21/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import Alamofire

class CoordinatorService: BaseService{
    static let COORDINATOR_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.host : Constants.WebService.Path.host) + Constants.WebService.API.Coordinator.Path
    
    class func endTrip(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(COORDINATOR_PATH + Constants.WebService.API.Coordinator.EndTrip, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func updateStatus(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(COORDINATOR_PATH + Constants.WebService.API.Coordinator.UpdateStatus, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getDrivers(parameters : [String : AnyObject] = ["vehicleId":1], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(COORDINATOR_PATH + Constants.WebService.API.Coordinator.GetDrivers, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getCurrentTrip(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(COORDINATOR_PATH + Constants.WebService.API.Coordinator.GetCurrentTrip, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getDriverInfo(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(COORDINATOR_PATH + Constants.WebService.API.Coordinator.GetDriverInfo, parameters: parameters, headers: headers, completion: completion)
    }
}
