//
//  BaseService.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class BaseService {
    class func post(url : URLStringConvertible, parameters : [String : AnyObject], headers: [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        print("POST: \(url); \(parameters); \(headers)")
        let request = RequestFactory.networkManager.request(Alamofire.Method.POST, url, parameters : parameters, encoding: .URL, headers: headers)
        request.responseJSON{response in completion(response)}
        return request
    }
    
    class func get(url : URLStringConvertible, parameters : [String : AnyObject], headers: [String : String], completion : Response<AnyObject, NSError> -> Void) -> Request{
        print("GET: \(url); \(parameters); \(headers)")
        let request = RequestFactory.networkManager.request(Alamofire.Method.GET, url
            , parameters: parameters, encoding: .URL, headers: headers)
        request.responseJSON{response in completion(response)}
        return request
    }
}