//
//  TripService.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/27/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import Alamofire

class TripService :BaseService {
    static let USER_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.host : Constants.WebService.Path.host) + Constants.WebService.API.Trip.Path
    
    class func bookTickets(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
//        return get(USER_PATH + Constants.WebService.API.Trip.BookTickets, parameters: parameters, headers: headers, completion: completion)
//        print(headers)
//        print(parameters)
        print(USER_PATH)
        print(headers)
        print(parameters)
        return post(USER_PATH + Constants.WebService.API.Trip.BookTickets, parameters: parameters, headers: headers, completion: completion)
    }

    class func updateMethodTickets(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(USER_PATH + Constants.WebService.API.Trip.UpdateMethodTickets, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func checkPromotionCode(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(USER_PATH + Constants.WebService.API.Trip.CheckPromotionCode, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getPromotions(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(USER_PATH + Constants.WebService.API.Trip.GetPromotions, parameters: parameters, headers: headers, completion: completion)
    }
}
