//
//  AuthService.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/5/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class AuthService : BaseService{
    static let AUTH_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.host : Constants.WebService.Path.host) + ""

    class func login(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + Constants.WebService.API.Login , parameters: parameters, completion: completion)
        
    }
    
    class func registerMobile(parameters : [String : AnyObject], headers: [String : String], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + Constants.WebService.API.RegisterMobile , parameters: parameters, headers: headers, completion: completion)
        
    }
    
    class func getCode(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + Constants.WebService.API.ForgetPasswordGetCode , parameters: parameters, completion: completion)
        
    }
    
    class func verifyCode(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + Constants.WebService.API.ForgetPasswordVerifyCode , parameters: parameters, completion: completion)
        
    }
    
    class func verifyToken(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + Constants.WebService.API.VerifiToken , parameters: parameters, completion: completion)
        
    }
    
    class func updatePassword(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + Constants.WebService.API.ForgetPasswordUpdatePass , parameters: parameters, completion: completion)
        
    }
    
    
}
