//
//  TransportService.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/27/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
//import PromiseKit
//import ObjectMapper
import Alamofire

class TransportService :BaseService {
    static let USER_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.host : Constants.WebService.Path.host) + Constants.WebService.API.Transport.Path
    
    class func getListItems(parameters : [String : AnyObject], headers : [String : String], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return get(USER_PATH + Constants.WebService.API.Transport.GetListItems , parameters: parameters, headers: headers, completion: completion)
    }
    
}
