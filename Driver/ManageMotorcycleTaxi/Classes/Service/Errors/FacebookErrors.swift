//
//  FacebookErrors.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 8/17/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//
import Foundation
enum FacebookError: ErrorType {
    case ProcessFail
    case LoginCanceled
    case NoPermission
}
