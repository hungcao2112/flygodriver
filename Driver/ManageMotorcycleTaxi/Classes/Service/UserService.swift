//
//  UserService.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import Alamofire

class UserService :BaseService {
    static let USER_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.host : Constants.WebService.Path.host) + Constants.WebService.API.User.Path
    
    class func getProfile(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return get(USER_PATH + Constants.WebService.API.User.GetProfile , parameters: parameters, headers: headers, completion: completion)
    }
    
    class func logout(parameters : [String : AnyObject],  headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(USER_PATH + Constants.WebService.API.User.Logout , parameters: parameters, headers: headers, completion: completion)
        
    }
    
    class func uploadAvatar(imageData: NSData?, success : Response<AnyObject, NSError> -> Void, fail : ErrorType -> Void){
        let url = USER_PATH + "/updateAvatar"
        let parameters = [String: AnyObject]()
        
        RequestFactory.networkManager.upload(.POST, url, headers: ["auth": Context.getAuthKey()], multipartFormData: { multipartFormData in
            let k: Int = 1;
            multipartFormData.appendBodyPart(data: imageData ?? NSData(), name: "file", fileName: "avatar\(k).jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                if let stringValue = value as? String {
                    multipartFormData.appendBodyPart(data: stringValue.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                } else {
                    multipartFormData.appendBodyPart(data: "\(value)".dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                }
            }
            }, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        if response.result.value != nil {
                        }
                        success(response)
                    }
                case .Failure(let encodingError):
                    fail(encodingError)
                }
        })
    }
    
//    class func uploadAvatar(imageData: NSData, completion : Response<AnyObject, NSError> -> Void) -> Request{
//        let url = USER_PATH + "/updateAvatar"
//        let headers: [String: String] = ["auth": Context.getAuthKey()]
//        let parameters: [String: AnyObject] = ["file": imageData]
//        return post(url, parameters: parameters, headers: headers, completion: completion)
//    }
    
    class func updatePassword(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(USER_PATH + Constants.WebService.API.User.UpdatePass , parameters: parameters, headers: headers, completion: completion)
        
    }
}
