//
//  DriverService.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import Alamofire

class DriverService: BaseService{
    //static let DRIVER_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.TestingHost : Constants.WebService.Path.ProductionHost) + Constants.WebService.API.Driver.Path
    
    static let DRIVER_PATH = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.host : Constants.WebService.Path.host) + Constants.WebService.API.Driver.Path
    
    static let DRIVER_PATH_ACCOUNT = (GlobalAppData.sharedObject.getIsTestingServer() ? Constants.WebService.Path.host : Constants.WebService.Path.host) + Constants.WebService.API.Driver.Account
    
    class func getAirport(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return get(DRIVER_PATH + Constants.WebService.API.Driver.GetAirport, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getDriverQueue(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return get(DRIVER_PATH + Constants.WebService.API.Driver.GetDriverQueue, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getAssignTrip(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.GetAssignTrip, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getCurrentTrip(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return get(DRIVER_PATH + Constants.WebService.API.Driver.GetCurrentTrip, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func updateStatus(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.UpdateStatus, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func acceptTrip(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.AcceptTrip, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func rejectTrip(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.RejectTrip, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func startDrive(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.StartDrive, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func endTrip(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.EndTrip, parameters: parameters, headers: headers, completion: completion)
    }
    
    class func getHistoryTrips(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.GetHistoryTrips, parameters: parameters, headers: headers, completion: completion)
    }
    class func register(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.Register, parameters: parameters, headers: headers, completion: completion)
    }
    class func active(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.Active, parameters: parameters, headers: headers, completion: completion)
    }
    class func getProfile(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return get(DRIVER_PATH + Constants.WebService.API.Driver.Profile, parameters: parameters, headers: headers, completion: completion)
    }
    class func updatePassword(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.updatePassword, parameters: parameters, headers: headers, completion: completion)
    }
    class func logout(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return get(DRIVER_PATH + Constants.WebService.API.Driver.logout, parameters: parameters, headers: headers, completion: completion)
    }
    class func getTripDetail(parameters : [String : AnyObject], headers : [String : String] = [:], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(DRIVER_PATH + Constants.WebService.API.Driver.getTripDetail, parameters: parameters, headers: headers, completion: completion)
    }
}
