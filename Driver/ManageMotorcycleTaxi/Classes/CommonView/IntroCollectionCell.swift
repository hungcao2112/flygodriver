//
//  TextColelctionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/7/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class IntroCollectionCell: UICollectionViewCell {
    
    static let cellIdentifier = "TextCollectionCellID"
    static let defaultHeight: CGFloat = 70
    
    private var containerView = UIView()
    
    var introLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        
        containerView.frame = CGRectMake(0, 0, self.width(), self.height())
        containerView.backgroundColor = UIColor.clearColor()
        addSubview(containerView)
        
        introLabel.frame = CGRectMake(Constants.UI.Margin.Left, 0, self.width() - Constants.UI.Margin.Left - Constants.UI.Margin.Right, self.height())
        introLabel.textColor = UIColor.blackColor()//UIColor.primaryTextColor()
        introLabel.numberOfLines = 0
        introLabel.textAlignment = NSTextAlignment.Center
        introLabel.text = NSLocalizedString("", comment: "")
        containerView.addSubview(introLabel)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func roundCornersForCell(corners: UIRectCorner, radius: CGFloat) {
        containerView.roundCorners(corners, radius: radius - 2)
        self.roundCorners(corners, radius: radius)
    }
}