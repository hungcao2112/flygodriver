//
//  CommentCollectionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/8/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class CommentCollectionCell: UICollectionViewCell {
    
    static let cellIdentifier = "CommentCollectionViewCellID"
    static let defaultHeight: CGFloat = 70
    
    private var containerView = UIView()
    
    var inputTextView = UITextView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.defaultBorderColor()
        
        containerView.frame = CGRectMake(2, 2, self.width() - 4, self.height() - 4)
        containerView.backgroundColor = UIColor.whiteColor()
        addSubview(containerView)
        
        inputTextView.frame = CGRectMake(0, 0, containerView.width(), containerView.height())
        inputTextView.textColor = UIColor.primaryTextColor()
        inputTextView.textAlignment = NSTextAlignment.Left
        inputTextView.text = NSLocalizedString("Comment text", comment: "Comment text")
        containerView.addSubview(inputTextView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func roundCornersForCell(corners: UIRectCorner, radius: CGFloat) {
        containerView.roundCorners(corners, radius: radius - 2)
        self.roundCorners(corners, radius: radius)
    }
}
