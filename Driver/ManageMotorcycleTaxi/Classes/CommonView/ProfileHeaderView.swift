//
//  ProfileHeaderView.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class ProfileHeaderView: UICollectionReusableView {
    
    static let cellIdentifier = "ProfileHeaderViewID"
    static let defaultHeight: CGFloat = 170
    
    private var containerView = UIView()
    
    var avatarImageView = UIImageView()
    var nameLabel = UILabel()
    
    var tapGesture: UITapGestureRecognizer?
    
    var tapHandler: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        //self.backgroundColor = UIColor(patternImage: UIImage(named: "profile-header-background")!)
        let labelHeight: CGFloat = 30
        let avatarWidth: CGFloat = self.width() > self.height() ? self.height()/2 : self.width()/2
        
        let marginTop = (self.height()/2 - Constants.UI.Margin.Top - labelHeight)/2
        containerView.frame = CGRectMake(0, 0, self.width(), self.height())
        containerView.backgroundColor = UIColor.clearColor()
        addSubview(containerView)
        
        avatarImageView.frame = CGRectMake(self.width()/2 -  avatarWidth/2, marginTop, avatarWidth, avatarWidth)
        avatarImageView.image = UIImage(named: "logo")
        avatarImageView.userInteractionEnabled = true
        avatarImageView.roundImage()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileHeaderView.viewDidTap))
        avatarImageView.addGestureRecognizer(tapGesture!)
        addSubview(avatarImageView)
        
        nameLabel.frame = CGRectMake(0, marginTop + avatarWidth + Constants.UI.Margin.Top, self.width(), labelHeight)
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.font = UIFont.systemFontOfSize(25)
        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = NSTextAlignment.Center
        nameLabel.text = NSLocalizedString("Name", comment: "Name")
        containerView.addSubview(nameLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func viewDidTap() {
        if let action = tapHandler{
            action()
        }
    }

}