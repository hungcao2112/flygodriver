//
//  MessageView.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

class MessageView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var messageLbl: UILabel!
    
    var title: String?{
        didSet{
            self.updateLayout()
        }
    }
    
    var message: String?{
        didSet{
            self.updateLayout()
        }
    }
    @IBAction func backAction(sender: AnyObject) {
        self.removeFromSuperview()
        if let action = self.backAction{
            action()
        }
    }
    
    @IBAction func confirmAction(sender: AnyObject) {
        if let action = self.confirmAction{
            action()
        }
    }
    
    var backAction: (() -> Void)?
    var confirmAction: (() -> Void)?
    init(frame: CGRect, title: String = "", message: String = "") {
        super.init(frame: frame)
        self.title = title
        self.message = message
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.containerView.layer.cornerRadius = Constants.UI.defaulCornerRadius
        self.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        self.containerView.frame = CGRectMake(self.containerView.frame.minX, self.containerView.frame.minY, self.containerView.frame.width, 70 + self.titleLbl.heightBaseText() +
            self.messageLbl.heightBaseText())
    }
    
    func updateLayout(){
        self.titleLbl.text = title
        self.titleLbl.frame = CGRectMake(self.titleLbl.frame.minX, self.titleLbl.frame.minY, self.titleLbl.frame.width, self.titleLbl.heightBaseText())
        
        self.messageLbl.text = message
        self.messageLbl.frame = CGRectMake(self.messageLbl.frame.minX, self.messageLbl.frame.minY, self.messageLbl.frame.width, self.messageLbl.heightBaseText())
        self.containerView.frame = CGRectMake(self.containerView.frame.minX, self.containerView.frame.minY, self.containerView.frame.width, 70 + self.titleLbl.heightBaseText() +
        self.messageLbl.heightBaseText())
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
