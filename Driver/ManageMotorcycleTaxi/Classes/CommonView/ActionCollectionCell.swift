//
//  ActionCellectionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/7/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

class ActionCollectionCell: UICollectionViewCell {
    
    static let cellIdentifier = "ActionCollectionCellID"
    static let defaultHeight: CGFloat = 50
    
    private var containerView = UIView()
    private var leftImageView: UIImageView?
    //var actionButton = UIButton()
    
    var actionHandler: ((button: UIButton) -> Void)?
    
    var actionButonDatas = [ActionButtonData](){
        didSet{
            self.addActionButtons()
        }
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        
        containerView.frame = CGRectMake(0, 0, self.width(),self.height())
        containerView.backgroundColor = UIColor.clearColor()
        addSubview(containerView)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showLeftImage(image: UIImage){
        let imageView = UIImageView(frame: CGRect(x: 2, y: 2, width: self.height() - 4, height: self.height() - 4))
        imageView.image = image
        leftImageView = imageView
        addSubview(imageView)
    }
    
    func addActionButtons(){
        let buttonMargin: CGFloat = 10
        
        let actionButonCount = actionButonDatas.count
        
        let buttonWidth = (self.width() - buttonMargin*(CGFloat(actionButonCount) - 1))/CGFloat(actionButonCount)
        
        var index = 0
        for actionButtonData in actionButonDatas{
            let actionButton = UIButton()
            actionButton.frame = CGRectMake(CGFloat(index)*(buttonWidth + buttonMargin), (containerView.height() - ActionCollectionCell.defaultHeight)/2, buttonWidth, ActionCollectionCell.defaultHeight)
            actionButton.setTitle(actionButtonData.title, forState: UIControlState.Normal)
            actionButton.contentHorizontalAlignment = actionButtonData.horizontalAlignment ?? UIControlContentHorizontalAlignment.Center
            actionButton.backgroundColor = actionButtonData.backgroundColor
            actionButton.setTitleColor(actionButtonData.titleColor, forState: UIControlState.Normal)
            actionButton.layer.cornerRadius = Constants.UI.defaulCornerRadius
            actionButton.layer.borderColor = actionButtonData.borderColor?.CGColor
            actionButton.layer.borderWidth = actionButtonData.borderWidth
            if let fontSize = actionButtonData.fontSize{
                actionButton.titleLabel!.font =  UIFont(name: (actionButton.titleLabel?.font?.fontName) ?? "", size: fontSize)
            }
            actionButton.addTarget(self, action: #selector(self.actionButtonTapped), forControlEvents: .TouchUpInside)
            
            actionButton.tag = index
            
            containerView.addSubview(actionButton)
            
            index = index + 1
        }
    }
    
    func roundCornersForCell(corners: UIRectCorner, radius: CGFloat) {
        containerView.roundCorners(corners, radius: radius - 2)
        self.roundCorners(corners, radius: radius)
    }
    
    func actionButtonTapped(sender: UIButton){
        let tag = sender.tag
        let actionButonData = actionButonDatas[tag]
        if let action = actionButonData.action{
            action()
        }
        
        if let callback = self.actionHandler{
            callback(button: sender)
        }
 
    }
}

class ActionButtonData{
    var title: String?
    var backgroundColor: UIColor?
    var titleColor: UIColor?
    var fontSize: CGFloat?
    var borderColor: UIColor?
    var borderWidth: CGFloat = 0
    var horizontalAlignment: UIControlContentHorizontalAlignment?
    var action: (() -> Void)?
    
    init(title: String? = "", backgroundColor: UIColor? = UIColor.defaulButtonColor(), titleColor: UIColor? = UIColor.whiteColor(), fontSize: CGFloat? = nil, borderWidth: CGFloat = 0, borderColor: UIColor? = UIColor.clearColor(), horizontalAlignment: UIControlContentHorizontalAlignment = UIControlContentHorizontalAlignment.Center, action: (() -> Void)? = nil){
        self.title = title
        self.backgroundColor = backgroundColor
        self.fontSize = fontSize
        self.titleColor = titleColor
        self.borderColor = borderColor
        self.borderWidth = borderWidth
        self.horizontalAlignment = horizontalAlignment
        self.action = action
    }
}