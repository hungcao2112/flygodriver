//
//  ProfileCollectionCell.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

class ProfileCollectionCell: InputCollectionViewCell {
    
    final let imageHeight = ProfileCollectionCell.defaultHeight / 2
    
    var imageView = UIImageView()
    
    var title: String?{
        didSet{
            self.setupLayout()
        }
    }
    
    var image: UIImage?{
        didSet{
            self.setupLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageView)
    }
    
    private func setupLayout(){
        if (image != nil){
            imageView.frame = CGRectMake(Constants.UI.Margin.Left, (self.height() - imageHeight)/2, imageHeight, imageHeight)
            imageView.image = image
        }
        else{
            imageView.frame = CGRectMake(0, 0, 0, 0)
        }
        
        if ((title?.isEmpty) != nil){
            titleLabel.frame = CGRectMake(0, 0, 0, 0)
        }
        else{
            titleLabel.frame = CGRectMake(imageView.maxX() + Constants.UI.Margin.Left, titleLabel.frame.minY, (title != "" ? 40 : 0), titleLabel.height())
            titleLabel.text = title
            titleLabel.textAlignment = NSTextAlignment.Center
        }
        
        inputTextField.frame = CGRectMake(titleLabel.maxX() + Constants.UI.Margin.Left, inputTextField.frame.minY, width() - 2*titleLabel.maxX() - Constants.UI.Margin.Left, inputTextField.height())
        inputTextField.textAlignment = NSTextAlignment.Center
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}