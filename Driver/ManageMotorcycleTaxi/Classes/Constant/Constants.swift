//
//  Constants.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    static let AppstoreURL = "https://itunes.apple.com/us/app/avigo/id1137342340"
    
    struct WebService {
        struct Path {
            static let TestingServer = ""
            static let DevelopmentServer = ""
            //static let ProductionServer = "http://avigo.com.vn:9000"
            //TODO: for testing
            static let host = "https://www.flygo.vn/api"
        }
        
        struct API {
            static let Login = "/account/login"
            static let RegisterMobile = "/mobile/register"
            
            static let ForgetPasswordGetCode = "/auth/forgetPassword/getCode"
            static let ForgetPasswordVerifyCode = "/auth/forgetPassword/verifyCode"
            static let ForgetPasswordUpdatePass = "/auth/forgetPassword/update"
            static let VerifiToken = "/auth/verifyToken"
            
            struct Customer {
                static let Path = "/customer"
                static let Register = "/user/register"
                static let SocialRegister = "/social/register"
                static let PhoneVerification = "/user/phoneVerification"
                static let SocialPhoneVerification = "/social/phoneVerification"
                static let GetHistory = "/user/getHistoryTrips"
                static let UpdateProfile = "/user/updateProfiles"
                static let CancelTrip = "/user/cancelTrip"
                static let GetDriverInfo = "/user/getDriverInfo"
                static let GetCurrentTrip = "/getCurrentTrip"
                static let SocialLogin = "/login/social"
            }
            
            struct User{
                static let Path = "/user"
                static let GetProfile = "/getProfiles"
                static let Logout = "/logout"
                static let UpdatePass = "/updatePassword"
            }
            
            struct Transport {
                static let Path = "/customer/user"
                static let GetListItems = "/getListItems"
            }
            
            struct Trip {
                static let Path = "/customer/user"
                static let BookTickets = "/bookTickets"
                static let UpdateMethodTickets = "/updateMethodTickets"
                static let CheckPromotionCode = "/checkPromotionCode"
                static let GetPromotions = "/getPromotions"
            }
            
            struct Driver {
                static let Path = ""
                static let GetAirport = "/getAirport"
                static let UpdateStatus = "/updateStatus"
                static let AcceptTrip = "/acceptTrip"
                static let StartDrive = "/startDrive"
                static let EndTrip = "/endTrip"
                static let GetHistoryTrips = "/driver/getBookings?token=\(Context.getAuthKey())"
                static let GetDriverQueue = "/getDriverQueue"
                static let GetAssignTrip = "/driver/update?token=\(Context.getAuthKey())"
                static let GetCurrentTrip = "/getCurrentTrip"
                static let RejectTrip = "/rejectTrip"
                static let Account = "/account"
                static let Register = "/account/register"
                static let Active = "/account/active"
                static let Profile = "/account/getProfile?token=\(Context.getAuthKey())"
                static let updatePassword = "/account/saveProfile?token=\(Context.getAuthKey())"
                static let logout = "/account/logout?token=\(Context.getAuthKey())"
                static let getTripDetail = "/driver/getBooking?token=\(Context.getAuthKey())"
            }
            
            struct Coordinator {
                static let Path = "/coordinator"
                static let EndTrip = "/endTrip"
                static let UpdateStatus = "/updateStatus"
                static let GetDrivers = "/getDrivers"
                //static let GetDrivers = "/getDriversTest"
                static let GetCurrentTrip = "/getCurrentTrip"
                static let GetDriverInfo = "/getDriverInfo"
            }
    
            struct Payment {
                static let Payment = "/customer/Payment"
                static let PaymentConfirm = "/customer/PaymentConfirm"
            }
        }
        
        static let IsRequestedCert = false
        
        static let CertificateName = ""
        
        static let Domain = "103.7.41.26:9000"
        
        static let ignoreSSLDomains = [
            Domain
        ]
        //Ex: github.com
    }
    
    struct StoryBoard {
        static let MainViewID = "MainViewStoryboardID"
        static let AutoLoginViewID = "AutoLoginViewStoryboardID"
        static let LoginViewID = "LoginViewStoryboardID"
        static let CustomerLoginViewID = "CustomerLoginViewStoryboardID"
        static let DriverViewID = "DriverViewStoryboardID"
        static let RegisterViewID = "RegisterViewStoryboardID"
        static let ForgotPasswordViewID = "ForgotPasswordViewStoryboardID"
        static let MobileVerificationViewID = "MobileVerificationViewStoryboardID"
        static let UpdatePasswordViewID = "UpdatePasswordViewStoryboardID"
        static let ProfileViewID = "ProfileViewStoryboardID"
        static let HistoryCollectionViewID = "HistoryCollectionViewStoryboardID"
        static let HistoryDetailViewID = "HistoryDetailViewStoryboardID"
        static let CustomerHomeViewID = "CustomerHomeViewStoryboardID"
        static let CustomerGuideTripViewID = "CustomerTripViewStoryboardID"
        static let TripPathViewID = "TripPathViewStoryboardID"
        static let CustomerBookingViewID = "CustomerBookingViewStoryboardID"
        static let CustomerRevealViewID = "CustomerRevealViewStoryboardID"
        static let CustomerBookingConfirmViewID = "CustomerBookingConfirmViewID"
        static let CustomerBookingPaymentViewControllerID = "CustomerBookingPaymentViewControllerID"
        static let CustomerBookingCompleteViewControllerID = "CustomerBookingCompleteViewControllerID"
        static let CustomerTripViewID = "CustomerTripViewStoryboardID"
        static let AppGuideViewControllerID = "AppGuideViewControllerID"
        static let SupportViewControllerID = "SupportViewControllerID"
        static let PromotionViewControllerID = "PromotionViewControllerID"
        
        struct Driver {
            static let DriverViewID = "DriverViewStoryboardID"
            static let DriverTripViewID = "DriverTripViewStoryboardID"
            static let DriverAcceptViewControllerID = "DriverAcceptViewControllerID"
            static let DriverWaitingAssignControllerID = "DriverWaitingAssignControllerID"
            static let DriverManagerControllerID = "DriverManagerControllerID"
        }
    }
    
    struct TSNAirportInfo {
        static let name = "Tan Son Nhat Airport"
        static let latitude = 10.813392
        static let longitude = 106.663637
//        static let validEnableReadyDistance = 20000.0
    }
    
    struct Transport {
        static let motobikeSlotNumber = 2
    }
    
    struct Trip {
        static let PaymentMethodTimeline = 24
        static let kMaxNumOfBike = 6
    }
    
    struct UI {
        struct Margin {
            static let Left = CGFloat(10.0)
            static let Right = CGFloat(10.0)
            static let Top = CGFloat(10.0)
            static let Bottom = CGFloat(10.0)
        }
        
        static let defaulCornerRadius = CGFloat(5.0)
        
        struct Button {
            static let cornerRadius = CGFloat(10.0)
            static let borderThickness = CGFloat(2.0)
        }
        
        struct CollectionCellHeight {
            static let NormalCellHeight = CGFloat(50.0)
            static let OrderCodeCellHeight = CGFloat(30.0)
            static let HistoryCellHeight = CGFloat(120.0)
            static let DriverEndTripInfoCellHeight = CGFloat(30.0)
            static let BankingCellHeight = CGFloat(120.0)
            static let BankingIntroCellHeight = CGFloat(80.0)
        }
        
        struct SideMenuBar {
            static let SideBarNotifName = "SideBarNotifName"
            static let SideBarNotifProfile = "SideBarNotifProfile"
            static let SideBarNotifBooking = "SideBarNotifBooking"
            static let SideBarNotifGuide = "SideBarNotifGuide"
            static let SideBarNotifHistory = "SideBarNotifHistory"
            static let SideBarNotifSupport = "SideBarNotifSupport"
            static let SideBarNotifPromotion = "SideBarNotifPromotion"
            static let SideBarNotifLogOut = "SideBarNotifLogOut"
            static let kSideBarHeightCellProfile = CGFloat(140.0)
            static let kSideBarHeightCellNormal = CGFloat(50.0)
        }
        
        struct MapView {
            static let StrokeWidth = CGFloat(5.0)
            static let CameraZoom = Float(12.0)
        }
    }
    
    struct Timer {
        static let TimerInterval = 0.1
        static let TimerDriverRefreshDriverQueue = 10.0
        static let TimerDriverRejectTrip = 120.0
        static let TimerCoordinatorRefreshDriverStatus = 20.0
    }
}
