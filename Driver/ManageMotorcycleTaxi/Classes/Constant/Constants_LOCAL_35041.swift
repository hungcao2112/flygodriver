//
//  Constants.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    struct WebService {
        struct Path {
            static let TestingServer = ""
            static let DevelopmentServer = ""
            static let ProductionServer = ""
            static let Host = ""
        }
        
        struct API {
            static let Login = ""
        }
        
        static let IsRequestedCert = false
        
        static let CertificateName = ""
        
        static let Domain = ""
        
        static let ignoreSSLDomains = [
            "",
            "",
            "",
            "",
            Domain
        ]
        //Ex: github.com
    }
    
    struct StoryBoard {
        static let MainViewID = "MainViewStoryboardID"
        static let LoginViewID = "LoginViewStoryboardID"
        static let CustomerViewID = "CustomerViewStoryboardID"
        static let DriverViewID = "DriverViewStoryboardID"
        static let RegisterViewID = "RegisterViewStoryboardID"
        static let MobileVerificationViewID = "MobileVerificationViewStoryboardID"
        static let CustomerHomeViewID = "CustomerHomeViewStoryboardID"
        static let CustomerTripViewID = "CustomerTripViewStoryboardID"
    }
    
    struct UI {
        struct Margin {
            static let Left = CGFloat(10.0)
            static let Right = CGFloat(10.0)
            static let Top = CGFloat(10.0)
            static let Bottom = CGFloat(10.0)
        }
        
        static let defaulCornerRadius = CGFloat(5.0)
        
        struct Button {
            static let cornerRadius = CGFloat(10.0)
            static let borderThickness = CGFloat(2.0)
        }
    }
}
