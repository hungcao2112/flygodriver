//
//  Constants.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
struct Constants {
    struct WebService {
        struct Path {
            static let TestingServer = ""
            static let DevelopmentServer = ""
            static let ProductionServer = ""
            static let Host = ""
        }
        
        struct API {
            static let Login = ""
        }
        
        static let IsRequestedCert = false
        
        static let CertificateName = ""
        
        static let Domain = ""
        
        static let ignoreSSLDomains = [
            "",
            "",
            "",
            "",
            Domain
        ]
        //Ex: github.com
    }
    
    struct StoryBoard {
        static let MainViewID = "MainViewStoryboardID"
        static let LoginViewID = "LoginViewStoryboardID"
        static let CustomerViewID = "CustomerViewStoryboardID"
        static let DriverViewID = "DriverViewStoryboardID"
    }
    
}
