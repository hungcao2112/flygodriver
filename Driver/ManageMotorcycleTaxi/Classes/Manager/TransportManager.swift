//
//  TransportManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/28/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class TransportManager {
    class func getListItems() -> Promise<AnyObject> {
        var auth = Context.getAuthKey()
        if auth == "" {
            auth = "anonymous@combros"
        }
        return Promise{ fulfill, reject in
            TransportService.getListItems([:], headers: ["auth":auth], completion: {
                (response) in
                print(response)
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<TransportListResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }

                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else {
                    reject(response.result.error!)
                }
                })
        }
    }
}