//
//  DriverPushManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit

enum DriverPushType : Int{
    case Unknown = 0,
    AcceptedTrip = 201,
    Driving = 202,
    AssignedTrip = 203
}

extension PushManager{
    func handleReceivedPushInfo (userInfo: [NSObject:AnyObject]){
        if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            
            let controller = Storyboards.driver.instantiateViewControllerWithIdentifier(Constants.StoryBoard.Driver.DriverAcceptViewControllerID)
                as! DriverAcceptViewController
            let str = userInfo["aps"]!["alert"]!!["title"]!
            controller.code = str!.componentsSeparatedByString(",")[0].componentsSeparatedByString(" ")[2]
            
            if let topController = topController as? MainNavigationController {
                if NavigationManager.sharedObject.isDriverAcceptViewAppear == false {
                    controller.navigator = Navigator.init(storyboard: Storyboards.driver, navigationController: topController)
                    if str != nil{
                        topController.pushViewController(controller, animated: true)
                    }
                }
            }
        }
    }
}
