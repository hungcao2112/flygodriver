//
//  AuthManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/18/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class AuthManager{
    
    class func registerMobile(pushKey: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["pushKey": pushKey, "imei": "", "deviceType" : "IOS"]
        let headers: [String: String] = ["auth": Context.getAuthKey()]
        return Promise{ fulfill, reject in
            AuthService.registerMobile(parameters, headers: headers, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    
    
    class func getCode(account: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["account": account]
        return Promise{ fulfill, reject in
            AuthService.getCode(parameters, completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let codeResponse = Mapper<CodeResponse>().map(response.result.value) {
                            fulfill(codeResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    
    class func verifyCode(code code: String, account: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["code" : code, "account": account]
        return Promise{ fulfill, reject in
            AuthService.verifyCode(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let codeResponse = Mapper<CodeResponse>().map(response.result.value) {
                            fulfill(codeResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func updatePass(code code: String, account: String, password: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["code" : code, "account": account, "password": password]
        return Promise{ fulfill, reject in
            AuthService.updatePassword(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        
                        print(response)
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func phoneVerification(code code: String, id: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["code" : code, "id": id]
        return Promise{ fulfill, reject in
            DriverService.active(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let loginResponse = Mapper<LoginResponse<User>>().map(response.result.value) {
                            fulfill(loginResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func socialPhoneVerification(code code: String, phone: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["code" : code, "phone": phone]
        return Promise{ fulfill, reject in
            CustomerService.socialPhoneVerifyCode(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let loginResponse = Mapper<LoginResponse<User>>().map(response.result.value) {
                            fulfill(loginResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func verifyToken(isDriver: Bool = false) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["token" : Context.getAuthKey()]
        return Promise{ fulfill, reject in
            AuthService.verifyToken(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if !isDriver{
                            if let loginResponse = Mapper<LoginResponse<User>>().map(response.result.value) {
                                fulfill(loginResponse)
                            }
                        }
                        else{
                            if let loginResponse = Mapper<LoginResponse<Driver>>().map(response.result.value) {
                                fulfill(loginResponse)
                            }
                        }
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
}
