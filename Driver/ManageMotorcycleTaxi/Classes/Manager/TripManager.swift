//
//  TripManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 6/27/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class TripManager{
    class func bookTickets(trip: Trip) -> Promise<AnyObject> {
        let headers: [String: String] = ["auth": Context.getAuthKey()]
        let parameters: [String: AnyObject] = ["startPoint": trip.originPlace.name, "endPoint": trip.destinationPlace.name, "estimatePrice": trip.priceExpected, "startDate": trip.startDate.toServerDayTimeString(), "vehicleId": trip.vehicleId, "kilometer": trip.kilometerExpected, "promotion": trip.promotion.code, "roundTrip": trip.isRoundTrip, "paymentMethod": trip.paymentMethod.rawValue, "amount": trip.numberOfBike, "duration": trip.durationExpected, "startLatitude": trip.originPlace.coordinate.latitude, "startLongitude": trip.originPlace.coordinate.longitude, "endLatitude": trip.destinationPlace.coordinate.latitude, "endLongitude": trip.destinationPlace.coordinate.longitude]
        return Promise{ fulfill, reject in
            TripService.bookTickets(parameters, headers: headers, completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<TripArrayResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    
    class func updateMethodTickets(invoiceCodeNumber: String, method: PaymentMethod) -> Promise<AnyObject> {
        let headers: [String: String] = ["auth": Context.getAuthKey()]
        let parameters: [String: AnyObject] = ["invoiceCodeNumber":invoiceCodeNumber, "method": method.rawValue]
        return Promise{ fulfill, reject in
            TripService.updateMethodTickets(parameters, headers: headers, completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<SingleTripResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    
    class func getPromotions(key: String) -> Promise<AnyObject> {
        let headers: [String: String] = ["auth": Context.getAuthKey()]
        let parameters: [String: AnyObject] = ["key":key]
        return Promise{ fulfill, reject in
            TripService.getPromotions(parameters, headers: headers, completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<GetPromotionsResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    
    class func checkPromotionCode(code: String) -> Promise<AnyObject> {
        let headers: [String: String] = ["auth": Context.getAuthKey()]
        let parameters: [String: AnyObject] = ["code":code]
        return Promise{ fulfill, reject in
            TripService.checkPromotionCode(parameters, headers: headers, completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<SinglePromotionResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
}