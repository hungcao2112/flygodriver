//
//  CustomerNavigationManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 8/6/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
extension NavigationManager{
    func navigateToLoginView(){
        let controller = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerHomeViewID)
            as! CustomerHomeViewController
        
        let navigationController = UINavigationController(rootViewController: controller)
        let navigator = Navigator(storyboard: Storyboards.customer, navigationController: navigationController)
        NavigationManager.sharedObject.navigator = navigator
        controller.navigator = navigator
        
        let mainWindow = UIApplication.sharedApplication().windows.first
        mainWindow?.rootViewController = navigationController
        mainWindow?.makeKeyAndVisible()
        
        let loginViewController = Storyboards.customer.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerLoginViewID)
            as! LoginViewController
        loginViewController.navigator = navigator
        
        navigationController.pushViewController(loginViewController, animated: true)
    }
}