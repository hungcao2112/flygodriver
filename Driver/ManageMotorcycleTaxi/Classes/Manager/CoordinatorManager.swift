//
//  CoordinatorManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/21/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class CoordinatorManager {
    class func getDrivers(vehicleID: Int = 1) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CoordinatorService.getDrivers(["vehicleId" : vehicleID], headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let loginResponse = Mapper<CoordinatorGetDriversResponse<Driver>>().map(response.result.value) {
                            fulfill(loginResponse)
                        }
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func getDriverInfo(driver: String) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CoordinatorService.getDriverInfo(["driver" : driver], headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<LoginResponse<Driver>>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func updateStatus(status: DriverStatus, driverAccount: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["driver": driverAccount, "status": status.rawValue]
        return Promise{ fulfill, reject in
            CoordinatorService.updateStatus(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<LoginResponse<Driver>>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func getCurrentTrip(driverAccount: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["driver": driverAccount]
        return Promise{ fulfill, reject in
            DriverService.getCurrentTrip(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let requestResponse = Mapper<TripArrayResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func endTrip(driverAccount: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["driver": driverAccount]
        return Promise{ fulfill, reject in
            CoordinatorService.endTrip(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let requestResponse = Mapper<SingleTripResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
}