//
//  CustomerPushManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

enum CustomerPushType : Int{
    case Unknown = 0,
    RegisteredTrip = 101,
    CanceledTrip = 102,
    AssignedTrip = 103,
    PaidInvoice = 104,
    DriverAcceptedTrip = 105,
    StartedTrip = 106,
    EndedTrip = 107
}

extension PushManager{
    func handleReceivedLaunchingNotification(){
        self.handleReceivedPushInfo(launchingNotification ?? [NSObject:AnyObject]() , complete: { () -> Void in
            self.launchingNotification = nil
        })
    }
    
    func handleReceivedPushInfo (userInfo: [NSObject:AnyObject], complete: (() -> ())? = nil){
        if let pushTypeId = userInfo["push_type"] as? Int {
            let pushType = CustomerPushType(rawValue: pushTypeId) ?? .Unknown
            
            if let dataStr = userInfo["data"] as? String{
                if let pushNotification = Mapper<PushNotification>().map(dataStr) {
                    switch pushType {
                    case .RegisteredTrip:
                        break
                    case .CanceledTrip:
                        break
                    case .AssignedTrip:
                        break
                    case .PaidInvoice:
                        break
                    case .DriverAcceptedTrip:
                        if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                            }
                            
                            topController.showAlertWindow(NSLocalizedString("Driver has accepted your trip", comment: "Driver has accepted your trip"), message: NSLocalizedString("Do you want to see your trip?", comment: "Do you want to see your trip?"),okAction: {
                                NavigationManager.sharedObject.navigationToTripHistoryView(pushNotification.trip ?? Trip(), driver: pushNotification.driver ?? Driver())
                            })
                        }
                    case .StartedTrip:
                        if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                            }
                            topController.showAlertWindow(NSLocalizedString("Your trip has been started", comment: "Your trip has been started"), message: NSLocalizedString("Do you want to see your trip?", comment: "Do you want to see your trip?"),okAction: {
                                 NavigationManager.sharedObject.navigationToTripHistoryView(pushNotification.trip ?? Trip(), driver: pushNotification.driver ?? Driver())
                            })
                        }
                    case .EndedTrip:
                        if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                            }
                            topController.showEndTripNotificationView(tripHistory: pushNotification.trip ?? Trip())
                        }

                        break
                    default:
                        break
                    }
                }
            }
        }
        
        if let action = complete{
            action()
        }
    }
}