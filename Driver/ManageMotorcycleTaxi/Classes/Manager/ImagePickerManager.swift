//
//  ImagePickerManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/30/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

protocol ImagePickerManagerDelegate {
    func didPickImage(image: UIImage!)
}

class ImagePickerManager: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    private var imagePickerController = UIImagePickerController()
    private var currentViewController: UIViewController!
    var delegate: ImagePickerManagerDelegate?
    
    init(viewController: UIViewController) {
        currentViewController = viewController
    }
    
    func presentPickerPhotoOptionView(cameraDevice cameraDevice: UIImagePickerControllerCameraDevice = .Front) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("Take from camera", comment: "Take from camera"), style: .Default, handler: { (alert: UIAlertAction!) -> Void in
            self.showCamera(withDevice: cameraDevice)
        })
        
        let photoLibraryAction = UIAlertAction(title:  NSLocalizedString("Take from photo library", comment: "Take from photo library"), style: .Default, handler: { (alert: UIAlertAction!) -> Void in
            self.showPhotoLibrary()
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .Cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = currentViewController.view
        alertController.popoverPresentationController?.sourceRect = currentViewController.view.bounds
        currentViewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showCamera(withDevice cameraDevice: UIImagePickerControllerCameraDevice = .Front) {
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            if !Utils.checkCameraPermission() { return }
            imagePickerController.sourceType = .Camera
            imagePickerController.cameraDevice = cameraDevice
            imagePickerController.delegate = self
            currentViewController.presentViewController(imagePickerController, animated: true, completion: nil)
        } else {
            
        }
    }
    
    func showPhotoLibrary() {
        imagePickerController.sourceType = .PhotoLibrary
        imagePickerController.delegate = self
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            currentViewController.presentViewController(imagePickerController, animated: true, completion: nil)
        } else {
        }
    }
    
    // UIImagePickerController delegates
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imagePickerController.dismissViewControllerAnimated(true) {
            let image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            self.delegate?.didPickImage(image)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        currentViewController.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
}