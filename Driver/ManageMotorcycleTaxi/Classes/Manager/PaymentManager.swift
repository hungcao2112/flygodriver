//
//  PaymentManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 8/12/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class PaymentManager{
    class func payment(amount: Int, phone: String, bankID: Int, codeNumber: String) -> Promise<AnyObject> {
        let headers: [String: String] = ["auth": Context.getAuthKey()]
        let parameters: [String: AnyObject] = ["amount": amount, "phone": phone, "bankID": bankID, "codeNumber" : codeNumber]
        return Promise{ fulfill, reject in
            PaymentService.payment(parameters, headers: headers, completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<PaymentResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    
    class func paymentConfirm(tranid: String, confirmCode: String, txnAmount: Int, mac: String, codeNumber: String) -> Promise<AnyObject> {
        let headers: [String: String] = ["auth": Context.getAuthKey()]
        let parameters: [String: AnyObject] = ["tranid": tranid, "confirmCode": confirmCode, "txnAmount": txnAmount, "mac": mac, "codeNumber":codeNumber]
        return Promise{ fulfill, reject in
            PaymentService.paymentConfirm(parameters, headers: headers, completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<PaymentResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
}