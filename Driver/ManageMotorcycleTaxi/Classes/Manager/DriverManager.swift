//
//  DriverManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class DriverManager {
    class func getAirport() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            DriverService.getAirport([:], headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let getAirportResponse = Mapper<DriverGetAirportResponse>().map(response.result.value) {
                            fulfill(getAirportResponse)
                        }

                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func getDriverQueue() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            DriverService.getDriverQueue([:], headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let getAirportResponse = Mapper<DriverGetDriverQueueResponse>().map(response.result.value) {
                            fulfill(getAirportResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func updateStatus(status: Int) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["status": status]
        return Promise{ fulfill, reject in
            DriverService.updateStatus(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<LoginResponse<Driver>>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func acceptTrip(code: String, status: String = "3") -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["code": code, "status": status]
        return Promise{ fulfill, reject in
            DriverService.getAssignTrip(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let requestResponse = Mapper<SingleTripResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    class func startTrip(code: String, status: String = "2") -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["code": code, "status": status]
        return Promise{ fulfill, reject in
            DriverService.getAssignTrip(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let requestResponse = Mapper<SingleTripResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    class func rejectTrip(code: String, status: String = "0") -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["code": code, "status": status]
        return Promise{ fulfill, reject in
            DriverService.getAssignTrip(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let requestResponse = Mapper<SingleTripResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func getAssignTrip(code: String, status: String) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            DriverService.getAssignTrip(["code": code, "status": status], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
//                        print(response)
                        if let requestResponse = Mapper<TripArrayResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func getCurrentTrip() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            DriverService.getCurrentTrip([:], headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let requestResponse = Mapper<TripArrayResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func startDrive(invoiceCodeNumber: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["invoiceCodeNumber": invoiceCodeNumber]
        return Promise{ fulfill, reject in
            DriverService.startDrive(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let requestResponse = Mapper<SingleTripResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func endTrip(invoiceCodeNumber: String, endPoint: String, endLatitude: Double, endLongitude: Double, kilometer: Double, duration: Int) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["invoiceCodeNumber": invoiceCodeNumber, "endPoint": endPoint, "endLatitude": endLatitude, "endLongitude": endLongitude, "kilometer": kilometer, "duration": duration]
        return Promise{ fulfill, reject in
            DriverService.endTrip(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                print(response)
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<SingleTripResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func getHistoryTrips(page: String, limit: String, order_by: String, order_mode: String) -> Promise<AnyObject> {
        let parameters = ["page": page ,"limit": limit, "order_by": order_by, "order_mode": order_mode]
        return Promise{ fulfill, reject in
            DriverService.getHistoryTrips(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<TripHistoryResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    class func getTripDetail(code: String) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            DriverService.getTripDetail(["code": code], completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let requestResponse = Mapper<DriverTripResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        var error = NSError(domain: "", code: statusCode, userInfo: nil)
                        if let errorResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                            if let errorCode = errorResponse.code as Int? {
                                error = NSError(domain: errorResponse.message ?? "", code: errorCode ?? 0, userInfo: nil)
                            }
                        }
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }

}
