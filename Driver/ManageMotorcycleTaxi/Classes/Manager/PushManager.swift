//
//  PushManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

class PushManager{
    
    var launchingNotification: [NSObject:AnyObject]?
    
    class var sharedObject: PushManager {
        get {
            struct Singleton {
                static let instance = PushManager()
            }
            return Singleton.instance
        }
    }
}