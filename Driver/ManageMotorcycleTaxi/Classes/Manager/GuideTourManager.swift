//
//  GuideTourManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/27/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

class GuideTourManager {
    private var guideTourItems = [GuideTourItem]()
    private var currentCircleMarkView: CircleMaskView?
    
    class var sharedObject: GuideTourManager {
        get {
            struct Singleton {
                static let instance = GuideTourManager()
            }
            return Singleton.instance
        }
    }
    
    func resetGuideTourItems(){
        if currentCircleMarkView != nil{
            currentCircleMarkView?.remove()
        }
        guideTourItems = []
    }
    
    func addGuideTourItem(item: GuideTourItem){
        guideTourItems.append(item)
    }
    
    func startTour(){
        showGuideTourItem(0)
    }
    
    func showGuideTourItem(index: Int){
        if index < self.guideTourItems.count{
            let guideTourItem = self.guideTourItems[index]
            currentCircleMarkView = CircleMaskView(drawIn: guideTourItem.superView, circleRect: guideTourItem.view.convertRect(guideTourItem.view.bounds, toView: guideTourItem.superView))
            currentCircleMarkView!.opacity = 0.5
            currentCircleMarkView!.draw()
            currentCircleMarkView?.addGuideMessageView(guideTourItem.title, message: guideTourItem.message)
            currentCircleMarkView?.didTapSubCircleView = { [weak self] () -> () in
                if let strongSelf = self{
                    strongSelf.currentCircleMarkView?.remove()
                    strongSelf.showGuideTourItem(index + 1)
                }
            }
        }
    }
}

class GuideTourItem {
    var view: UIView
    var superView: UIView
    var title: String?
    var message: String?
    
    var action: (() -> ())?
    init(view: UIView, superView: UIView, title: String? = "", message: String? = "", action: (() -> ())?){
        self.view = view
        self.superView = superView
        self.title = title
        self.message = message
        self.action = action
    }
}