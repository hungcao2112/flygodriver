//
//  CustomerManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/27/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class CustomerManager {
    enum SocialType: Int {
        case FACEBOOK = 0,
        GOOGLE
        
        static func getEnumString(socialType: SocialType) -> String{
            switch socialType {
            case SocialType.FACEBOOK:
                return "FACEBOOK"
            case SocialType.GOOGLE:
                return "GOOGLE"
            }
        }
    }
    
    class func getHistories(page: Int = 0) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CustomerService.getTripHistories(["page": page], headers: Context.getTokenHeader(), completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<TripHistoryResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    
    class func updateProfile(name: String, email: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["fullName" : name, "email": email]
        return Promise{ fulfill, reject in
            CustomerService.updateProfile(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        print(response)
                        if let loginResponse = Mapper<LoginResponse<User>>().map(response.result.value) {
                            fulfill(loginResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func cancelTrip(invoiceCodeNumber invoiceCodeNumber: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["invoiceCodeNumber" : invoiceCodeNumber]
        return Promise{ fulfill, reject in
            CustomerService.cancelTrip(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if let requestResponse = Mapper<RequestResponse<EmptyResponseData>>().map(response.result.value) {
                        fulfill(requestResponse)
                    }
                    
                    fulfill("OK")
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func getDriverInfo(driverNumber driverNumber: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = ["driver" : driverNumber]
        return Promise{ fulfill, reject in
            CustomerService.getDriverInfo(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if let requestResponse = Mapper<LoginResponse<Driver>>().map(response.result.value) {
                        fulfill(requestResponse)
                    }
                    
                    fulfill("OK")
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func getCurrentTrip() -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = [:]
        return Promise{ fulfill, reject in
            CustomerService.getCurrentTrip(parameters, headers: ["auth" : Context.getAuthKey()], completion: {
                (response) in
                if response.result.isSuccess{
                    if let requestResponse = Mapper<TripArrayResponse>().map(response.result.value) {
                        fulfill(requestResponse)
                    }
                    
                    fulfill("OK")
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    class func socialLogin(socialId: String, socialToken: String, socialType: SocialType) -> Promise<AnyObject>{
        
        let parameters: [String: AnyObject] = ["type" : SocialType.getEnumString(socialType),"socialId" : socialId, "socialToken" : socialToken]
        
        return Promise{ fulfill, reject in
            CustomerService.socialLogin(parameters, headers: [:], completion: {
                (response) in
                if response.result.isSuccess{
                    print(response.result.value)
                    if let loginResponse = Mapper<LoginResponse<User>>().map(response.result.value) {
                        fulfill(loginResponse)
                    }
                    
                    fulfill("OK")
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
}