//
//  UserManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class UserManager {
    class func getProfile() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            UserService.getProfile([:], headers: Context.getTokenHeader(), completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        let currentTarget = GlobalAppData.sharedObject.currentTarget
                        switch currentTarget{
                        case .Customer:
                            if let requestResponse = Mapper<LoginResponse<User>>().map(response.result.value) {
                                fulfill(requestResponse)
                            }
                        case .Driver:
                            if let requestResponse = Mapper<LoginResponse<Driver>>().map(response.result.value) {
                                fulfill(requestResponse)
                            }
                        default:
                            break
                        }
//                        if let requestResponse = Mapper<RequestResponse<LoginResponseData<User>>>().map(response.result.value) {
//                            fulfill(requestResponse)
//                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }
    
    class func logout() -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = [:]
        return Promise{ fulfill, reject in
            DriverService.logout(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<ProfileResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
                
            })
        }
    }

    class func uploadAvatar(imageData: NSData) -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            UserService.uploadAvatar(imageData, success: { response in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<LoginResponse<User>>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        
                        fulfill("OK")
                    } else {
                    }
                } else {
                    reject(response.result.error!)
                }
                }, fail: { (error) in
                    reject(error)
            })
        }
    }
    
    class func updatePass(phone: String, fullname: String, address: String, country: String, city: String, title: String, birthday: String, new_password: String) -> Promise<AnyObject> {
        let parameters: [String: AnyObject] = [
            "phone" : phone,
            "fullname": fullname,
            "address": address,
            "country": country,
            "city": city,
            "title": title,
            "birthday": birthday,
            "new_password": new_password
            ]
        return Promise{ fulfill, reject in
            DriverService.updatePassword(parameters, completion: {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        if let requestResponse = Mapper<ProfileResponse>().map(response.result.value) {
                            fulfill(requestResponse)
                        }
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        
                        print(response)
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            })
        }
    }
}
