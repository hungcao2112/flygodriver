//
//  NavigationManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/12/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

class NavigationManager{
    var navigator: Navigator?
    var isDriverAcceptViewAppear = false
    
    class var sharedObject: NavigationManager {
        get {
            struct Singleton {
                static let instance = NavigationManager()
            }
            return Singleton.instance
        }
    }
    
    func navigationToTripHistoryView(tripHistory: Trip, driver: Driver){
        if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            let controller = Storyboards.customerBooking.instantiateViewControllerWithIdentifier(Constants.StoryBoard.CustomerTripViewID)
                as! CustomerTripViewController
            controller.tripHistory = tripHistory
            
            if let customSWRevealViewController = topController as? CustomSWRevealViewController{
                if let frontViewController = customSWRevealViewController.frontViewController as? UINavigationController{
                    let newNavVC = UINavigationController(rootViewController: controller)
                    newNavVC.navigationBar.format()
                    frontViewController.presentViewController(newNavVC, animated: true, completion: nil)
                }
                else{
                    let newNavVC = UINavigationController(rootViewController: controller)
                    newNavVC.navigationBar.format()
                    customSWRevealViewController.frontViewController.navigationController?.presentViewController(newNavVC, animated: true, completion: nil)
                }
            }
            else if let topController = topController as? BaseViewController{
                let newNavVC = UINavigationController(rootViewController: controller)
                newNavVC.navigationBar.format()
                topController.navigator!.navigationController?.presentViewController(newNavVC, animated: true, completion: nil)
            }
            else if let vc = topController as? CustomerTripViewController{
                vc.dismissViewControllerAnimated(true, completion: nil)
                self.navigationToTripHistoryView(tripHistory, driver: driver)
            }
            else if let nvc = topController as? UINavigationController{
                for vc in nvc.viewControllers{
                    if let customerTripViewController = vc as? CustomerTripViewController{
                        customerTripViewController.dismissViewControllerAnimated(true, completion: { 
//                            let newNavVC = UINavigationController(rootViewController: controller)
//                            newNavVC.navigationBar.format()
//                            nvc.presentViewController(newNavVC, animated: true, completion: nil)
                            self.navigationToTripHistoryView(tripHistory, driver: driver)
                        })
                        break
                    }
                }
            }
        }
    }
}