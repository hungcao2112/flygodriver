//
//  CacheManager.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/7/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

class CacheManager {
    class func getCachedImages(transports: [Transport]!) {
        for transport in transports! {
            let motobikeImageView = UIImageView()
            motobikeImageView.af_setImageWithURL(
                NSURL(string: transport.icon)!,
                placeholderImage: nil,
                filter: nil,
                imageTransition: .None,
                completion: { response in
                    print(transport.icon)
                    print(response.result.value)
                    print(response.result.error)
            })
        }
    }
}