//
//  GlobalAppData.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

enum Target: Int{
    case Unknown = 0,
    Customer,
    Driver
}

class GlobalAppData{

    private var loginUser: User?
    private var currentTrip: Trip?
    private var pendingOrderingTrip: Trip?
    private var transportList: [Transport]?
    private var currentDriverTrip: DriverTrip?
    //all transports, include available & not available
    
    //ONLY AVAILABLE
    private var transportAvailableList = [Transport]()              //all transports with available status
    private var transportDict = NSMutableDictionary()               //
    private var transportSlotNumbersList = NSMutableArray()         //all slot number
    private var transportPresentativeList = NSMutableArray()        //all presentative transport
    private var airportPlace: GMSCustomPlace?
    
    private var isTestingServer = false
    
    var currentTarget = Target.Driver
    
    class var sharedObject: GlobalAppData {
        get {
            struct Singleton {
                static let instance = GlobalAppData()
            }
            return Singleton.instance
        }
    }
    
    func setIsTestingServer(isTesting: Bool) {
        isTestingServer = isTesting
    }
    
    func getIsTestingServer() -> Bool {
        return isTestingServer
    }
    
    func setLoginUser(user: User){
        loginUser = user
    }
    
    func getLoginUser() -> User{
        return loginUser ?? User()
    }
    
    func setCurrentTrip(trip: DriverTrip){
        currentDriverTrip = trip
    }
    
    func getCurrentTrip() -> DriverTrip{
        return currentDriverTrip ?? DriverTrip()
    }
    
    func setPendingOrderTrip(trip: Trip?){
        pendingOrderingTrip = trip
    }
    
    func getPendingOrderTrip() -> Trip?{
        return pendingOrderingTrip
    }
    
    func setTransportList(list: [Transport]) {
        transportList = list
        for transport in list {
            if let transport = transport as Transport! {
                if transport.status == TransportStatus.Available {
                    transportAvailableList.append(transport)
                    if (!transportSlotNumbersList.containsObject(transport.slot_number)) {
                        transportSlotNumbersList.addObject(transport.slot_number)
                        transportPresentativeList.addObject(transport)
                    }
                }
            }
        }
        for slot_number in transportSlotNumbersList {
            let transports = NSMutableArray()
            for transport in transportAvailableList {
                if (transport.slot_number == Int(slot_number as! NSNumber)) {
                    transports.addObject(transport)
                }
            }
            transportDict.setValue(transports, forKey: String(slot_number))
        }
    }
    
    func getTransportSlotNumber(vehicleID: Int) -> Int {
        for transport in transportList! {
            if (transport.id == vehicleID) {
                return transport.slot_number
            }
        }
        return 2
    }
    
    func getTransportAvailableListBySlotNumber(slotNumber: Int) -> [Transport] {
        let slotNumberString = String(slotNumber)
        if let transports = transportDict.valueForKey(slotNumberString) as? [Transport]{
            return transports
        }
        return [Transport]()
    }
    
    func getTransportByVehicleID(vehicleID: Int) -> Transport! {
        for transport in transportList! {
            if (transport.id == vehicleID) {
                return transport
            }
        }
        return nil
    }
    
     func getTransportPresentativeList() -> NSMutableArray {
        return transportPresentativeList
     }
    
    func setAirportPlace(airportPlace: GMSCustomPlace) {
        self.airportPlace = airportPlace
    }
    
    func getAirportPlace() -> GMSCustomPlace {
        return airportPlace ?? GMSCustomPlace()
    }
}
