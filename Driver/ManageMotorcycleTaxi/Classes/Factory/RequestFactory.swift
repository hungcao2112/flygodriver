//
//  RequestFactory.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/5/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

import Foundation
import Alamofire

class RequestFactory {
    
    static let networkManager: Manager = {
        
        if !Constants.WebService.IsRequestedCert {
            return Alamofire.Manager()
        }
        
//        let pathToCert = NSBundle.mainBundle().pathForResource(Constants.WebService.CertificateName, ofType: "cer") ?? ""
//        let localCertificate:NSData = NSData(contentsOfFile: pathToCert) ?? NSData()
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = Manager.defaultHTTPHeaders
        
        var serverTrustPolicies: [String: ServerTrustPolicy] = [:]
        
//        let serverTrustPolicy = ServerTrustPolicy.PinCertificates(
//            certificates: [SecCertificateCreateWithData(nil, localCertificate)!],
//            validateCertificateChain: true,
//            validateHost: true
//        )
        
//        serverTrustPolicies = [
//            Constants.WebService.Domain: serverTrustPolicy
//        ]
        
        for domain in Constants.WebService.ignoreSSLDomains {
            serverTrustPolicies[domain] = .DisableEvaluation
        }
        
        return Manager(
            configuration: NSURLSessionConfiguration.defaultSessionConfiguration(),
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
    }()
    
    class func get(url : URLStringConvertible, parameters : [String : AnyObject]? = nil) -> Request{
        
        let request = networkManager.request(Alamofire.Method.GET, url, parameters: parameters, headers : nil)
        return request
    }
    
    class func post(url : URLStringConvertible, parameters : [String : AnyObject]? = nil, appendUserKey: Bool = true) -> Request{
        
        let request = networkManager.request(Alamofire.Method.POST, url, parameters: parameters, headers : nil, encoding : .JSON)
        
        return request
    }
}