//
//  UILabelExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

extension UILabel{
    func heightBaseText(text text: String? = nil, width: CGFloat? = nil) -> CGFloat {
        let label = UILabel(frame: CGRectMake(0, 0, (width != nil) ? width! : frame.width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = lineBreakMode
        label.font = font
        label.text = (text != nil) ? text! : self.text
        
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func widthBaseText(text text: String? = nil, height: CGFloat? = nil) -> CGFloat {
        let label = UILabel(frame: CGRectMake(0, 0, CGFloat.max, (height != nil) ? height! : frame.height))
        label.numberOfLines = 1
        label.font = font
        label.text = (text != nil) ? text! : self.text
        
        label.sizeToFit()
        
        return label.frame.width
    }

}