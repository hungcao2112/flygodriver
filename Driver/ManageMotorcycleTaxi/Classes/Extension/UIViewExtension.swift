//
//  UIViewExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/6/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func minX() -> CGFloat{
        return self.frame.minX
    }
    
    func minY() -> CGFloat{
        return self.frame.minY
    }
    
    func midX() -> CGFloat{
        return self.frame.midX
    }
    
    func midY() -> CGFloat{
        return self.frame.midY
    }
    
    func maxX() -> CGFloat{
        return self.frame.maxX
    }
    
    func maxY() -> CGFloat{
        return self.frame.maxY
    }
    
    func width() -> CGFloat{
        return self.frame.size.width
    }
    
    func height() -> CGFloat{
        return self.frame.size.height
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let boundView = self.bounds
        let path = UIBezierPath(roundedRect: boundView, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        self.layer.mask = mask
    }
    
    func roundCornersWithBorder(borderWidth: CGFloat, borderColor: CGColor, radius: CGFloat) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setBorder(borderWidth: CGFloat, borderColor: CGColor) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor
    }
    
    func setBorderDefaultColor(borderWidth: CGFloat) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    func setBorderYellowColor(borderWidth: CGFloat) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = UIColor.yellowColor().CGColor
    }
    
    func roundCornersWithBorder(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func addMenuDefaultGradientLayer(){
        let blueLayer = UIColor.menuGradient()
        blueLayer.frame = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height)
        self.layer.insertSublayer(blueLayer, atIndex: 0)
    }
    
    func addDefaultGradientLayer(){
        let blueLayer = UIColor.defaultGradient()
        blueLayer.frame = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height)
        self.layer.insertSublayer(blueLayer, atIndex: 0)
    }
    
    func setGradient(color1: UIColor? = UIColor(red: 201/255, green: 37/255, blue: 44/255, alpha: 1.0), color2: UIColor? = UIColor(red: 247/255, green: 148/255, blue: 30/255, alpha: 1.0)) {
        self.backgroundColor = UIColor.clearColor()
        let context = UIGraphicsGetCurrentContext()
        let gradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [color1!.CGColor, color2!.CGColor], [0, 1])!
        
        // Draw Path
        let path = UIBezierPath(rect: CGRectMake(0, 0, frame.width, frame.height))
        CGContextSaveGState(context!)
        path.addClip()
        CGContextDrawLinearGradient(context!, gradient, CGPointMake(frame.width / 2, 0), CGPointMake(frame.width / 2, frame.height), CGGradientDrawingOptions())
        CGContextRestoreGState(context!)
    }
    
    func addShadowView(width:CGFloat=1.0, height:CGFloat=1.0, Opacidade:Float=0.7, maskToBounds:Bool=true, radius:CGFloat=10.0){
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = Opacidade
        self.layer.masksToBounds = maskToBounds
    }
}
