//
//  UIColorExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/6/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    class func defaultGreenAppColor() -> UIColor {
        return UIColor(red: 0/255, green: 100/255, blue: 55/255, alpha: 1.0)
    }
    
    class func defaultAppButtonGreenColor() -> UIColor {
        return UIColor(red: 33/255, green: 178/255, blue: 75/255, alpha: 1.0)
    }
    
    class func defaultAppColor() -> UIColor {
//        return UIColor(red: 247/255, green: 148/255, blue: 30/255, alpha: 1.0)  //f7941e
//        return UIColor(red: 194/255, green: 33/255, blue: 38/255, alpha: 1.0)
        return defaultAppRedColor()
    }
    
    class func defaultAppRedColor() -> UIColor {
        return UIColor(red: 42/255, green: 64/255, blue: 143/255, alpha: 1.0)
    }
    
    class func defaultAppYellowColor() -> UIColor {
        return UIColor(red: 247/255, green: 148/255, blue: 30/255, alpha: 1.0)  //f7941e
    }
    
    class func defaultBackgroundColor() -> UIColor {
        return UIColor(red: 224/255, green: 223/255, blue: 227/255, alpha: 1.0)
    }
    
    class func defaultBorderColor() -> UIColor {
        return UIColor(red: 196/255, green: 196/255, blue: 196/255, alpha: 1.0)
    }
    
    class func defaulButtonColor() -> UIColor {
        return defaultAppRedColor()
        //return UIColor(red: 0/255, green: 100/255, blue: 55/255, alpha: 1.0)
        //return UIColor(red: 247/255, green: 148/255, blue: 30/255, alpha: 1.0)
    }
    
    class func secondaryButtonColor() -> UIColor {
        //return UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        return UIColor(red: 247/255, green: 148/255, blue: 30/255, alpha: 1.0)
    }
    
    class func primaryTextColor() -> UIColor {
        return defaultAppRedColor()
        //return UIColor(red: 13/255, green: 8/255, blue: 34/255, alpha: 1.0)
        //return UIColor(red: 247/255, green: 148/255, blue: 30/255, alpha: 1.0)
    }
    
    class func defaultButtonBorderColor() -> UIColor {
        //return UIColor(red: 85/255, green: 180/255, blue: 84/255, alpha: 1.0)
        //return UIColor(red: 247/255, green: 148/255, blue: 30/255, alpha: 1.0)
        return defaultAppRedColor()
    }
    
    class func navigationBarBackgroundColor() -> UIColor {
        return defaultAppRedColor()
    }
    
    class func facebookButtonBackgroundColor() -> UIColor{
        return UIColor(red: 69/255, green: 97/255, blue: 157/255, alpha: 1.0)
    }
    
    class func guideTourMessageColor() -> UIColor {
        return UIColor(red: 50/255, green: 153/255, blue: 214/255, alpha: 1.0)
    }
    
    class func defaultGradient() -> CAGradientLayer{
        let colorOne = defaultAppRedColor().CGColor as CGColorRef
        let colorTwo = defaultAppYellowColor().CGColor as CGColorRef
        let colors = [colorOne, colorTwo]
        
//        let stopOne = NSNumber(float: 0.0)
//        let stopTwo = NSNumber(float: 0.5)
//        let locations = [stopOne, stopTwo]
        
        let headerLayer = CAGradientLayer()
        headerLayer.colors = colors
//        headerLayer.locations = locations
//        headerLayer.startPoint = CGPointZero;
//        headerLayer.endPoint = CGPointMake(1, 1)
        return headerLayer
    }
    
    class func menuGradient() -> CAGradientLayer{
        let colorOne = UIColor(red: 201/255, green: 37/255, blue: 44/255, alpha: 1.0).CGColor as CGColorRef
        let colorTwo = UIColor(red: 247/255, green: 148/255, blue: 30/255, alpha: 1.0).CGColor as CGColorRef
        let colorThree = UIColor.grayColor().CGColor as CGColorRef
        let colors = [colorOne, colorTwo,colorThree]
        
        let stopOne = NSNumber(float: 0.0)
        let stopTwo = NSNumber(float: 0.5)
        let stopThree = NSNumber(float: 1.0)
        let locations = [stopOne, stopTwo, stopThree]
        
        let headerLayer = CAGradientLayer()
        headerLayer.colors = colors
        headerLayer.locations = locations
        headerLayer.startPoint = CGPointZero;
        headerLayer.endPoint = CGPointMake(1, 1)
        return headerLayer
    }
}
