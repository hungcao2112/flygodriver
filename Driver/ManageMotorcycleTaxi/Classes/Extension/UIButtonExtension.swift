//
//  UIButtonExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import UIKit

extension UIButton {
    func format(){
        self.layer.borderColor = UIColor.defaultButtonBorderColor().CGColor
        self.layer.cornerRadius = Constants.UI.Button.cornerRadius
        self.layer.borderWidth = Constants.UI.Button.borderThickness
    }
    
    func format2(){
        self.titleLabel?.font = UIFont.boldSystemFontOfSize((self.titleLabel?.font.pointSize)!)
        self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
        self.backgroundColor = UIColor.defaulButtonColor()
        self.layer.cornerRadius = Constants.UI.defaulCornerRadius
    }
    
    func formatWhiteBackground() {
        self.layer.cornerRadius = Constants.UI.Button.cornerRadius
        self.backgroundColor = UIColor.whiteColor()
        self.setTitleColor(UIColor.defaultAppColor(), forState: UIControlState.Normal)
    }
    
    func formatWithDefaultBackground() {
        self.layer.cornerRadius = Constants.UI.Button.cornerRadius
        self.backgroundColor = UIColor.defaultAppColor()
        self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    }
}
