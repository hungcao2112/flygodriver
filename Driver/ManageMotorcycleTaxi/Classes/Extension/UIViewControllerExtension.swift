//
//  UIViewControllerExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 7/22/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import CoreLocation

extension UIViewController{
    func showAlertWindow(title: String? = "", message: String? = "", okAction: (() -> ())?, cancelAction: (() -> ())? = nil){
        let uiAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        self.presentViewController(uiAlert, animated: true, completion: nil)
        
        uiAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            if let action = okAction{
                action()
            }
        }))
        
        uiAlert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .Cancel, handler: { action in
            if let action = cancelAction{
                action()
            }
        }))
    }
    
    func showAlertWindowWithOK(title: String? = "", message: String? = "", okAction: (() -> ())? = nil){
        let uiAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        self.presentViewController(uiAlert, animated: true, completion: nil)
        
        uiAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            if let action = okAction{
                action()
            }
        }))
        
    }
    
    func showEndTripNotificationView(tripHistory tripHistory: Trip){
        if let endTripMessageView = NSBundle.mainBundle().loadNibNamed("EndTripMessageView", owner: self, options: nil)![0] as? EndTripMessageView{
            endTripMessageView.viewController = self
            if let window = self.view.window{
                endTripMessageView.frame = (window.frame) ?? CGRectZero
                endTripMessageView.loadView(tripHistory: tripHistory)
                window.addSubview(endTripMessageView)
            }
            else{
                endTripMessageView.frame = self.view.frame
                endTripMessageView.loadView(tripHistory: tripHistory)
                self.view.addSubview(endTripMessageView)
            }
            
        }
    }
    
    func checkLocationEnabled() -> Bool {
        if (!CLLocationManager.locationServicesEnabled()) {
            self.showAlertWindow(NSLocalizedString("Please turn on Location Service to perform this operation.", comment: "Please turn on Location Service to perform this operation."), okAction: { () -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            })
            return false
        } else {
            print("Access")
            return true
        }
    }
}
