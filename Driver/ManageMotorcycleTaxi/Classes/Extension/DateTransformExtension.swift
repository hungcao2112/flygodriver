//
//  DateTransformExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/14/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import ObjectMapper

public class DateTransformExtension: DateTransform {
    public struct DateFormatStyle {
        static let DateTimeComplex = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let DateTimeSimple = "yyyy-MM-dd HH:mm:ss"
        static let DateOnly = "yyyy-MM-dd"
    }
    
    public typealias Object = NSDate
    public typealias JSON = String
    
    let dateFormatter = NSDateFormatter()
    
    public init(dateFormat: String = DateFormatStyle.DateTimeComplex) {
        dateFormatter.dateFormat = dateFormat
    }
    
    public override func transformFromJSON(value: AnyObject?) -> NSDate? {
        if let dateInString = value as? Double {
            return NSDate.init(timeIntervalSince1970: Double(dateInString)/1000.0)
        }
        return nil
    }
    
    public func transformToJSON(value: NSDate?) -> String? {
        if let date = value {
            return dateFormatter.stringFromDate(date)
        }
        return nil
    }
    
    public func transformToString(value: NSDate?, dateFormat: String) -> String?{
        if let date = value{
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = dateFormat
            return dateFormatter.stringFromDate(date)
        }
        return nil
    }
}