//
//  StringExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

extension String{
    func toDateTime(formatStr: String) -> NSDate?
    {
        //"yyyy-MM-dd HH:mm:ss Z"
        let formatter = NSDateFormatter()
        formatter.dateFormat = formatStr
        return formatter.dateFromString(self)
    }
    
    func toLocalDateTime(formatStr: String) -> NSDate?
    {
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = formatStr
        return formatter.dateFromString(self)
    }
    
    func isValidPhone() -> Bool {
        let regEx = "[0-9]{1,20}"
        return regCheck(regEx)
    }
    
    func isValidEmail() -> Bool {
        let regEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        return regCheck(regEx)
    }
    
    func isValidAccount() -> Bool{
        return self.isValidPhone() || self.isValidEmail()
    }
    
    func isValidPassword() -> Bool{
        return true
    }
    
    private func regCheck(regEx: String) -> Bool {
        let regTest = NSPredicate(format:"SELF MATCHES %@", regEx)
        return regTest.evaluateWithObject(self)
    }
    
    func substringWithRange(start: Int, end: Int) -> String
    {
        if (start < 0 || start > self.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if end < 0 || end > self.characters.count
        {
            print("end index \(end) out of bounds")
            return ""
        }
//        let range = Range(start: self.startIndex.advancedBy(start), end: self.startIndex.advancedBy(end))
        let range = self.startIndex.advancedBy(start)..<self.startIndex.advancedBy(end)
        return self.substringWithRange(range)
    }
    
    func substringWithRange(start: Int, location: Int) -> String
    {
        if (start < 0 || start > self.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if location < 0 || start + location > self.characters.count
        {
            print("end index \(start + location) out of bounds")
            return ""
        }
//        let range = Range(start: self.startIndex.advancedBy(start), end: self.startIndex.advancedBy(start + location))
        let range = self.startIndex.advancedBy(start)..<self.startIndex.advancedBy(start + location)
        return self.substringWithRange(range)
    }
    
    func toReadableAddress() -> String {
        var tmpString = self
        if (self.hasPrefix(",")) {
            tmpString = self.substringWithRange(1, end: tmpString.utf16.count - 2)
        }
//        int times = [[tmpString componentsSeparatedByString:@","] count]-1;
        let components:[String] = tmpString.componentsSeparatedByString(",")
        let times = components.count - 1
        if (times < 2) {
            return tmpString
        }
        return (components[0] + "," + components[1])
    }
    
    func convertToDictionary() -> [String:AnyObject]? {
        if let data = self.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
}