//
//  DistanceFeeExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/24/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

extension Double {
    func doubleToDistanceString() -> String {
        let formatter = NSNumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        return (formatter.stringFromNumber(self) ?? "") + "Km"
    }
}

extension Int {
    func intToPriceString() -> String {
        if (self < 1000000) {
            return String(Int(self/1000)) + "K"
        }
        else {
            let formatter = NSNumberFormatter()
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
            return formatter.stringFromNumber(Double(self)/1000000.0)! + NSLocalizedString("M", comment: "M")
        }
    }
}