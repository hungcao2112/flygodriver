//
//  NSDataExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/26/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

import Foundation

extension NSData{
    func toString() -> String?{
        let bytes = UnsafeBufferPointer<UInt8>(start: UnsafePointer(self.bytes), count:self.length)
        let dataString = bytes.map { String(format: "%02hhx", $0) }.reduce("", combine: { $0 + $1 })
        return dataString
    }
}