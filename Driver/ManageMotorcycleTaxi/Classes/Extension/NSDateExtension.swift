//
//  NSDateExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/3/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

extension NSDate : Comparable{
    func isSameDateWithoutTime(comparedDate: NSDate) -> Bool{
        if self.year() == comparedDate.year() && self.month() == comparedDate.month() && self.day() == comparedDate.day(){
            return true
        }
        
        return false
    }
    
    func year() -> Int
    {
        //Get Hour
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Year, fromDate: self)
        let year = components.year
        
        //Return Hour
        return year
    }
    
    func month() -> Int
    {
        //Get Hour
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Month, fromDate: self)
        let month = components.month
        
        //Return Hour
        return month
    }
    
    func day() -> Int
    {
        //Get Hour
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Day, fromDate: self)
        let day = components.day
        
        //Return Hour
        return day
    }
    
    func hour() -> Int
    {
        //Get Hour
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Hour, fromDate: self)
        let hour = components.hour
        
        //Return Hour
        return hour
    }
    
    
    func minute() -> Int
    {
        //Get Minute
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Minute, fromDate: self)
        let minute = components.minute
        
        //Return Minute
        return minute
    }
    
    func toShortTimeString() -> String
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        let timeString = formatter.stringFromDate(self)
        
        //Return Short Time String
        return timeString
    }
    
    func toLongTimeString() -> String
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        formatter.timeStyle = .LongStyle
        let timeString = formatter.stringFromDate(self)
        
        //Return Short Time String
        return timeString
    }
    
    func toLongDateString() -> String
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        formatter.dateStyle = .LongStyle
        let timeString = formatter.stringFromDate(self)
        
        //Return Short Time String
        return timeString
    }
    
    func toShortDateString() -> String
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        formatter.dateStyle = .ShortStyle
        let timeString = formatter.stringFromDate(self)
        
        //Return Short Time String
        return timeString
    }
    
    func toMediumDateString() -> String
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        formatter.dateStyle = .MediumStyle
        let timeString = formatter.stringFromDate(self)
        
        //Return Short Time String
        return timeString
    }
    
    func getDayNameBy() -> String?
    {
        let df  = NSDateFormatter()
        df.dateFormat = "EEEE"
        return df.stringFromDate(self);
    }
    
    func toDayTimeString() -> String {
        let df  = NSDateFormatter()
        df.dateFormat = "dd/MM/yyyy, HH:mm"
        return df.stringFromDate(self);
    }
    
    func toServerDayTimeString() -> String {
        return String(Int64(self.timeIntervalSince1970 * 1000))
    }
    
    func numOfHoursToDate(compareDate: NSDate) -> Int {
        let diff = compareDate.timeIntervalSinceDate(self)
        return Int(diff/3600.0)
    }
}

func + (date: NSDate, timeInterval: NSTimeInterval) -> NSDate {
    return date.dateByAddingTimeInterval(timeInterval)
}

public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
    if lhs.compare(rhs) == .OrderedSame {
        return true
    }
    return false
}

public func <(lhs: NSDate, rhs: NSDate) -> Bool {
    if lhs.compare(rhs) == .OrderedAscending {
        return true
    }
    return false
}

public func >(lhs: NSDate, rhs: NSDate) -> Bool {
    if lhs.compare(rhs) == .OrderedDescending {
        return true
    }
    return false
}
