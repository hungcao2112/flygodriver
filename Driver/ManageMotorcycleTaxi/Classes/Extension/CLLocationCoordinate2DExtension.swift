//
//  CLLocationCoordinate2DExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Tai Ho Trong on 7/11/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D {
    func distanceToCoordinate(place: CLLocationCoordinate2D) -> Double {
        let fromLoc = CLLocation(latitude: self.latitude, longitude: self.longitude)
        let toLoc = CLLocation(latitude: place.latitude, longitude: place.longitude)
        let distance = fromLoc.distanceFromLocation(toLoc)
        return distance
    }
}