//
//  UINavigationBarExtension.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    func format(){
        self.barTintColor = UIColor.navigationBarBackgroundColor()
        self.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.barStyle = UIBarStyle.Black
        self.tintColor = UIColor.whiteColor()
    }
}