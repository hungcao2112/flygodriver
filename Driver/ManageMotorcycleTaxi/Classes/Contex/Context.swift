//
//  Context.swift
//  ManageMotorcycleTaxi
//
//  Created by Quang Truong Dinh on 6/19/16.
//  Copyright © 2016 COMBROS TECHNOLOGY CO., LTD. All rights reserved.
//

import Foundation

class Context{
    class func getTokenHeader() -> [String : String]{
        if (NSUserDefaults.standardUserDefaults().objectForKey("token") == nil){
            return ["auth" : ""]
        }
        return["auth" : (NSUserDefaults.standardUserDefaults().objectForKey("token") as! String)]
        
    }
    
    class func setToken(tokenString : String) -> Void {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(tokenString,forKey: "token")
        preferences.synchronize()
    }
    
    class func setPushKey(tokenString : String) -> Void {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(tokenString,forKey: "pushKey")
        preferences.synchronize()
    }
    
    class func getPushKey() -> String{
        if (NSUserDefaults.standardUserDefaults().objectForKey("pushKey") == nil){
            return ""
        }
        return (NSUserDefaults.standardUserDefaults().objectForKey("pushKey") as! String)
        
    }
    
    class func setAuthKey(tokenString : String) -> Void {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(tokenString,forKey: "auth")
        preferences.synchronize()
    }
    
    class func getAuthKey() -> String{
        let auth = NSUserDefaults.standardUserDefaults().objectForKey("auth") ?? ""
        if let authStr = auth as? String{
            return authStr
        }
        else{
             return ""
        }
    }
    
    class func setFirstLaunchKey(tokenString : String) -> Void {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(tokenString,forKey: "firstLaunch")
        preferences.synchronize()
    }

    
    class func getFirstLaunchKey() -> String{
        let auth = NSUserDefaults.standardUserDefaults().objectForKey("firstLaunch") ?? ""
        if let authStr = auth as? String{
            return authStr
        }
        else{
            return ""
        }
    }
}